/*
 Navicat Premium Data Transfer

 Source Server         : web
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : 47.106.94.193:3306
 Source Schema         : feasthotel

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 25/10/2018 16:53:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha`  (
  `uuid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'uuid',
  `code` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统验证码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('02236118-1d4c-4280-88d8-5c5aec8bbce5', 'g7npg', '2018-09-25 17:21:22');
INSERT INTO `sys_captcha` VALUES ('05abaaa6-ae82-4a99-84e1-7941c342975b', 'yya6m', '2018-07-27 15:07:22');
INSERT INTO `sys_captcha` VALUES ('05d735ed-b662-4297-88b2-f9bc561fadf7', '8nbxe', '2018-08-14 18:07:58');
INSERT INTO `sys_captcha` VALUES ('06d2a03f-d571-4d59-893a-3b5458fd18ac', 'yae4m', '2018-07-27 18:11:42');
INSERT INTO `sys_captcha` VALUES ('11696637-9654-413a-884d-35079b8357cb', 'nm8gn', '2018-10-15 10:56:20');
INSERT INTO `sys_captcha` VALUES ('12156315-2884-483d-859f-fb5600114b21', 'm8cpe', '2018-07-31 17:49:10');
INSERT INTO `sys_captcha` VALUES ('154e48c0-615f-4d24-8dbe-daded8bf2ed9', 'egxd5', '2018-10-22 15:33:41');
INSERT INTO `sys_captcha` VALUES ('19f047a5-bb60-4644-8367-e393debbc991', '7na7y', '2018-07-30 11:03:24');
INSERT INTO `sys_captcha` VALUES ('1d73a077-80e6-49a6-81ee-c9a190769694', 'fawma', '2018-10-23 17:18:16');
INSERT INTO `sys_captcha` VALUES ('1e5ad30d-5c66-4361-82e0-cc9db07a17d7', 'd2pmb', '2018-08-22 17:08:33');
INSERT INTO `sys_captcha` VALUES ('21576979-213d-433f-8bb0-3779a6b1ec98', '26xm8', '2018-08-21 11:47:05');
INSERT INTO `sys_captcha` VALUES ('26208d83-3761-4794-8eba-a18d42a79a9f', '577ne', '2018-07-31 16:19:43');
INSERT INTO `sys_captcha` VALUES ('302d220a-d01f-4557-846c-79cf6c67004e', 'm3xxw', '2018-09-26 19:01:12');
INSERT INTO `sys_captcha` VALUES ('36951544-a061-4944-816c-b7aafa99acd8', '3ap26', '2018-10-12 11:20:16');
INSERT INTO `sys_captcha` VALUES ('3bb2a7f4-bb12-4761-8482-75e40bb34ae4', 'bee4f', '2018-09-17 09:39:39');
INSERT INTO `sys_captcha` VALUES ('4d91e59a-4119-4def-858a-5cc5baa2d92e', 'g3ce8', '2018-09-18 11:55:06');
INSERT INTO `sys_captcha` VALUES ('530cd80f-29d1-4ea1-879b-215571b46cca', 'ndyb5', '2018-10-23 11:56:15');
INSERT INTO `sys_captcha` VALUES ('58c88e1f-0eba-4ed5-8ba7-5db1d17a808f', 'xcd3n', '2018-09-19 18:08:54');
INSERT INTO `sys_captcha` VALUES ('59c17ef9-1be3-415b-8b6b-fc6ae3fab60a', '8n6m7', '2018-09-21 09:42:45');
INSERT INTO `sys_captcha` VALUES ('5a755c46-c566-4ceb-82e4-93034c4944e4', 'n5pab', '2018-09-21 18:11:57');
INSERT INTO `sys_captcha` VALUES ('5ba78c10-6ff5-4369-819f-728ae41b51e3', 'w2pef', '2018-09-21 18:10:40');
INSERT INTO `sys_captcha` VALUES ('5fcbb2b5-514c-4305-850a-6be56910f755', '3fay5', '2018-09-12 16:28:54');
INSERT INTO `sys_captcha` VALUES ('61ed1027-497b-4443-8f4c-f6105e9bc534', '23nx5', '2018-09-12 18:13:07');
INSERT INTO `sys_captcha` VALUES ('61f8f17d-27ab-400a-884d-50d11d83ac0f', 'f7eng', '2018-09-12 16:52:32');
INSERT INTO `sys_captcha` VALUES ('64b64045-1106-47c6-81b4-d7820a4d8132', 'yx2pn', '2018-09-20 09:59:51');
INSERT INTO `sys_captcha` VALUES ('6575b81b-fc26-4b81-89a6-775010a1786d', 'd6g8n', '2018-09-12 15:17:43');
INSERT INTO `sys_captcha` VALUES ('6d0d8561-a224-4f2a-8bee-b13b5ad9ea5d', 'mefnw', '2018-07-24 15:05:43');
INSERT INTO `sys_captcha` VALUES ('714244f1-f561-41c3-8ac6-00c385240d61', 'xef5b', '2018-09-20 11:42:40');
INSERT INTO `sys_captcha` VALUES ('77583ca4-cade-4d98-88a5-02184d1925c7', '47n8c', '2018-10-23 11:17:17');
INSERT INTO `sys_captcha` VALUES ('779086a1-a938-44c4-88a2-cb037da100fe', 'b2egb', '2018-07-26 09:40:42');
INSERT INTO `sys_captcha` VALUES ('7f341890-8b2b-41d8-8948-cb7df3787cb6', 'c2nw2', '2018-08-24 16:26:56');
INSERT INTO `sys_captcha` VALUES ('828c629e-8de6-49b3-8cd2-7fd976c2285e', 'y55cn', '2018-10-15 09:08:15');
INSERT INTO `sys_captcha` VALUES ('832baad5-1b09-41db-8472-f1b2e022b638', '3n8mw', '2018-09-21 12:04:29');
INSERT INTO `sys_captcha` VALUES ('837d22a7-1883-4224-8897-d7c4461c1b64', 'bmefb', '2018-09-21 18:11:55');
INSERT INTO `sys_captcha` VALUES ('84564059-2851-47d6-8215-6b2cf3f07ce2', 'ecdew', '2018-09-21 18:11:55');
INSERT INTO `sys_captcha` VALUES ('85cbb533-eb64-43b7-860d-a8897fadcc5c', 'anppg', '2018-10-23 11:16:53');
INSERT INTO `sys_captcha` VALUES ('87537cdf-fb00-4881-8fca-373930de8af3', '3g6en', '2018-07-30 11:28:39');
INSERT INTO `sys_captcha` VALUES ('98482353-bc22-4073-8b67-5f00bd13dda7', 'b47np', '2018-09-26 17:57:23');
INSERT INTO `sys_captcha` VALUES ('a03d591a-9b70-482d-82e2-5e18ef0b3596', 'agbxn', '2018-09-26 14:36:05');
INSERT INTO `sys_captcha` VALUES ('a1f08727-8635-4d20-8417-fe8e70e3669a', 'c4w3a', '2018-07-27 17:28:29');
INSERT INTO `sys_captcha` VALUES ('a47d3027-9b9f-4f71-8873-4b289173c433', 'y5f74', '2018-10-15 10:56:27');
INSERT INTO `sys_captcha` VALUES ('a56fd8a4-6b1c-47e4-859b-e17d36791af6', '363ma', '2018-09-20 09:56:25');
INSERT INTO `sys_captcha` VALUES ('a9b005d9-a439-4452-87b3-d382d0b7774d', 'dnnm7', '2018-10-24 11:20:03');
INSERT INTO `sys_captcha` VALUES ('a9f1ea99-27eb-43da-85cf-181e681a914c', '7peeg', '2018-09-21 18:11:56');
INSERT INTO `sys_captcha` VALUES ('ab0c43a2-0278-4c6e-8910-68c2b86f79f3', '8ndpn', '2018-09-21 12:01:01');
INSERT INTO `sys_captcha` VALUES ('ad43c8b2-43f7-4876-8539-3b154464c189', 'ngf4c', '2018-09-12 18:09:41');
INSERT INTO `sys_captcha` VALUES ('b0513e71-a5f4-4119-838f-287a1861c8c5', 'm66fp', '2018-07-31 18:06:30');
INSERT INTO `sys_captcha` VALUES ('b23829ae-6591-4ac8-8753-51d57a28383a', 'n8y2b', '2018-07-31 17:10:53');
INSERT INTO `sys_captcha` VALUES ('b71cd9ba-3195-4fd4-8510-23284a6a4741', 'dxmx6', '2018-07-26 15:48:02');
INSERT INTO `sys_captcha` VALUES ('bc0fef32-06b5-4ff9-807e-5ee7164879b6', '676ym', '2018-09-12 17:59:41');
INSERT INTO `sys_captcha` VALUES ('bdc9bf7e-c7ac-4cab-8a3e-cb940ac4b747', 'n56m3', '2018-09-21 18:11:53');
INSERT INTO `sys_captcha` VALUES ('bf477834-48c1-4969-8dc6-5ccd4359eb4b', 'a5na8', '2018-09-28 09:33:26');
INSERT INTO `sys_captcha` VALUES ('c079494d-2d44-4667-8078-e5e3470e197d', 'pxbww', '2018-09-19 09:50:25');
INSERT INTO `sys_captcha` VALUES ('c4b36f2d-43d2-47b0-87ab-0c828d73198d', '8nx5n', '2018-10-22 16:44:47');
INSERT INTO `sys_captcha` VALUES ('c56cac4b-a37e-4dc0-894e-af6de23cff42', '2b4w5', '2018-08-02 11:46:39');
INSERT INTO `sys_captcha` VALUES ('cf755cdd-c1bd-4d83-8af5-c963271ea123', 'fpy3g', '2018-09-26 14:46:34');
INSERT INTO `sys_captcha` VALUES ('d4e04596-c15b-4936-8870-652cdcb8fd88', 'pdpe8', '2018-08-22 17:08:33');
INSERT INTO `sys_captcha` VALUES ('def8535c-d5bb-4792-8fa5-be53749a5af0', 'xym8c', '2018-09-21 18:10:32');
INSERT INTO `sys_captcha` VALUES ('e16518d1-b553-4ca0-8886-cb46d5bbd96a', 'n4gbn', '2018-10-24 09:15:29');
INSERT INTO `sys_captcha` VALUES ('e2dd9fa1-1e16-49a8-85e6-37615dacb9c8', 'pwc5x', '2018-10-09 15:34:33');
INSERT INTO `sys_captcha` VALUES ('e3a3eae6-423a-4279-832b-2960a310f4dc', 'defgg', '2018-09-18 11:07:05');
INSERT INTO `sys_captcha` VALUES ('e8f3f677-5000-4abd-839c-6c3e8d3e7280', 'eada5', '2018-09-21 18:10:08');
INSERT INTO `sys_captcha` VALUES ('ea41ee0b-1811-49d0-8550-4d7a26b978aa', '2bnbx', '2018-07-27 18:08:59');
INSERT INTO `sys_captcha` VALUES ('ebe3bc99-f9ec-4566-8959-cafea2b55c54', 'c2cxx', '2018-09-21 18:11:57');
INSERT INTO `sys_captcha` VALUES ('f2928678-7a10-463c-8088-0494df25f192', 'a8mcm', '2018-09-19 17:51:50');
INSERT INTO `sys_captcha` VALUES ('f639e7ed-01c3-462d-8f6a-066835532f01', 'ma4gc', '2018-09-26 18:25:18');
INSERT INTO `sys_captcha` VALUES ('f9b53c8d-ee13-46f9-8943-382f3a335f45', 'n653p', '2018-07-30 09:51:23');
INSERT INTO `sys_captcha` VALUES ('fdd6c7df-160a-495c-80f3-dc7b216da76c', '5ec5e', '2018-09-21 17:48:41');
INSERT INTO `sys_captcha` VALUES ('fe7ffa0b-9b99-4393-877b-60d03fe47a73', 'cc38w', '2018-09-26 19:51:32');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', 0, '云存储配置信息');
INSERT INTO `sys_config` VALUES (2, '123', '123', 1, '');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 164 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (148, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":30,\"parentId\":35,\"name\":\"文章区域\",\"url\":\"articlearea/articlearea\",\"type\":1,\"icon\":\"config\",\"orderNum\":1}', 103, '127.0.0.1', '2018-10-23 13:42:30');
INSERT INTO `sys_log` VALUES (149, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":52,\"parentId\":51,\"name\":\"客服电话\",\"url\":\"servertel/servertel\",\"type\":1,\"icon\":\"config\",\"orderNum\":6}', 102, '127.0.0.1', '2018-10-23 13:42:51');
INSERT INTO `sys_log` VALUES (150, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":71,\"parentId\":70,\"name\":\"通知绑定\",\"url\":\"wxuserinfo/wxuserbind\",\"type\":1,\"icon\":\"config\",\"orderNum\":0}', 100, '127.0.0.1', '2018-10-23 13:43:09');
INSERT INTO `sys_log` VALUES (151, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":41,\"parentId\":35,\"name\":\"文章列表\",\"url\":\"article/article\",\"type\":1,\"icon\":\"config\",\"orderNum\":3}', 103, '127.0.0.1', '2018-10-23 13:44:20');
INSERT INTO `sys_log` VALUES (152, 'admin', '修改角色', 'io.youngking.modules.sys.controller.SysRoleController.update()', '{\"roleId\":1,\"roleName\":\"攻略管理\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[1,2,15,16,17,18,3,19,20,21,22,4,23,24,25,26,27,29,35,30,31,32,33,34,36,37,38,39,40,41,42,43,44,45,51,46,47,48,49,50,52,53,54,55,56,136,137,138,139,140,148,149,150,151,152,153,154,155,156,157,57,58,59,60,61,62,163,164,165,166,167,173,174,175,176,177,70,65,66,67,68,69,71,72,73,74,75,76,77,78,89,90,91,92,93,158,159,160,161,162,129,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,178,179,180,181,182,130,84,85,86,87,88,114,115,116,117,118,141,142,119,120,121,122,123,124,125,126,127,128,168,169,170,171,172,-666666]}', 3094, '127.0.0.1', '2018-10-24 09:34:30');
INSERT INTO `sys_log` VALUES (153, 'admin', '保存配置', 'io.youngking.modules.sys.controller.SysConfigController.save()', '{\"id\":2,\"paramKey\":\"123\",\"paramValue\":\"123\",\"remark\":\"\"}', 359, '127.0.0.1', '2018-10-24 11:25:32');
INSERT INTO `sys_log` VALUES (154, 'admin', '删除菜单', 'io.youngking.modules.sys.controller.SysMenuController.delete()', '27', 0, '127.0.0.1', '2018-10-24 11:26:31');
INSERT INTO `sys_log` VALUES (155, 'hahaha', '删除菜单', 'io.youngking.modules.sys.controller.SysMenuController.delete()', '27', 0, '127.0.0.1', '2018-10-24 11:26:57');
INSERT INTO `sys_log` VALUES (156, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":27,\"parentId\":70,\"name\":\"参数管理\",\"url\":\"sys/config\",\"perms\":\"sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete\",\"type\":1,\"icon\":\"config\",\"orderNum\":4}', 32, '127.0.0.1', '2018-10-24 11:27:01');
INSERT INTO `sys_log` VALUES (157, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":27,\"parentId\":70,\"name\":\"参数管理\",\"url\":\"sys/config\",\"perms\":\"sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete\",\"type\":1,\"icon\":\"config\",\"orderNum\":4}', 16, '127.0.0.1', '2018-10-24 11:27:02');
INSERT INTO `sys_log` VALUES (158, 'admin', '删除菜单', 'io.youngking.modules.sys.controller.SysMenuController.delete()', '27', 0, '127.0.0.1', '2018-10-24 11:27:09');
INSERT INTO `sys_log` VALUES (159, 'admin', '修改菜单', 'io.youngking.modules.sys.controller.SysMenuController.update()', '{\"menuId\":27,\"parentId\":1,\"name\":\"参数管理\",\"url\":\"sys/config\",\"perms\":\"sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete\",\"type\":1,\"icon\":\"config\",\"orderNum\":4}', 16, '127.0.0.1', '2018-10-24 11:27:16');
INSERT INTO `sys_log` VALUES (160, 'admin', '保存菜单', 'io.youngking.modules.sys.controller.SysMenuController.save()', '{\"menuId\":185,\"parentId\":0,\"name\":\"驱蚊器\",\"url\":\"d\\u0027sa\\u0027f\\u0027f\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}', 78, '127.0.0.1', '2018-10-24 11:30:23');
INSERT INTO `sys_log` VALUES (161, 'admin', '删除菜单', 'io.youngking.modules.sys.controller.SysMenuController.delete()', '185', 156, '127.0.0.1', '2018-10-25 09:50:55');
INSERT INTO `sys_log` VALUES (162, 'admin', '修改角色', 'io.youngking.modules.sys.controller.SysRoleController.update()', '{\"roleId\":2,\"roleName\":\"酒店信息\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[46,47,48,49,50,-666666,51]}', 219, '127.0.0.1', '2018-10-25 14:39:27');
INSERT INTO `sys_log` VALUES (163, 'admin', '修改角色', 'io.youngking.modules.sys.controller.SysRoleController.update()', '{\"roleId\":1,\"roleName\":\"攻略管理\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[35,30,31,32,33,34,36,37,38,39,40,41,42,43,44,45,-666666]}', 63, '127.0.0.1', '2018-10-25 14:53:09');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 186 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 6);
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (30, 35, '文章区域', 'articlearea/articlearea', NULL, 1, 'config', 1);
INSERT INTO `sys_menu` VALUES (31, 30, '查看', NULL, 'articlearea:articlearea:list,articlearea:articlearea:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (32, 30, '新增', NULL, 'articlearea:articlearea:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (33, 30, '修改', NULL, 'articlearea:articlearea:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (34, 30, '删除', NULL, 'articlearea:articlearea:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (35, 0, '攻略管理', NULL, NULL, 0, 'menu', 2);
INSERT INTO `sys_menu` VALUES (36, 35, '文章类别', 'articletype/articletype', NULL, 1, 'config', 2);
INSERT INTO `sys_menu` VALUES (37, 36, '查看', NULL, 'articletype:articletype:list,articletype:articletype:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (38, 36, '新增', NULL, 'articletype:articletype:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (39, 36, '修改', NULL, 'articletype:articletype:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (40, 36, '删除', NULL, 'articletype:articletype:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (41, 35, '文章列表', 'article/article', NULL, 1, 'config', 3);
INSERT INTO `sys_menu` VALUES (42, 41, '查看', NULL, 'article:article:list,article:article:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (43, 41, '新增', NULL, 'article:article:save,articletype:articletype:select,articlearea:articlearea:select', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (44, 41, '修改', NULL, 'article:article:update,articletype:articletype:select,articlearea:articlearea:select', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (45, 41, '删除', NULL, 'article:article:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (46, 51, '酒店信息', 'hotelinfo/hotelinfo', NULL, 1, 'config', 1);
INSERT INTO `sys_menu` VALUES (47, 46, '查看', NULL, 'hotelinfo:hotelinfo:list,hotelinfo:hotelinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (48, 46, '新增', NULL, 'hotelinfo:hotelinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (49, 46, '修改', NULL, 'hotelinfo:hotelinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (50, 46, '删除', NULL, 'hotelinfo:hotelinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (51, 0, '酒店管理', NULL, NULL, 0, 'log', 4);
INSERT INTO `sys_menu` VALUES (52, 51, '客服电话', 'servertel/servertel', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (53, 52, '查看', NULL, 'servertel:servertel:list,servertel:servertel:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (54, 52, '新增', NULL, 'servertel:servertel:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (55, 52, '修改', NULL, 'servertel:servertel:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (56, 52, '删除', NULL, 'servertel:servertel:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (57, 0, '房间订单', NULL, NULL, 0, 'bianji', 3);
INSERT INTO `sys_menu` VALUES (58, 57, '订房订单', 'roomreceipt/roomreceipt', NULL, 1, 'config', 1);
INSERT INTO `sys_menu` VALUES (59, 58, '查看', NULL, 'roomreceipt:roomreceipt:list,roomreceipt:roomreceipt:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (60, 58, '新增', NULL, 'roomreceipt:roomreceipt:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (61, 58, '修改', NULL, 'roomreceipt:roomreceipt:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (62, 58, '删除', NULL, 'roomreceipt:roomreceipt:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (65, 70, '公众号用户', 'wxuserinfo/wxuserinfo', NULL, 1, 'config', 0);
INSERT INTO `sys_menu` VALUES (66, 65, '查看', NULL, 'wxuserinfo:wxuserinfo:list,wxuserinfo:wxuserinfo:info,roomreceipt:roomreceipt:orderlist', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (67, 65, '新增', NULL, 'wxuserinfo:wxuserinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (68, 65, '修改', NULL, 'wxuserinfo:wxuserinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (69, 65, '删除', NULL, 'wxuserinfo:wxuserinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (70, 0, '客户管理', NULL, NULL, 0, 'admin', 5);
INSERT INTO `sys_menu` VALUES (71, 70, '通知绑定', 'wxuserinfo/wxuserbind', NULL, 1, 'config', 0);
INSERT INTO `sys_menu` VALUES (72, 71, '查看', 'wxuserinfo:userbind:list', 'wxuserinfo:wxuserbind:list', 2, NULL, 1);
INSERT INTO `sys_menu` VALUES (73, 71, '订房绑定', NULL, 'wxuserinfo:wxuserbind:DFBD', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (74, 71, '订房解绑', NULL, 'wxuserinfo:wxuserbind:DFJB', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (75, 71, '订餐绑定', NULL, 'wxuserinfo:wxuserbind:DCBD', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (76, 71, '订餐解绑', NULL, 'wxuserinfo:wxuserbind:DCJB', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (77, 71, '点评绑定', NULL, 'wxuserinfo:wxuserbind:DPBD', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (78, 71, '点评解绑', NULL, 'wxuserinfo:wxuserbind:DPJB', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (84, 130, '用户评价', 'comment/comment', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (85, 84, '查看', NULL, 'comment:comment:list,comment:comment:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (86, 84, '新增', NULL, 'comment:comment:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (87, 84, '修改', NULL, 'comment:comment:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (88, 84, '删除', NULL, 'comment:comment:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (89, 70, '用户建议', 'feedback/feedback', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (90, 89, '查看', NULL, 'feedback:feedback:list,feedback:feedback:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (91, 89, '新增', NULL, 'feedback:feedback:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (92, 89, '修改', NULL, 'feedback:feedback:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (93, 89, '删除', NULL, 'feedback:feedback:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (94, 129, '菜品信息', 'good/good', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (95, 94, '查看', NULL, 'good:good:list,good:good:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (96, 94, '新增', NULL, 'good:good:save,good:good:selecttypelist', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (97, 94, '修改', NULL, 'good:good:update,good:good:selecttypelist', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (98, 94, '删除', NULL, 'good:good:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (99, 129, '菜单分类', 'goodtype/goodtype', NULL, 1, 'config', 1);
INSERT INTO `sys_menu` VALUES (100, 99, '查看', NULL, 'goodtype:goodtype:list,goodtype:goodtype:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (101, 99, '新增', NULL, 'goodtype:goodtype:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (102, 99, '修改', NULL, 'goodtype:goodtype:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (103, 99, '删除', NULL, 'goodtype:goodtype:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (104, 129, '订餐订单', 'goodreceipt/goodreceipt', NULL, 1, 'config', 0);
INSERT INTO `sys_menu` VALUES (105, 104, '查看', NULL, 'goodreceipt:goodreceipt:list,goodreceipt:goodreceipt:info,goodrecepititem:goodrecepititem:list', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (106, 104, '新增', NULL, 'goodreceipt:goodreceipt:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (107, 104, '修改', NULL, 'goodreceipt:goodreceipt:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (108, 104, '删除', NULL, 'goodreceipt:goodreceipt:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (114, 130, '房间信息', 'roominfo/roominfo', NULL, 1, 'config', 1);
INSERT INTO `sys_menu` VALUES (115, 114, '查看', NULL, 'roominfo:roominfo:list,roominfo:roominfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (116, 114, '新增', NULL, 'roominfo:roominfo:save,roomtype:roomtype:selectlist', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (117, 114, '修改', NULL, 'roominfo:roominfo:update,roomtype:roomtype:selectlist', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (118, 114, '删除', NULL, 'roominfo:roominfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (119, 130, '房间类型', 'roomtype/roomtype', NULL, 1, 'config', 0);
INSERT INTO `sys_menu` VALUES (120, 119, '查看', NULL, 'roomtype:roomtype:list,roomtype:roomtype:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (121, 119, '新增', NULL, 'roomtype:roomtype:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (122, 119, '修改', NULL, 'roomtype:roomtype:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (123, 119, '删除', NULL, 'roomtype:roomtype:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (124, 130, '订房用户', 'roomuser/roomuser', NULL, 1, 'config', 3);
INSERT INTO `sys_menu` VALUES (125, 124, '查看', NULL, 'roomuser:roomuser:list,roomuser:roomuser:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (126, 124, '新增', NULL, 'roomuser:roomuser:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (127, 124, '修改', NULL, 'roomuser:roomuser:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (128, 124, '删除', NULL, 'roomuser:roomuser:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (129, 0, '订餐管理', NULL, NULL, 0, 'editor', 1);
INSERT INTO `sys_menu` VALUES (130, 0, '房间管理', NULL, NULL, 0, 'shouye', 0);
INSERT INTO `sys_menu` VALUES (136, 51, '首页图片', 'homepicture/homepicture', NULL, 1, 'config', 4);
INSERT INTO `sys_menu` VALUES (137, 136, '查看', NULL, 'homepicture:homepicture:list,homepicture:homepicture:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (138, 136, '新增', NULL, 'homepicture:homepicture:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (139, 136, '修改', NULL, 'homepicture:homepicture:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (140, 136, '删除', NULL, 'homepicture:homepicture:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (141, 114, '开发', NULL, 'roominfo:roominfo:open', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (142, 114, '退房', NULL, 'roominfo:roominfo:reset', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (148, 51, '周边信息', 'surrounding/surrounding', NULL, 1, 'config', 2);
INSERT INTO `sys_menu` VALUES (149, 148, '查看', NULL, 'surrounding:surrounding:list,surrounding:surrounding:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (150, 148, '新增', NULL, 'surrounding:surrounding:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (151, 148, '修改', NULL, 'surrounding:surrounding:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (152, 148, '删除', NULL, 'surrounding:surrounding:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (153, 51, '早餐信息', 'breakfast/breakfast', NULL, 1, 'config', 3);
INSERT INTO `sys_menu` VALUES (154, 153, '查看', NULL, 'breakfast:breakfast:list,breakfast:breakfast:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (155, 153, '新增', NULL, 'breakfast:breakfast:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (156, 153, '修改', NULL, 'breakfast:breakfast:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (157, 153, '删除', NULL, 'breakfast:breakfast:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (158, 70, '小程序用户', 'wxappletsuserinfo/wxappletsuserinfo', NULL, 1, 'config', 0);
INSERT INTO `sys_menu` VALUES (159, 158, '查看', NULL, 'wxappletsuserinfo:wxappletsuserinfo:list,wxappletsuserinfo:wxappletsuserinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (160, 158, '新增', NULL, 'wxappletsuserinfo:wxappletsuserinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (161, 158, '修改', NULL, 'wxappletsuserinfo:wxappletsuserinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (162, 158, '删除', NULL, 'wxappletsuserinfo:wxappletsuserinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (163, 57, '退款订单', 'refundreceipt/refundreceipt', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (164, 163, '查看', NULL, 'refundreceipt:refundreceipt:list,refundreceipt:refundreceipt:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (165, 163, '新增', NULL, 'refundreceipt:refundreceipt:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (166, 163, '修改', NULL, 'refundreceipt:refundreceipt:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (167, 163, '删除', NULL, 'refundreceipt:refundreceipt:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (168, 130, '节日价格', 'typeprice/typeprice', NULL, 1, 'config', 2);
INSERT INTO `sys_menu` VALUES (169, 168, '查看', NULL, 'typeprice:typeprice:list,typeprice:typeprice:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (170, 168, '新增', NULL, 'typeprice:typeprice:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (171, 168, '修改', NULL, 'typeprice:typeprice:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (172, 168, '删除', NULL, 'typeprice:typeprice:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (173, 57, '订房通知', 'roomnotice/roomnotice', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (174, 173, '查看', NULL, 'roomnotice:roomnotice:list,roomnotice:roomnotice:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (175, 173, '新增', NULL, 'roomnotice:roomnotice:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (176, 173, '修改', NULL, 'roomnotice:roomnotice:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (177, 173, '删除', NULL, 'roomnotice:roomnotice:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (178, 129, '订餐通知', 'goodnotice/goodnotice', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (179, 178, '查看', NULL, 'goodnotice:goodnotice:list,goodnotice:goodnotice:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (180, 178, '新增', NULL, 'goodnotice:goodnotice:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (181, 178, '修改', NULL, 'goodnotice:goodnotice:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (182, 178, '删除', NULL, 'goodnotice:goodnotice:delete', 2, NULL, 6);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '攻略管理', '', 1, '2018-09-20 11:36:41');
INSERT INTO `sys_role` VALUES (2, '酒店信息', '', 1, '2018-09-26 14:48:45');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 519 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (495, 2, 46);
INSERT INTO `sys_role_menu` VALUES (496, 2, 47);
INSERT INTO `sys_role_menu` VALUES (497, 2, 48);
INSERT INTO `sys_role_menu` VALUES (498, 2, 49);
INSERT INTO `sys_role_menu` VALUES (499, 2, 50);
INSERT INTO `sys_role_menu` VALUES (500, 2, -666666);
INSERT INTO `sys_role_menu` VALUES (501, 2, 51);
INSERT INTO `sys_role_menu` VALUES (502, 1, 35);
INSERT INTO `sys_role_menu` VALUES (503, 1, 30);
INSERT INTO `sys_role_menu` VALUES (504, 1, 31);
INSERT INTO `sys_role_menu` VALUES (505, 1, 32);
INSERT INTO `sys_role_menu` VALUES (506, 1, 33);
INSERT INTO `sys_role_menu` VALUES (507, 1, 34);
INSERT INTO `sys_role_menu` VALUES (508, 1, 36);
INSERT INTO `sys_role_menu` VALUES (509, 1, 37);
INSERT INTO `sys_role_menu` VALUES (510, 1, 38);
INSERT INTO `sys_role_menu` VALUES (511, 1, 39);
INSERT INTO `sys_role_menu` VALUES (512, 1, 40);
INSERT INTO `sys_role_menu` VALUES (513, 1, 41);
INSERT INTO `sys_role_menu` VALUES (514, 1, 42);
INSERT INTO `sys_role_menu` VALUES (515, 1, 43);
INSERT INTO `sys_role_menu` VALUES (516, 1, 44);
INSERT INTO `sys_role_menu` VALUES (517, 1, 45);
INSERT INTO `sys_role_menu` VALUES (518, 1, -666666);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '31abdee2bfd78897c4c009931a42d72b6deba64e7b41ff1777a4bb20641dc330', 'YzcmCZNvbXocrsz9dm8e', 'root@youngking.io', '13612345678', 1, 1, '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES (2, 'hahaha', '31abdee2bfd78897c4c009931a42d72b6deba64e7b41ff1777a4bb20641dc330', 'YzcmCZNvbXocrsz9dm8e', '1747454154@qq.com', '15754310990', 1, 1, '2018-09-20 11:35:28');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (4, 2, 1);
INSERT INTO `sys_user_role` VALUES (5, 2, 2);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (1, '881ad41d673bdbc8121e2faf4ba5c572', '2018-10-26 04:01:37', '2018-10-25 16:01:37');
INSERT INTO `sys_user_token` VALUES (2, '0c43da4daf6ec9eedd21009d4b54c3e7', '2018-10-26 03:24:53', '2018-10-25 15:24:53');

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `areaid` int(11) DEFAULT 0 COMMENT '区域id',
  `typeid` int(11) DEFAULT 0 COMMENT '类别id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标题',
  `titleimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标题图片',
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `contents` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '正文内容',
  `num` int(11) DEFAULT 0 COMMENT '阅读量',
  `opuser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '发布人',
  `sendtime` datetime(0) DEFAULT NULL COMMENT '发布时间',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:显示；1:不显示)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_article
-- ----------------------------
INSERT INTO `tb_article` VALUES (22, 12, 8, '一日游', '10c4d49b-6491-45e9-bee6-e6f6fc57713c.jpg', '', 'https://jingyan.baidu.com/article/f0e83a2562e7ea22e5910138.html', 0, 'admin', '2018-10-25 11:31:30', 0, 0);
INSERT INTO `tb_article` VALUES (23, 13, 9, '特价', '7dabeb53-dbab-4698-b015-d380d5ece566.jpg', '', 'http://you.ctrip.com/sight/beijing1/1461311.html', 0, 'admin', '2018-10-25 11:58:16', 0, 0);

-- ----------------------------
-- Table structure for tb_articlearea
-- ----------------------------
DROP TABLE IF EXISTS `tb_articlearea`;
CREATE TABLE `tb_articlearea`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `areacode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '区域编号',
  `areaname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '区域名称',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:是；1:否)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章区域表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_articlearea
-- ----------------------------
INSERT INTO `tb_articlearea` VALUES (12, '', '公园', 0, 0, '');
INSERT INTO `tb_articlearea` VALUES (13, '', '湖景', 0, 0, '');

-- ----------------------------
-- Table structure for tb_articletype
-- ----------------------------
DROP TABLE IF EXISTS `tb_articletype`;
CREATE TABLE `tb_articletype`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `typecode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '类别编号',
  `typename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '类别名称',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:显示；1:不显示)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_articletype
-- ----------------------------
INSERT INTO `tb_articletype` VALUES (8, '', '攻略', 0, 0, '');
INSERT INTO `tb_articletype` VALUES (9, '', '票价', 0, 0, '');

-- ----------------------------
-- Table structure for tb_breakfast
-- ----------------------------
DROP TABLE IF EXISTS `tb_breakfast`;
CREATE TABLE `tb_breakfast`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标题',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `price` decimal(10, 2) DEFAULT 0.00 COMMENT '价格',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:显示；1:不显示)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '早餐表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_breakfast
-- ----------------------------
INSERT INTO `tb_breakfast` VALUES (12, '851cedc9-024b-4559-8bbb-6acde63687d6.jpg', '麻园', '', 18.00, 0, 0);

-- ----------------------------
-- Table structure for tb_cart
-- ----------------------------
DROP TABLE IF EXISTS `tb_cart`;
CREATE TABLE `tb_cart`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `foodid` int(11) DEFAULT 0 COMMENT '食品id',
  `price` decimal(10, 2) DEFAULT 0.00 COMMENT '单价',
  `num` int(11) DEFAULT 0 COMMENT '数量',
  `ischarge` int(11) DEFAULT 0 COMMENT '是否结算(0:未结算；1:已结算)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment`;
CREATE TABLE `tb_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用户昵称',
  `roomtypename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '房型名称',
  `receiptid` int(11) DEFAULT 0 COMMENT '订单id',
  `contents` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '评价内容',
  `imgurl` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `replyinfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '回复内容',
  `replytime` datetime(0) DEFAULT NULL COMMENT '回复时间',
  `commenttime` datetime(0) DEFAULT NULL COMMENT '评论时间',
  `roomtypeid` int(11) DEFAULT 0 COMMENT '房间类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房间评价表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_comment
-- ----------------------------
INSERT INTO `tb_comment` VALUES (110, 24, '甜品', '大床房', 202, '环境可以就是没有特色服务。', '', '感谢您的评价，我们会继续改进，欢迎下次光临', '2018-10-22 17:02:55', '2018-10-22 16:56:21', 11);
INSERT INTO `tb_comment` VALUES (112, 0, '哈哈哈', '大床房', 0, '这个地方空气很好', '', '', NULL, '2018-10-25 10:07:00', 11);

-- ----------------------------
-- Table structure for tb_feedback
-- ----------------------------
DROP TABLE IF EXISTS `tb_feedback`;
CREATE TABLE `tb_feedback`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `itype` int(11) DEFAULT 0 COMMENT '建议类型(0:酒店；1:服务)',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `praise` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '好评度',
  `contents` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '评论内容',
  `imgurl` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `sendtime` datetime(0) DEFAULT NULL COMMENT '发送时间',
  `replycontent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '回复内容',
  `replytime` datetime(0) DEFAULT NULL COMMENT '回复时间',
  `replystaff` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '回复人',
  `status` int(11) DEFAULT 0 COMMENT '回复状态(0:未回复，1:已回复)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 142 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户建议表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_feedback
-- ----------------------------
INSERT INTO `tb_feedback` VALUES (141, 0, 13, '2', '意见反馈', '', '2018-10-23 15:42:45', '都很好', '2018-10-23 15:43:09', '13', 1);

-- ----------------------------
-- Table structure for tb_good
-- ----------------------------
DROP TABLE IF EXISTS `tb_good`;
CREATE TABLE `tb_good`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `typeid` int(11) DEFAULT 0 COMMENT '类别id',
  `typename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '类别名称',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '美食名称',
  `price` decimal(10, 2) DEFAULT 0.00 COMMENT '单价',
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标签',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:显示；1:不显示)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_good
-- ----------------------------
INSERT INTO `tb_good` VALUES (42, 16, '主食', '小笼包', 0.01, '', '', '', 0, 1, '63b5c08e-fcd5-47b5-baa8-1f7495bad270.jpg');
INSERT INTO `tb_good` VALUES (43, 16, '主食', '米饭', 4.00, '', '', '', 0, 0, '1651b301-54bc-400f-8010-791ffce0eddc.jpg');
INSERT INTO `tb_good` VALUES (44, 16, '主食', '面条', 18.00, '', '', '', 0, 0, '1514358b-7be4-4011-adf9-965ea9482da5.jpg');
INSERT INTO `tb_good` VALUES (45, 16, '主食', '馒头', 4.00, '', '', '', 0, 0, '97d4e25d-ad5a-4f30-a271-4dd27921c796.jpg');
INSERT INTO `tb_good` VALUES (46, 15, '饮料', '柠檬汁', 16.00, '', '', '', 0, 0, 'f0f6dd31-1e3e-41c6-b2d5-a982e6760c91.jpg');
INSERT INTO `tb_good` VALUES (47, 15, '饮料', '西柚汁', 18.00, '', '', '', 0, 0, '0567a605-3327-4607-800e-c8b518481ec2.jpg');
INSERT INTO `tb_good` VALUES (48, 15, '饮料', '橙汁', 18.00, '', '', '', 0, 0, 'f1f4f88c-beef-44e6-9349-31638f88e3e1.jpg');

-- ----------------------------
-- Table structure for tb_goodnotice
-- ----------------------------
DROP TABLE IF EXISTS `tb_goodnotice`;
CREATE TABLE `tb_goodnotice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `receiptid` int(11) DEFAULT 0 COMMENT '订单id',
  `sendtime` datetime(0) DEFAULT NULL COMMENT '发送时间',
  `isreply` int(11) DEFAULT 0 COMMENT '是否回复',
  `recontent` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '回复内容',
  `retime` datetime(0) DEFAULT NULL COMMENT '回复时间',
  `reuser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '回复人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订餐通知表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_goodnotice
-- ----------------------------
INSERT INTO `tb_goodnotice` VALUES (7, 13, 170, '2018-10-23 15:47:34', 1, '马上为你准备', '2018-10-23 15:48:09', '13');
INSERT INTO `tb_goodnotice` VALUES (9, 16, 174, '2018-10-25 11:38:31', 1, '稍等2分钟！', '2018-10-25 11:42:39', '13');

-- ----------------------------
-- Table structure for tb_goodreceipt
-- ----------------------------
DROP TABLE IF EXISTS `tb_goodreceipt`;
CREATE TABLE `tb_goodreceipt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `receiptcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '订单编号',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '送餐地址',
  `sendtime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '送餐时间',
  `totalmoney` decimal(10, 2) DEFAULT 0.00 COMMENT '合计金额',
  `status` int(11) DEFAULT 0 COMMENT '订单状态(0:未付款；1:待配送；2:已完成)',
  `customername` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用户姓名',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '联系电话',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `chargetime` datetime(0) DEFAULT NULL COMMENT '下单时间',
  `peoplenum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用餐人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 195 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_goodreceipt
-- ----------------------------
INSERT INTO `tb_goodreceipt` VALUES (170, '205131415', 13, '201', '2018-10-23 11:00', 0.01, 1, '李飞', '18725036936', '', '2018-10-23 15:47:33', '1');
INSERT INTO `tb_goodreceipt` VALUES (171, '205307334', 16, '006', '2018-10-25 11:00', 0.01, 1, '哈哈哈', '15754310990', '', '2018-10-25 11:13:37', '2');
INSERT INTO `tb_goodreceipt` VALUES (172, '205341380', 13, '101', '2018-10-25 12:00', 0.01, 1, '基督教', '18725011111', '', '2018-10-25 11:17:41', '1');
INSERT INTO `tb_goodreceipt` VALUES (173, '205301722', 16, '006', '2018-10-25 11:00', 0.01, 1, '哈哈哈', '15754310990', '', '2018-10-25 11:25:09', '1');
INSERT INTO `tb_goodreceipt` VALUES (174, '205326110', 16, '006', '2018-10-25 11:00', 0.01, 1, '哈哈哈', '15754310990', '', '2018-10-25 11:38:30', '5');
INSERT INTO `tb_goodreceipt` VALUES (175, '205348237', 13, '101', '2018-10-25 12:30', 0.01, 0, '快捷键', '18725025825', '', NULL, '1');
INSERT INTO `tb_goodreceipt` VALUES (190, '205364284', 13, '101', '2018-10-25 18:00', 0.01, 0, '发过火', '18725025825', '', NULL, '2');
INSERT INTO `tb_goodreceipt` VALUES (191, '205344999', 13, '101', '2018-10-25 18:00', 0.01, 0, '发过火', '18725025825', '', NULL, '2');
INSERT INTO `tb_goodreceipt` VALUES (192, '205318209', 13, '102', '2018-10-26 18:00', 0.01, 0, '李', '18725025558', '', NULL, '1');
INSERT INTO `tb_goodreceipt` VALUES (193, '205385530', 16, '006', '2018-10-25 11:00', 0.01, 0, '哈哈哈', '15754431099', '', NULL, '2');
INSERT INTO `tb_goodreceipt` VALUES (194, '205368226', 16, '006', '2018-10-25 11:00', 0.01, 0, '哈哈哈', '15555555555', '', NULL, '2');

-- ----------------------------
-- Table structure for tb_goodrecepititem
-- ----------------------------
DROP TABLE IF EXISTS `tb_goodrecepititem`;
CREATE TABLE `tb_goodrecepititem`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `receiptid` int(11) DEFAULT 0 COMMENT '订单id',
  `goodid` int(11) DEFAULT 0 COMMENT '食品id',
  `goodname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '食品名称',
  `price` decimal(10, 2) DEFAULT 0.00 COMMENT '单价',
  `num` int(11) DEFAULT 0 COMMENT '数量',
  `totalmoney` decimal(10, 2) DEFAULT NULL COMMENT '合计金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 127 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_goodrecepititem
-- ----------------------------
INSERT INTO `tb_goodrecepititem` VALUES (102, 170, 41, '小白菜', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (103, 171, 41, '小白菜', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (104, 172, 41, '小白菜', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (105, 173, 41, '小白菜', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (106, 174, 41, '小白菜', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (107, 175, 42, '小笼包', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (122, 190, 42, '小笼包', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (123, 191, 42, '小笼包', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (124, 192, 42, '小笼包', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (125, 193, 42, '小笼包', 0.01, 1, 0.01);
INSERT INTO `tb_goodrecepititem` VALUES (126, 194, 42, '小笼包', 0.01, 1, 0.01);

-- ----------------------------
-- Table structure for tb_goodtype
-- ----------------------------
DROP TABLE IF EXISTS `tb_goodtype`;
CREATE TABLE `tb_goodtype`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `typename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '类别名称',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:显示；1:不显示)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品类别表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_goodtype
-- ----------------------------
INSERT INTO `tb_goodtype` VALUES (15, '饮料', '', 0, 0);
INSERT INTO `tb_goodtype` VALUES (16, '主食', '', 0, 0);

-- ----------------------------
-- Table structure for tb_homepicture
-- ----------------------------
DROP TABLE IF EXISTS `tb_homepicture`;
CREATE TABLE `tb_homepicture`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `isshow` int(11) DEFAULT 0 COMMENT '当前显示（0：是；1：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '首页图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_homepicture
-- ----------------------------
INSERT INTO `tb_homepicture` VALUES (7, 'ba458304-13a7-4765-a1c3-bf7786f05a13.jpg', '', '', 0, 0);

-- ----------------------------
-- Table structure for tb_hotelinfo
-- ----------------------------
DROP TABLE IF EXISTS `tb_hotelinfo`;
CREATE TABLE `tb_hotelinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '酒店名称',
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `settletime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '入住时间',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '电话',
  `isdefault` int(11) DEFAULT 0 COMMENT '显示状态(0:是；1:否)',
  `installation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '酒店设施',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '酒店简介',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '酒店信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_hotelinfo
-- ----------------------------
INSERT INTO `tb_hotelinfo` VALUES (37, '晏坐民宿', '', '3b0db89a-16c6-43f9-ba0f-8febe7ec0922.jpg,4180e5b7-4d13-4bdb-8390-04e7d1be3150.jpg,f20cb345-9369-43d2-ab2d-5b0abb388e31.jpg', '14：00', '', 0, '以上都有', '一处风景优美的……');

-- ----------------------------
-- Table structure for tb_refundreceipt
-- ----------------------------
DROP TABLE IF EXISTS `tb_refundreceipt`;
CREATE TABLE `tb_refundreceipt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `refundcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '退款单编号',
  `refundtime` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `refundreason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '退款理由',
  `status` int(11) DEFAULT 0 COMMENT '审核状态(0:待退款；1:退款成功)',
  `receiptid` int(11) DEFAULT 0 COMMENT '源订单id',
  `imgurl` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 80 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退款信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_refundreceipt
-- ----------------------------
INSERT INTO `tb_refundreceipt` VALUES (79, '205183773', '2018-10-23 17:10:01', '我就要退', 0, 204, '');

-- ----------------------------
-- Table structure for tb_roominfo
-- ----------------------------
DROP TABLE IF EXISTS `tb_roominfo`;
CREATE TABLE `tb_roominfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `typeid` int(11) DEFAULT 0 COMMENT '房间类型',
  `roomcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '房间编号',
  `roomname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '房间名称',
  `secretkey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '房间秘钥',
  `floor` int(255) DEFAULT 0 COMMENT '楼层',
  `iswindow` int(255) DEFAULT 0 COMMENT '是否有窗户(0:是；1:否)',
  `towards` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '朝向',
  `area` float(11, 0) DEFAULT 0 COMMENT '面积',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `status` int(11) DEFAULT 0 COMMENT '状态(0:空房；1:预定；2:入住)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房间信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_roominfo
-- ----------------------------
INSERT INTO `tb_roominfo` VALUES (16, 11, '001', '解放碑', '', 1, 0, '朝南', 25, 0, 0);
INSERT INTO `tb_roominfo` VALUES (17, 11, '003', '朝天门', '45581', 1, 0, '朝南', 25, 2, 2);
INSERT INTO `tb_roominfo` VALUES (18, 11, '002', '洋人街', '', 1, 0, '朝南', 25, 1, 0);
INSERT INTO `tb_roominfo` VALUES (19, 11, '006', '两路口', '98736', 1, 0, '朝南', 25, 5, 2);
INSERT INTO `tb_roominfo` VALUES (20, 11, '004', '磁器口', '65470', 1, 0, '朝南', 25, 3, 2);
INSERT INTO `tb_roominfo` VALUES (21, 11, '005', '沙坪坝1', '', 1, 0, '朝南', 25, 4, 2);
INSERT INTO `tb_roominfo` VALUES (22, 0, 'fsadf', 'sdf', '', 0, 0, '', 0, 0, 0);
INSERT INTO `tb_roominfo` VALUES (50, 12, '231', '123', '', 0, 0, '', 0, 0, 0);
INSERT INTO `tb_roominfo` VALUES (51, 11, '123', '123', '', 0, 0, '', 0, 0, 0);
INSERT INTO `tb_roominfo` VALUES (52, 13, '13213', '2312', '', 0, 0, '', 0, 0, 0);
INSERT INTO `tb_roominfo` VALUES (53, 12, 'asda', 'sdsa', '', 0, 0, '', 0, 0, 0);

-- ----------------------------
-- Table structure for tb_roomnotice
-- ----------------------------
DROP TABLE IF EXISTS `tb_roomnotice`;
CREATE TABLE `tb_roomnotice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `receiptid` int(11) DEFAULT 0 COMMENT '订单id',
  `sendtime` datetime(0) DEFAULT NULL COMMENT '发送时间',
  `senduser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '发送人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订房通知表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_roomnotice
-- ----------------------------
INSERT INTO `tb_roomnotice` VALUES (6, 24, 202, '2018-10-22 16:51:02', '');
INSERT INTO `tb_roomnotice` VALUES (7, 12, 203, '2018-10-23 15:39:29', '');
INSERT INTO `tb_roomnotice` VALUES (8, 17, 204, '2018-10-23 17:06:01', '');

-- ----------------------------
-- Table structure for tb_roomreceipt
-- ----------------------------
DROP TABLE IF EXISTS `tb_roomreceipt`;
CREATE TABLE `tb_roomreceipt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `receiptcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '单据编号',
  `customerid` int(11) DEFAULT 0 COMMENT '用户id',
  `totalshouldmoney` decimal(18, 2) DEFAULT 0.00 COMMENT '合计应收金额',
  `totalmoney` decimal(18, 2) DEFAULT 0.00 COMMENT '合计实收金额',
  `chargetime` datetime(0) DEFAULT NULL COMMENT '下单时间',
  `status` int(11) DEFAULT 0 COMMENT '订单状态(0=待付款,1=待入住,2=已入住,3=已完成,4=已评价,5=已退款)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `roomnum` int(11) DEFAULT 0 COMMENT '房间数量',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '入住人姓名',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '联系电话',
  `starttime` date DEFAULT NULL COMMENT '入住时间',
  `endtime` date DEFAULT NULL COMMENT '离开时间',
  `roomtype` int(11) DEFAULT 0 COMMENT '房间类型',
  `holdtime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '保留时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 247 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房间订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_roomreceipt
-- ----------------------------
INSERT INTO `tb_roomreceipt` VALUES (202, '205080384', 24, 0.01, 0.01, '2018-10-22 16:51:01', 4, '', 1, '田亮', '18725187254', '2018-10-22', '2018-10-23', 11, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (203, '205174689', 12, 0.01, 0.01, '2018-10-23 15:39:27', 2, '', 1, '李飞', '18725036936', '2018-10-23', '2018-10-24', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (204, '205106553', 17, 0.01, 0.01, '2018-10-23 17:06:00', 4, '', 1, '倔强', '15743100909', '2018-10-23', '2018-10-24', 12, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (205, '205371042', 34, 0.01, 0.01, '2018-10-25 14:58:45', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (206, '205306757', 34, 0.01, 0.01, '2018-10-25 14:58:46', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (207, '205317252', 34, 0.01, 0.01, '2018-10-25 14:58:59', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (208, '205313007', 34, 0.01, 0.01, '2018-10-25 14:58:59', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (209, '205324139', 34, 0.01, 0.01, '2018-10-25 14:58:59', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (210, '205354420', 34, 0.01, 0.01, '2018-10-25 14:59:00', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (211, '205377109', 34, 0.01, 0.01, '2018-10-25 14:59:00', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (212, '205316756', 34, 0.01, 0.01, '2018-10-25 14:59:00', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (213, '205353461', 34, 0.01, 0.01, '2018-10-25 14:59:18', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (214, '205361674', 34, 0.01, 0.01, '2018-10-25 14:59:17', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (215, '205331529', 34, 0.01, 0.01, '2018-10-25 14:59:18', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (216, '205335523', 34, 0.01, 0.01, '2018-10-25 14:59:18', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (217, '205360240', 34, 0.01, 0.01, '2018-10-25 14:59:18', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (218, '205306059', 34, 0.01, 0.01, '2018-10-25 14:59:18', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (219, '205314034', 34, 0.01, 0.01, '2018-10-25 14:59:18', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (220, '205381304', 34, 0.01, 0.01, '2018-10-25 14:59:19', 0, '', 1, '不谢不谢哈', '15754313188', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (221, '205381136', 24, 0.01, 0.01, '2018-10-25 15:07:13', 0, '', 1, '李飞', '18725182526', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (222, '205325763', 24, 0.01, 0.01, '2018-10-25 15:07:21', 0, '', 1, '李飞', '18725182526', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (223, '205386354', 24, 0.01, 0.01, '2018-10-25 15:07:42', 1, '', 1, '李飞', '18725182526', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (224, '205356220', 24, 0.01, 0.01, '2018-10-25 15:08:12', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (225, '205330423', 24, 0.01, 0.01, '2018-10-25 15:08:21', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (226, '205318479', 24, 0.01, 0.01, '2018-10-25 15:08:22', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (227, '205312271', 24, 0.01, 0.01, '2018-10-25 15:08:22', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (228, '205329175', 24, 0.01, 0.01, '2018-10-25 15:09:19', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (229, '205366705', 24, 0.01, 0.01, '2018-10-25 15:09:19', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (230, '205377390', 24, 0.01, 0.01, '2018-10-25 15:09:19', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (231, '205380346', 24, 0.01, 0.01, '2018-10-25 15:09:20', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (232, '205362359', 24, 0.01, 0.01, '2018-10-25 15:09:20', 0, '', 1, '李飞', '18725182523', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (233, '205343482', 17, 0.01, 0.01, '2018-10-25 15:48:35', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (234, '205300500', 17, 0.01, 0.01, '2018-10-25 15:48:36', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (235, '205301194', 17, 0.01, 0.01, '2018-10-25 15:48:36', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (236, '205393939', 17, 0.01, 0.01, '2018-10-25 15:48:36', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (237, '205308939', 17, 0.01, 0.01, '2018-10-25 15:48:36', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (238, '205327461', 17, 0.01, 0.01, '2018-10-25 15:48:43', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (239, '205383673', 17, 0.01, 0.01, '2018-10-25 15:48:42', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (240, '205361634', 17, 0.01, 0.01, '2018-10-25 15:48:43', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (241, '205338500', 17, 0.01, 0.01, '2018-10-25 15:48:43', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (242, '205398455', 17, 0.01, 0.01, '2018-10-25 15:48:43', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (243, '205381933', 17, 0.01, 0.01, '2018-10-25 15:48:44', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (244, '205390891', 17, 0.01, 0.01, '2018-10-25 15:48:45', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (245, '205383661', 17, 0.01, 0.01, '2018-10-25 15:48:45', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');
INSERT INTO `tb_roomreceipt` VALUES (246, '205367557', 17, 0.01, 0.01, '2018-10-25 15:48:45', 0, '', 1, '很时尚', '15754310990', '2018-10-25', '2018-10-26', 13, '14:00');

-- ----------------------------
-- Table structure for tb_roomtype
-- ----------------------------
DROP TABLE IF EXISTS `tb_roomtype`;
CREATE TABLE `tb_roomtype`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `typename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '类型名称',
  `stock` int(11) DEFAULT 0 COMMENT '库存数量',
  `price` decimal(10, 2) DEFAULT 0.00 COMMENT '价格',
  `floor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '所在楼层',
  `format` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '规格',
  `describe` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `status` int(11) DEFAULT 0 COMMENT '状态(0:上架；1:下架)',
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标签',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房间类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_roomtype
-- ----------------------------
INSERT INTO `tb_roomtype` VALUES (11, '大床房', 13, 0.01, '1', '30', '风景独特', 0, '8637cdf9-7840-43d6-8acc-6075c7467d17.jpg', '', 0);
INSERT INTO `tb_roomtype` VALUES (12, '标间', 1, 0.01, '2', '45', '风景独特', 0, '6cbe9963-652c-4007-83b4-5f8e0f5eaa07.jpg', '', 0);
INSERT INTO `tb_roomtype` VALUES (13, '套房', 9, 0.01, '1-2楼', '', '', 0, '2e0b2850-e767-4ba2-a60e-30d37eb9798b.jpg', '', 0);

-- ----------------------------
-- Table structure for tb_roomuser
-- ----------------------------
DROP TABLE IF EXISTS `tb_roomuser`;
CREATE TABLE `tb_roomuser`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `roomid` int(11) DEFAULT 0 COMMENT '房间id',
  `userid` int(11) DEFAULT 0 COMMENT '用户id',
  `createtime` datetime(0) DEFAULT NULL COMMENT '绑定时间',
  `status` int(11) DEFAULT 0 COMMENT '入住状态(0:未入住；1:已入住)',
  `settlenum` int(11) DEFAULT 0 COMMENT '入住次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房间用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_roomuser
-- ----------------------------
INSERT INTO `tb_roomuser` VALUES (7, 20, 16, '2018-10-24 09:21:46', 0, 0);
INSERT INTO `tb_roomuser` VALUES (8, 19, 16, '2018-10-25 10:23:49', 1, 1);

-- ----------------------------
-- Table structure for tb_scape
-- ----------------------------
DROP TABLE IF EXISTS `tb_scape`;
CREATE TABLE `tb_scape`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '景区名称',
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '联系电话',
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图片',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示(0:显示，1:不显示)',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '景区信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_servertel
-- ----------------------------
DROP TABLE IF EXISTS `tb_servertel`;
CREATE TABLE `tb_servertel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `staffname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '客服名称',
  `tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '电话号码',
  `isdefault` int(11) DEFAULT 0 COMMENT '当前客服(0:是；1:否)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客服电话表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_servertel
-- ----------------------------
INSERT INTO `tb_servertel` VALUES (6, '222', '12345678910', 0, '');

-- ----------------------------
-- Table structure for tb_surrounding
-- ----------------------------
DROP TABLE IF EXISTS `tb_surrounding`;
CREATE TABLE `tb_surrounding`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `itype` int(255) DEFAULT 0 COMMENT '类型（1：景区，2：交通；3：其他）',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '名称',
  `contents` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '内容',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `isshow` int(11) DEFAULT 0 COMMENT '是否显示（0：显示；1：不显示）',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '周边信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_surrounding
-- ----------------------------
INSERT INTO `tb_surrounding` VALUES (10, 1, '湖', '大湖', 0, 0, '');

-- ----------------------------
-- Table structure for tb_typeprice
-- ----------------------------
DROP TABLE IF EXISTS `tb_typeprice`;
CREATE TABLE `tb_typeprice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `typeid` int(11) DEFAULT 0 COMMENT '类型id',
  `starttime` date DEFAULT NULL COMMENT '开始时间',
  `endtime` date DEFAULT NULL COMMENT '结束时间',
  `prices` decimal(10, 2) DEFAULT 0.00 COMMENT '价格',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房型价格表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_typeprice
-- ----------------------------
INSERT INTO `tb_typeprice` VALUES (9, 11, '2018-10-03', '2018-10-11', 111.00, '');

-- ----------------------------
-- Table structure for tb_wxappletsuserinfo
-- ----------------------------
DROP TABLE IF EXISTS `tb_wxappletsuserinfo`;
CREATE TABLE `tb_wxappletsuserinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户标识',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '昵称',
  `avatarurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '头像',
  `sex` int(11) DEFAULT 0 COMMENT '性别(0:未知；1:男；2:女)',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所在城市',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所在省份',
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所在国家',
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '语言',
  `authtime` datetime(0) DEFAULT NULL COMMENT '授权时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '小程序用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_wxappletsuserinfo
-- ----------------------------
INSERT INTO `tb_wxappletsuserinfo` VALUES (7, 'oZVYo4-Vz_a90H5RDhediokmBjr0', 'HX', 'https://wx.qlogo.cn/mmopen/vi_32/yoicO3BQGjGJgYNaFtX16Se0UicHyghBBcmuXK99Op6DWL0Zp03zNFiaY8wkFfZeh3dlHzs29dRqvWaaHwOc1B2dg/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-10-12 14:31:43', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (8, 'oZVYo4x8v_RRn6L3BPfhgzgjjzaI', '西南偏南ccj', 'https://wx.qlogo.cn/mmopen/vi_32/tqwH1vzc3ZGBQKwLmIpYsHIeHjktFvXsYbjJCuibXc4pYnAia93mChdnZIs8cia3QRJS2LBkZqCSicnPqfBQRWzgxA/132', 1, '', '巴塞罗那', '西班牙', 'zh_CN', '2018-10-12 15:11:10', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (10, 'oZVYo4yzIdfjnQ3uo9hX2LMgsAEI', '秦明', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI39XReVrENha89NJuf6bwRVnHDxbmQicnj2le4xqRviaJWoFDBn23BvNSbvrKoZoiau1zY9cxjeCTWg/132', 0, '', '', '', 'zh_CN', '2018-09-25 11:57:54', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (11, 'oZVYo4-ABg-uxy5U0MIcUMJPR9Jk', '李昱辰', 'https://wx.qlogo.cn/mmopen/vi_32/PAPHBnl1rvibEyJGBCGuXhUHiayPEoae603SibyjLejXJQyRwzkGWXmSkv0RibfzibT3ROls0etx64KVfpiaQXiaZ6yuA/132', 1, '杭州', '浙江', '中国', 'zh_CN', '2018-09-25 14:05:29', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (12, 'oZVYo4z5srWWnlgiX-JiE9GXggJ0', '飞', 'https://wx.qlogo.cn/mmopen/vi_32/JQOmVGoj832XLaBrPxDMzL3CfabZ3tW16YGhEicUe80kFRHQrm693UANyfBicgZuDwyJxjHoZpbVpTQBeibmZjqzA/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-10-25 14:06:49', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (13, 'oZVYo4_yjFkx5XC_Mt3RZQ2q6enU', '陈启', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLEJ36Ayx0nBB2aUOP1Bh874nM30uA6k0ewiaS1vIPkkIm8DSYLeCy46U0swbtQPN5SkOaGWAmpia9g/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-09-25 14:12:00', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (14, 'oZVYo46VbnY5guX4jydy6vpFM7dU', '小布', 'https://wx.qlogo.cn/mmopen/vi_32/lD37QlfG38e3ZvOOKQcLu2x6NAXtY7diccnWuJOw2En7KgcPO5Fa59MbJBRhNjmm30kXHZtJ8RJ8ZGyRibDgfCfg/132', 1, '曲靖', '云南', '中国', 'zh_CN', '2018-09-26 16:59:55', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (15, 'oZVYo45FNqhLUxt_c4mfg8dJaZCM', '张清', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKPWuQJN0ibDBhKL1d10XGicdXIPQRAwW5V3WEhSh86L9ghIusY9iaJnHeribIFBk1kejibDKpBwyYsdoQ/132', 0, '', '', '', 'zh_CN', '2018-09-25 16:34:03', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (16, 'oZVYo45uWgAmJHRLIp8YMiXQ4RwU', '李应', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIcuN0dyZaibzxHfVs2ic01LPNVrOqia12T55LLsRF6jwRzUyicorQjHeCSpkkTkWmbiaMkeXLtrFquzBw/132', 0, '', '', '', 'zh_CN', '2018-09-25 18:19:58', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (17, 'oZVYo44PJ2Dfy7WNH2OPHA1k5UhY', '哈哈哈', 'https://wx.qlogo.cn/mmopen/vi_32/QcYwj83bbs2a2Od30dpBctwODibRcvpKczuT4EedqOiahPyU3JheughySeZLlVOFHJzexdTJze9OERibJhDxghZ9Q/132', 2, '昆明', '云南', '中国', 'zh_CN', '2018-10-25 16:04:47', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (19, 'oZVYo42zwJo7o4b75HHXlZT9ZgBA', '恒', 'https://wx.qlogo.cn/mmopen/vi_32/klXzOkxhSicpViaAEAuyYcYibJy0Paz9R0NALTfnsuLFiaODZ5C18hgHvDCbeicfPebfnYy00PDCibqWa8xd1qcQXsVA/132', 0, '', '', '', 'zh_CN', '2018-09-26 15:30:06', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (20, 'oZVYo4-KsvU0vfz77euE99jXybIM', '平常心', 'https://wx.qlogo.cn/mmopen/vi_32/tfpMlTywv12L2hkgic6MZRLs9VT0zWJ4txx92fzThhHAtjDmo7xznnLn9C7OZjja4VH5WYiboOnUQLZ3rYqu1uoA/132', 2, '自贡', '四川', '中国', 'zh_CN', '2018-09-26 15:32:37', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (21, 'oZVYo4x6z5vLm05ruFN65LWWpm1c', '嘿咻！', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLUzAeUNt88ficuWq60oas6yA8PveQ19gLkYzjibJwxyPjNoTkEhvhSdh9su4R7MAdicHYTJqYRYic51Q/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-09-26 16:50:18', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (22, 'oZVYo4yNw4vDnv_Qm7Fyr2XRl6n0', '🍓', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDo0e74cnXic01anicbmYnY5fjmoQDjic2J2GsVOj6bKWH7Tpz9TjS3WWzIib9xpseA3dpAgq8Hia100g/132', 2, '', '', '关岛', 'zh_CN', '2018-09-26 16:53:17', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (23, 'oZVYo45Z2KXFhs4OiiZvgfgN7HpY', 'HR、', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKOCS1HBgCPBf5TvUDuhJvicI6BssPKrUdDvdibM9VHb13TUzTVCZFF6tu0lAib3dGXjZDl2V4uHoGYw/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-09-26 16:58:20', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (24, 'oZVYo4z0ksLGBQorFngmCb75rTiA', '甜品', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJWtciayReiaLIpaiaT18f2fbJ8mfnLQheTNthO87mDYMYrgWsTwDsWT6KkT6jmiajqzHhYtdpcnGPduw/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-09-26 17:02:52', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (25, 'oZVYo42c__XPUdPy7uBOQd7wYGqE', '我', 'https://wx.qlogo.cn/mmopen/vi_32/2Kic7OLRKdnHqvKSnmq4tk4W1MVC9KXheB6dqLV2qY5VCGoQdCf9gscTQoia66HoE7JcwPnxCxWytLKZibWWBmZLQ/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-09-26 17:03:05', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (26, 'oZVYo4zTkSLyyJyu7ox6oAyAW5TQ', '柴进', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIcu5pBsZPqcELnAqYNzn3hXfxSQg9EnGcZ5ZuMB3p53icFzEKfQibG4IY9fs5f2ib8BywuKk8ibObibrg/132', 0, '', '', '', 'zh_CN', '2018-09-26 17:10:41', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (27, 'oZVYo48Yb5sehgA5EHtmBxFpghCA', '董平', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJqmibkz27DKeG1dgjSUcjQRGOtJdUOlPRx5NjVxrlmHpFRhLEQ8GB1vrsWKFFuPygOMMyVu8Giao3A/132', 0, '', '', '', 'zh_CN', '2018-09-26 18:23:41', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (28, 'oZVYo4-4CoqQ7ZDMaCzmi9HbZL0M', '吖頭', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKX879Vwuv3tBqLCUzw1Oq6W2dG6MPy9L24zY6bx2lN4zibsodIJ3fGZ9kSwnJNdiaU5siamPEMXDF0Q/132', 2, '', '', '新加坡', 'zh_CN', '2018-09-26 20:04:05', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (29, 'oZVYo49TNecX6VkinDB5m3P_JQNE', '🇱 🇴 🇻 🇪 『🐷 猪』', 'https://wx.qlogo.cn/mmopen/vi_32/NDZOjfCogce6KUmtxMu801DUm7zYxMf0RVOUjxEfHbtPS2kLr6pQGrzH0ibeP38vIIyOiaX7LHA7MHibj3E8wEOsg/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-09-26 20:07:38', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (30, 'oZVYo45Pi1CTT3BTT_3tAWEBaIdo', '卢俊义', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKWFom0HvwlCXfibGFkg028UoBcslPmx8rRAUo6RXgg172YNCAlHsAmvZ6ibCZZZ3ax3PLJmF9w7fsA/132', 0, '', '', '', 'zh_CN', '2018-09-27 09:46:38', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (31, 'oZVYo45t-M9LWvfBthw5VX9zHg1U', '瓶邪、🇨🇳', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLSzCjD9Clpsty8l7t5HtFposgH1NzOAv5yqibBQBXOicFiaJiblWFZQUPQAtEc2tTWavJwKYAOKuBMYA/132', 1, '汉中', '陕西', '中国', 'zh_CN', '2018-09-27 09:52:15', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (32, 'oZVYo472UYE8DmP5vSNZ_dgwJB4w', '鵼', 'https://wx.qlogo.cn/mmopen/vi_32/ic9v1meib9tApkWyHDibBV4hOo4RhJmLakAf2vDRSc3Pq4Da5VnQpicicgujXCjkibXSPSxdAgibpgTsJS7wucfrNcNicA/132', 2, 'Others', 'Ehime-ken', 'JP', 'zh_CN', '2018-09-27 17:16:05', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (33, 'oZVYo432nKfTC_CiexL9ynblMPzc', 'A   360推广江添翼15288209451', 'https://wx.qlogo.cn/mmopen/vi_32/KFlmibV8hcFaIZOibrJXC5iaFLFPKzKdyQah62aTWkq6LUGcZ2zswaoamA4Grb85DRuFpx7mg9VJOnoy0P6znSu6w/132', 0, '', '', '', 'zh_CN', '2018-09-27 18:06:34', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (34, 'oZVYo43gq7W3inv-JjjAc8XFZ6W0', '🍘', 'https://wx.qlogo.cn/mmopen/vi_32/icmsfz1so8owqsgq9lYsGK15wiaXXCMtwoWxYibEjSiaDiceGUaoPQae5eHCh9fx7z5r2uR3eTFaBTsfNAgJIhqCvicQ/132', 2, '', '', '南极洲', 'zh_CN', '2018-09-29 09:49:54', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (35, 'oZVYo45wM-8M61oxstB2BCDziw3E', '馬夋', 'https://wx.qlogo.cn/mmopen/vi_32/qxWEIjy9XYeOxEqHHZV8ZSNGf7a2wicLG940C75BEX9EfTwzxT8Dkpwiayt9UwgXIw3A9NJtzh0QZLJj9ibibXRkQw/132', 1, '无锡', '江苏', '中国', 'zh_CN', '2018-09-29 19:39:56', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (36, 'oZVYo49_zOs-1LP1ZtESr9jwfRpU', '何万里', 'https://wx.qlogo.cn/mmopen/vi_32/kDbBxASmTvHiaelqyCfRLHJeoWk3XfGYseVoz78KpjQImxKnT91Hua1K6HFNOclGBYxRibnZ5rjZbFdbWHYNzWCA/132', 1, '成都', '四川', '中国', 'zh_CN', '2018-09-30 01:34:20', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (37, 'oZVYo40DhKSFYV32ZnCGZtt2O-is', '呼延灼', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJZPdAzRMUVto6tkzWxwd0ZwvRla3Rpqeuibq3T4l5dWMcSufEzB6kydJ4ichbtQEMSOT3kUzNGmEqQ/132', 0, '', '', '', 'zh_CN', '2018-10-02 09:50:12', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (38, 'oZVYo4ya0UWZs1UsCODMK3PhI50I', '雅', 'https://wx.qlogo.cn/mmopen/vi_32/qXV3XCvULia7nMuMHfAbbMZ7Atn25kyFI3MX07NYeMTyAice1uoQibOnlmGY3BIrEJv4qe4Z52nMuk82KWZC46DEg/132', 2, '成都', '四川', '中国', 'zh_CN', '2018-10-05 22:01:36', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (39, 'oZVYo47FTZ3t0I-AjUDXEuW9qcdo', '罗铭', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ4V30MKQQP0qQZr6dmVODib77WwWIF3FISGxwSKrT4MnYRMmuz2sfQbbmtx205mUEmF1rwuicJsOjQ/132', 0, '长沙', '湖南', '中国', 'zh_CN', '2018-10-08 08:31:05', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (40, 'oZVYo4yGainJ8xfBQuIKiEgUPvSQ', '请叫我女王', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIBWN7ibZHRPm3ZCsxibrh1EFJ0jLDXWl4mhuJ0WkA5ibFbZQjA1UriaSkG3ZavbM3UJcWIfWVDcmrkGA/132', 2, '广州', '广东', '中国', 'zh_CN', '2018-10-09 13:05:30', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (41, 'oZVYo4421Z9mDCGzFNjpzPM8ofzQ', '朱仝', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJqmibkz27DKeOCZibm0eVAGUpEd1bCibUOia0L1qgjKUziaJTFc0Lz8kNCrtTLQibfFgWibjLy06Jzf4JKw/132', 0, '', '', '', 'zh_CN', '2018-10-09 17:37:28', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (42, 'oZVYo48v0eXtGaYnT-Fr75fQQo_w', '吴用', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKl62qzuUxaszEkOp76hpZRN6kRiaCzh37XPXjxhuYX5GZyS6ERUJ85UAgtPzr4NMtW1IAyyLygibPQ/132', 0, '', '', '', 'zh_CN', '2018-10-11 16:01:25', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (43, 'oZVYo4x2LxTF9QOZi-mx49B3J66I', '朱晋', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep2zxmRZDTC25OSU8gps1eNC32oXshYib7BYv8HQJddk35NCp3k3J0WdoHA0gvQfW6883ictakiaMkkg/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-10-22 14:53:30', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (44, 'oZVYo41zWQU0eI7fzBqoDpDCSTBs', 'Like A G 6', 'https://wx.qlogo.cn/mmopen/vi_32/sVNChNl8RvvIOsUbzpzm1pMG60rRuru6YjIe53qibFp7hxAsicx4mDRI7FoQV5NByLIpQZRvKzfKhAFVIziaNy4jg/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-10-24 15:31:27', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (45, 'oZVYo42n3mBB36H11H5ogwjgJht0', 'peng', 'https://wx.qlogo.cn/mmopen/vi_32/BJauHx0FuYPKGYpAUhzfTYMJpMJj8GTKAokajicaxlfqZdlZ2hQh6JmET7NTHvTYxgf2vic6RAUT9woE96w5pcHw/132', 1, '昆明', '云南', '中国', 'zh_CN', '2018-10-24 15:39:08', '');
INSERT INTO `tb_wxappletsuserinfo` VALUES (46, 'oZVYo440eRHeG8AOaJY2aN0_IWZw', '小可红领巾', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoibWovh8LSDIicgTtQZHR2klQ5SodhL8NrHEuIxCw54qZ7V2kjRqwVFXvQPyQ7M3dtLezd6cp3blXQ/132', 1, '', '', '安道尔', 'zh_CN', '2018-10-24 16:42:50', '');

-- ----------------------------
-- Table structure for tb_wxuserinfo
-- ----------------------------
DROP TABLE IF EXISTS `tb_wxuserinfo`;
CREATE TABLE `tb_wxuserinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '普通用户的标识',
  `subscribe` int(11) DEFAULT 0 COMMENT '是否订阅',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '昵称',
  `sex` int(11) DEFAULT 0 COMMENT '性别(1:男；2:女)',
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所用语言',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所在城市',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所在省份',
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '所在国家',
  `headimgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '头像地址',
  `subscribe_time` datetime(0) DEFAULT NULL COMMENT '最后一次关注时间',
  `unionid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '公众号绑定id',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `groupid` int(11) DEFAULT 0 COMMENT '分组id',
  `qrurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '个人二维码',
  `imagepath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '条形码路径',
  `refreshtime` datetime(0) DEFAULT NULL COMMENT '刷新时间(每个用户每天刷新一次)',
  `isstaff` int(11) DEFAULT 0 COMMENT '是否公司员工(0：否；1：是)',
  `isdfnotice` int(11) DEFAULT 0 COMMENT '订房通知(0：否；1：是)',
  `isdcnotice` int(11) DEFAULT 0 COMMENT '订餐通知(0：否；1：是)',
  `isdpnotice` int(11) DEFAULT 0 COMMENT '点评通知(0：否；1：是)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_wxuserinfo
-- ----------------------------
INSERT INTO `tb_wxuserinfo` VALUES (13, 'ohrqY0XGYBMogkaHCHOaCJOgFSLk', 1, '飞', 1, 'zh_CN', '昆明', '云南', '中国', 'http://thirdwx.qlogo.cn/mmopen/AbNexq9nckmORHo0qGWAeQpYI0wktKSicGTtXqVfZBJCmLGT62tNVdZAfuxhUIgx06st3h6faiavSsjHJ4b3BACsjbbr5NwunY/132', '2018-10-08 15:14:43', '', '', 0, '', '', NULL, 1, 0, 1, 1);
INSERT INTO `tb_wxuserinfo` VALUES (14, 'ohrqY0WP_ETa1-sRV-zai80tUFHM', 1, 'HX', 1, 'zh_CN', '昆明', '云南', '中国', 'http://thirdwx.qlogo.cn/mmopen/AbNexq9nckl9PWvxfWYHIfYpQtsCK8KyrVCoEk16AJu8Ljfib3ibhhAFmzmsUHGibNiboPcC1CD552bARjFshZQxkCq9WfxY8g1v/132', '2018-10-08 15:37:47', '', '', 0, '', '', NULL, 0, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (15, 'ohrqY0Qs1J4Po0rJIHw6Syed69ss', 1, '西南偏南ccj', 1, 'zh_CN', '', '巴塞罗那', '西班牙', 'http://thirdwx.qlogo.cn/mmopen/tvZyVUtBDIia1W0Obe5tL0wGWn7LT30Hn0rv0icstr7zlzHYJh3nApzE0poWNo8icZe5uO3lUa1NoUquJVxI9KJjMtXPQQfKzvh/132', '2018-10-08 17:36:40', '', '', 0, '', '', NULL, 0, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (16, 'ohrqY0X-M7G8VsWTHVvRReNDifnI', 1, '哈哈哈', 2, 'zh_CN', '昆明', '云南', '中国', 'http://thirdwx.qlogo.cn/mmopen/tvZyVUtBDIiaMDiawOXiaoV99MIPgFibw1iaAfeFMCcYEKSN3LL12yX4buvk5LHyDclTZv67e8yOdtuTZnYjzvlichGJZDCo43UibMF/132', '2018-10-09 16:50:24', '', '', 0, '', '', NULL, 1, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (17, 'ohrqY0RQ5CBorX12PKolv48Q1_oc', 1, '王恩明', 2, 'zh_CN', '宜昌', '湖北', '中国', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJU2fsLm98lIWO419pmIKPAQ3EmRkLYFIf8WVbqAV3tSMTKqwQMJymCfibAZneRZXEBlx3KKW1EnaQ/132', '2018-10-09 17:55:58', '', '', 0, '', '', NULL, 0, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (18, 'ohrqY0bCAjckzJGtGUKSvPagzGkw', 1, '小可红领巾', 1, 'zh_CN', '', '', '安道尔', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJaMUw9o3ZXU35ODy5ic9lTJplK9JwHRlJvL6mFTNswazYR7IKQOaV8yhXrAgI6Z7cZI1dv41WTibtQ/132', '2018-10-17 21:16:43', '', '', 0, '', '', NULL, 0, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (19, 'ohrqY0Ti7cpGO80lyM4ezqw-VP1Q', 1, '瓶邪、🇨🇳', 1, 'zh_CN', '汉中', '陕西', '中国', 'http://thirdwx.qlogo.cn/mmopen/tvZyVUtBDIiacNCoDnsykBLa3y1O8f5pNUlK1ps8ea64yQEC6TALqQia7xlVvPweYneOtSXTS5R4HabnjFhW1icnPLAcLWwH0Km/132', '2018-10-22 15:25:22', '', '', 0, '', '', NULL, 0, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (20, 'ohrqY0Q6wUIqaYCgLwa5iP49jJ-Q', 1, '野 泽 。', 2, 'zh_CN', '', '', '关岛', 'http://thirdwx.qlogo.cn/mmopen/AbNexq9ncknhRPruXWmxhxSUDrc4UWvZN488YLxeRibt8aMs1sPUgAatmx8AP5tvQWer1iaXGVtP7icbHUa0wewbZ9nqVCBou5d/132', '2018-10-25 15:14:29', '', '', 0, '', '', NULL, 0, 0, 0, 0);
INSERT INTO `tb_wxuserinfo` VALUES (21, 'ohrqY0UZgP1df6tD1G-cxn2YoFzM', 0, '酒 篱 。', 2, 'zh_CN', '', '', '南极洲', 'http://thirdwx.qlogo.cn/mmopen/AbNexq9nckkdWt9unTZARbQI45qmgeoia08MunauZjn9Thf8GZFxgNicrmVkvbicbTxBrJgYmotPnFZXMNdciabJ1SoscPvC0e6C/132', '2018-10-25 15:19:04', '', '', 0, '', '', NULL, 0, 0, 0, 0);

SET FOREIGN_KEY_CHECKS = 1;
