package io.youngking.modules.typeprice.dao;

import io.youngking.common.utils.PageUtils;
import io.youngking.modules.typeprice.entity.TypepriceEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 房型价格表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-20 09:49:46
 */
@Mapper
public interface TypepriceDao extends BaseMapper<TypepriceEntity> {
    List<Map<String,Object>> getlist(Map<String, Object> params);
}
