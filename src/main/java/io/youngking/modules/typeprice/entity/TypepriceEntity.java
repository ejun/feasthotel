package io.youngking.modules.typeprice.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 房型价格表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-20 09:49:46
 */
@TableName("tb_typeprice")
public class TypepriceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 类型id
	 */
	private Integer typeid;
	/**
	 * 开始时间
	 */
	private Date starttime;
	/**
	 * 结束时间
	 */
	private Date endtime;
	/**
	 * 价格
	 */
	private BigDecimal prices;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类型id
	 */
	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}
	/**
	 * 获取：类型id
	 */
	public Integer getTypeid() {
		return typeid;
	}
	/**
	 * 设置：开始时间
	 */
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	/**
	 * 获取：开始时间
	 */
	public Date getStarttime() {
		return starttime;
	}
	/**
	 * 设置：结束时间
	 */
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	/**
	 * 获取：结束时间
	 */
	public Date getEndtime() {
		return endtime;
	}
	/**
	 * 设置：价格
	 */
	public void setPrices(BigDecimal prices) {
		this.prices = prices;
	}
	/**
	 * 获取：价格
	 */
	public BigDecimal getPrices() {
		return prices;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
