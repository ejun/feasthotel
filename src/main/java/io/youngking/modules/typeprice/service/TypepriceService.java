package io.youngking.modules.typeprice.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.typeprice.entity.TypepriceEntity;

import java.util.List;
import java.util.Map;

/**
 * 房型价格表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-20 09:49:46
 */
public interface TypepriceService extends IService<TypepriceEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils Pagelist(Map<String, Object> params);

    List<Map<String,Object>> getlist(Map<String, Object> params);
}

