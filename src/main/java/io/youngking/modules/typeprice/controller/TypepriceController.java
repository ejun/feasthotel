package io.youngking.modules.typeprice.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.typeprice.entity.TypepriceEntity;
import io.youngking.modules.typeprice.service.TypepriceService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;

import javax.servlet.http.HttpServletRequest;


/**
 * 房型价格表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-20 09:49:46
 */
@RestController
@RequestMapping("typeprice/typeprice")
public class TypepriceController {
    @Autowired
    private TypepriceService typepriceService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("typeprice:typeprice:list")
    public R list(@RequestParam Map<String, Object> params) {
        int currPage = 0;
        int limit = 0;

        Map<String, Object> map = new HashMap<>();
        //分页参数
        if (params.get("page") != null) {
            currPage = Integer.parseInt((String) params.get("page"));
        }
        if (params.get("limit") != null) {
            limit = Integer.parseInt((String) params.get("limit"));
        }
        map.put("key", params.get("key"));
        map.put("offset", (currPage - 1) * limit);
        map.put("limit", limit);

        //获取数据
        List<Map<String, Object>> list = typepriceService.getlist(map);
        Map<String, Object> params1 = new HashMap<>();
        params1.put("list", list);
        params1.put("CurrPage", currPage);
        params1.put("PageSize", limit);
        params1.put("TotalCount", list.size());
        params1.put("TotalPage", (int) Math.ceil((double) list.size() / limit));

        PageUtils page = typepriceService.Pagelist(params1);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("typeprice:typeprice:info")
    public R info(@PathVariable("id") Integer id) {
        TypepriceEntity typeprice = typepriceService.selectById(id);

        return R.ok().put("typeprice", typeprice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("typeprice:typeprice:save")
    public R save(@RequestBody Map<String, Object> params) throws ParseException {
        String typeid = params.get("typeid").toString();
        String starttime = params.get("starttime").toString();
        String endtime = params.get("endtime").toString();
        String prices = params.get("prices").toString();
        String remark = params.get("remark").toString();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        TypepriceEntity typepriceEntity = typepriceService.selectOne(
                new EntityWrapper<TypepriceEntity>()
                        .where("starttime<={0}", starttime)
                        .andNew("endtime>={0}", starttime)
                        .andNew("typeid={0}",typeid));
        TypepriceEntity typepriceEntity1 = typepriceService.selectOne(
                new EntityWrapper<TypepriceEntity>()
                        .where("starttime<={0}", endtime)
                        .andNew("endtime>={0}", endtime)
                        .andNew("typeid={0}",typeid));
        TypepriceEntity typepriceEntity2 = typepriceService.selectOne(
                new EntityWrapper<TypepriceEntity>()
                        .where("starttime>={0}", starttime)
                        .andNew("endtime<={0}", endtime)
                        .andNew("typeid={0}",typeid));
        if (typepriceEntity != null || typepriceEntity1 != null || typepriceEntity2 != null) {
            return R.error("时段冲突，请核对后再进行操作");
        }
        TypepriceEntity typeprice = new TypepriceEntity();
        typeprice.setTypeid(Integer.parseInt(typeid));
        typeprice.setStarttime(df.parse(starttime));
        typeprice.setEndtime(df.parse(endtime));
        typeprice.setPrices(BigDecimal.valueOf(Float.parseFloat(prices)));
        typeprice.setRemark(remark);
        typepriceService.insert(typeprice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("typeprice:typeprice:update")
    public R update(@RequestBody Map<String, Object> params) throws ParseException {
        String id = params.get("id").toString();
        String typeid = params.get("typeid").toString();
        String starttime = params.get("starttime").toString();
        String endtime = params.get("endtime").toString();
        String prices = params.get("prices").toString();
        String remark = params.get("remark").toString();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        TypepriceEntity typeprice = typepriceService.selectById(id);

        TypepriceEntity typepriceEntity = typepriceService.selectOne(
                new EntityWrapper<TypepriceEntity>()
                        .where("starttime<={0}", starttime)
                        .andNew("endtime>={0}", starttime)
                        .andNew("typeid={0}",typeid)
                        .andNew("id<>{0}",typeprice.getId()));
        TypepriceEntity typepriceEntity1 = typepriceService.selectOne(
                new EntityWrapper<TypepriceEntity>()
                        .where("starttime<={0}", endtime)
                        .andNew("endtime>={0}", endtime)
                        .andNew("typeid={0}",typeid)
                        .andNew("id<>{0}",typeprice.getId()));
        TypepriceEntity typepriceEntity2 = typepriceService.selectOne(
                new EntityWrapper<TypepriceEntity>()
                        .where("starttime>={0}", starttime)
                        .andNew("endtime<={0}", endtime)
                        .andNew("typeid={0}",typeid)
                        .andNew("id<>{0}",typeprice.getId()));
        if (typepriceEntity != null || typepriceEntity1 != null || typepriceEntity2 != null) {
            return R.error("时段冲突，请核对后再进行操作");
        }

        typeprice.setTypeid(Integer.parseInt(typeid));
        typeprice.setStarttime(df.parse(starttime));
        typeprice.setEndtime(df.parse(endtime));
        typeprice.setPrices(BigDecimal.valueOf(Float.parseFloat(prices)));
        typeprice.setRemark(remark);
        typepriceService.updateById(typeprice);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("typeprice:typeprice:delete")
    public R delete(@RequestBody Integer[] ids) {
        typepriceService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
