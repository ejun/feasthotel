package io.youngking.modules.goodtype.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.goodtype.entity.GoodtypeEntity;

import java.util.Map;

/**
 * 商品类别表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:38
 */
public interface GoodtypeService extends IService<GoodtypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

