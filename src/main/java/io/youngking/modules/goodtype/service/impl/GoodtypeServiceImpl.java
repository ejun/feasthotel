package io.youngking.modules.goodtype.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.goodtype.dao.GoodtypeDao;
import io.youngking.modules.goodtype.entity.GoodtypeEntity;
import io.youngking.modules.goodtype.service.GoodtypeService;


@Service("goodtypeService")
public class GoodtypeServiceImpl extends ServiceImpl<GoodtypeDao, GoodtypeEntity> implements GoodtypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
        Page<GoodtypeEntity> page = this.selectPage(
                new Query<GoodtypeEntity>(params).getPage(),
                new EntityWrapper<GoodtypeEntity>().like(StringUtils.isNotBlank(key),"typename",key)

        );

        return new PageUtils(page);
    }


//    public PageUtils queryPage(Map<String, Object> params) {
//        String key = (String)params.get("key");
//        Page<ArticleEntity> page = this.selectPage(
//                new Query<ArticleEntity>(params).getPage(),
//                new EntityWrapper<ArticleEntity>().like(StringUtils.isNotBlank(key),"title",key)
//                        .orNew(StringUtils.isNotBlank(key),"`describe` like {0}","%"+key+"%")
//                        .orderBy("sort desc,id desc")
//        );
//
//        return new PageUtils(page);
//    }

}
