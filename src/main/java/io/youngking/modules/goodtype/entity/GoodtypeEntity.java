package io.youngking.modules.goodtype.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import io.youngking.modules.good.entity.GoodEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 商品类别表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:38
 */
@TableName("tb_goodtype")
public class GoodtypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 类别名称
	 */
	private String typename;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 是否显示(0:显示；1:不显示)
	 */
	private Integer isshow;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 商品列表
	 */
	@TableField(exist = false)
	private List<GoodEntity> goodEntities;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类别名称
	 */
	public void setTypename(String typename) {
		this.typename = typename;
	}
	/**
	 * 获取：类别名称
	 */
	public String getTypename() {
		return typename;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：是否显示(0:显示；1:不显示)
	 */
	public void setIsshow(Integer isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否显示(0:显示；1:不显示)
	 */
	public Integer getIsshow() {
		return isshow;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}

	public List<GoodEntity> getGoodEntities() {
		return goodEntities;
	}

	public void setGoodEntities(List<GoodEntity> goodEntities) {
		this.goodEntities = goodEntities;
	}
}
