package io.youngking.modules.goodtype.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.goodtype.entity.GoodtypeEntity;
import io.youngking.modules.goodtype.service.GoodtypeService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 商品类别表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:38
 */
@RestController
@RequestMapping("goodtype/goodtype")
public class GoodtypeController {
    @Autowired
    private GoodtypeService goodtypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("goodtype:goodtype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = goodtypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("goodtype:goodtype:info")
    public R info(@PathVariable("id") Integer id){
			GoodtypeEntity goodtype = goodtypeService.selectById(id);

        return R.ok().put("goodtype", goodtype);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("goodtype:goodtype:save")
    public R save(@RequestBody GoodtypeEntity goodtype){
			goodtypeService.insert(goodtype);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("goodtype:goodtype:update")
    public R update(@RequestBody GoodtypeEntity goodtype){
			goodtypeService.updateById(goodtype);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("goodtype:goodtype:delete")
    public R delete(@RequestBody Integer[] ids){
			goodtypeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
