package io.youngking.modules.goodtype.dao;

import io.youngking.modules.goodtype.entity.GoodtypeEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品类别表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:38
 */
@Mapper
public interface GoodtypeDao extends BaseMapper<GoodtypeEntity> {
	
}
