package io.youngking.modules.articletype.dao;

import io.youngking.modules.articletype.entity.ArticletypeEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文章分类表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 13:44:47
 */
@Mapper
public interface ArticletypeDao extends BaseMapper<ArticletypeEntity> {
	
}
