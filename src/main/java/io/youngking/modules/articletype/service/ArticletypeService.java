package io.youngking.modules.articletype.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.articletype.entity.ArticletypeEntity;

import java.util.Map;

/**
 * 文章分类表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 13:44:47
 */
public interface ArticletypeService extends IService<ArticletypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

