package io.youngking.modules.articletype.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.articletype.dao.ArticletypeDao;
import io.youngking.modules.articletype.entity.ArticletypeEntity;
import io.youngking.modules.articletype.service.ArticletypeService;


@Service("articletypeService")
public class ArticletypeServiceImpl extends ServiceImpl<ArticletypeDao, ArticletypeEntity> implements ArticletypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
        Page<ArticletypeEntity> page = this.selectPage(
                new Query<ArticletypeEntity>(params).getPage(),
                new EntityWrapper<ArticletypeEntity>().like(StringUtils.isNotBlank(key),"typecode",key).
                        orNew(StringUtils.isNotBlank(key),"typename like {0}","%"+key+"%")
                        .orderBy("sort desc,id desc")
        );

        return new PageUtils(page);
    }

}
