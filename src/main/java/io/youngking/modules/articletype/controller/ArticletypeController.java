package io.youngking.modules.articletype.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.articletype.entity.ArticletypeEntity;
import io.youngking.modules.articletype.service.ArticletypeService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 文章分类表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 13:44:47
 */
@RestController
@RequestMapping("articletype/articletype")
public class ArticletypeController {
    @Autowired
    private ArticletypeService articletypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("articletype:articletype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = articletypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("articletype:articletype:info")
    public R info(@PathVariable("id") Integer id){
			ArticletypeEntity articletype = articletypeService.selectById(id);

        return R.ok().put("articletype", articletype);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("articletype:articletype:save")
    public R save(@RequestBody ArticletypeEntity articletype){
			articletypeService.insert(articletype);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("articletype:articletype:update")
    public R update(@RequestBody ArticletypeEntity articletype){
			articletypeService.updateById(articletype);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("articletype:articletype:delete")
    public R delete(@RequestBody Integer[] ids){
			articletypeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }
    /**
     * 文章类别列表数据(添加、修改文章)
     */
    @GetMapping("/select")
    @RequiresPermissions("articletype:articletype:select")
    public R select(){
        //查询列表数据
        List<ArticletypeEntity> typeList = articletypeService.selectList(null);
        return R.ok().put("typeList", typeList);
    }
}
