package io.youngking.modules.articletype.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章分类表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 13:44:47
 */
@TableName("tb_articletype")
public class ArticletypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 类型编号
	 */
	private String typecode;
	/**
	 * 类别名称
	 */
	private String typename;
	/**
	 * 是否显示(0:显示；1:不显示)
	 */
	private Integer isshow;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类型编号
	 */
	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}
	/**
	 * 获取：类型编号
	 */
	public String getTypecode() {
		return typecode;
	}
	/**
	 * 设置：类别名称
	 */
	public void setTypename(String typename) {
		this.typename = typename;
	}
	/**
	 * 获取：类别名称
	 */
	public String getTypename() {
		return typename;
	}
	/**
	 * 设置：是否显示(0:显示；1:不显示)
	 */
	public void setIsshow(Integer isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否显示(0:显示；1:不显示)
	 */
	public Integer getIsshow() {
		return isshow;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
