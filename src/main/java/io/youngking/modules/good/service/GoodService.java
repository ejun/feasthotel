package io.youngking.modules.good.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.good.entity.GoodEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品信息表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:02
 */
public interface GoodService extends IService<GoodEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<GoodEntity> getlist(Map<String,Object>params );
}

