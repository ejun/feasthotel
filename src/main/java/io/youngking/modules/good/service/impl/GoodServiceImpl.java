package io.youngking.modules.good.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.good.dao.GoodDao;
import io.youngking.modules.good.entity.GoodEntity;
import io.youngking.modules.good.service.GoodService;


@Service("goodService")
public class GoodServiceImpl extends ServiceImpl<GoodDao, GoodEntity> implements GoodService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        Page<GoodEntity> page = this.selectPage(
                new Query<GoodEntity>(params).getPage(),
                new EntityWrapper<GoodEntity>().like(StringUtils.isNotBlank(key), "name", key)
                        .orNew("typename like {0}","%"+key+"%")
                        .orderBy("sort desc,id desc")
        );

        return new PageUtils(page);
    }

    @Override
    public List<GoodEntity> getlist(Map<String, Object> params) {
        return baseMapper.getlist(params);
    }

}