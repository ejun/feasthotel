package io.youngking.modules.good.controller;

import java.io.File;
import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import io.youngking.modules.goodtype.entity.GoodtypeEntity;
import io.youngking.modules.goodtype.service.GoodtypeService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.good.entity.GoodEntity;
import io.youngking.modules.good.service.GoodService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 商品信息表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:02
 */
@RestController
@RequestMapping("good/good")
public class GoodController {
    @Autowired
    private GoodService goodService;
    @Autowired
    private GoodtypeService goodtypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("good:good:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = goodService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("good:good:info")
    public R info(@PathVariable("id") Integer id){
			GoodEntity good = goodService.selectById(id);
        String img = good.getImgurl();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/goods/" + imglist[i];
                newlist += "{name:'"+imglist[i]+"',"+"url:'" + fileSavePath + "'}" + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = "[" + newlist.substring(0, newlist.length() - 1) + "]";
        }
        good.setImgurl(newlist);
        return R.ok().put("good", good);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/goods/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/goods/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("good:good:save")
    public R save(@RequestBody GoodEntity good){
			goodService.insert(good);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("good:good:update")
    public R update(@RequestBody GoodEntity good){
			goodService.updateById(good);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("good:good:delete")
    public R delete(@RequestBody Integer[] ids){
			goodService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

    @RequestMapping("/selecttypelist")
    @RequiresPermissions("good:good:selecttypelist")
    public R selecttypelist(){
        List<GoodtypeEntity> typeList=goodtypeService.selectList(null);
        return R.ok().put("typeList",typeList);
    }
}
