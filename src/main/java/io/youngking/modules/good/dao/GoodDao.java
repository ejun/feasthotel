package io.youngking.modules.good.dao;

import io.youngking.modules.good.entity.GoodEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 商品信息表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:02
 */
@Mapper
public interface GoodDao extends BaseMapper<GoodEntity> {

    List<GoodEntity> getlist(Map<String,Object> params);
}
