package io.youngking.modules.good.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 商品信息表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 10:57:02
 */
@TableName("tb_good")
public class GoodEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;

	private Integer typeid;
	/**
	 * typename
	 * 食品类别
	 */
	private String typename;
	/**
	 * 美食名称
	 */
	private String name;
	/**
	 * 单价
	 */
	private BigDecimal price;
	/**
	 * 描述
	 */
	private String describe;
	/**
	 * 标签
	 */
	private String tag;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 是否显示(0:显示；1:不显示)
	 */
	private Integer isshow;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 图片
	 */
	private String imgurl;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeid() {
		return typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：美食名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：美食名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：单价
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * 获取：单价
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * 设置：描述
	 */
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	/**
	 * 获取：描述
	 */
	public String getDescribe() {
		return describe;
	}
	/**
	 * 设置：标签
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * 获取：标签
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：是否显示(0:显示；1:不显示)
	 */
	public void setIsshow(Integer isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否显示(0:显示；1:不显示)
	 */
	public Integer getIsshow() {
		return isshow;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：图片
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 获取：图片
	 */
	public String getImgurl() {
		return imgurl;
	}
}
