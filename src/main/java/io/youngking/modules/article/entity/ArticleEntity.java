package io.youngking.modules.article.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 14:06:17
 */
@TableName("tb_article")
public class ArticleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 区域id
	 */
	private Integer areaid;
	/**
	 * 类别id
	 */
	private Integer typeid;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 标题图片
	 */
	private String titleimg;
	/**
	 * 描述
	 */
	private String describe;
	/**
	 * 正文内容
	 */
	private String contents;
	/**
	 * 阅读量
	 */
	private Integer num;
	/**
	 * 发布人
	 */
	private String opuser;
	/**
	 * 发布时间
	 */
	private Date sendtime;
	/**
	 * 是否显示(0:显示；1:不显示)
	 */
	private Integer isshow;
	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：区域id
	 */
	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}
	/**
	 * 获取：区域id
	 */
	public Integer getAreaid() {
		return areaid;
	}
	/**
	 * 设置：类别id
	 */
	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}
	/**
	 * 获取：类别id
	 */
	public Integer getTypeid() {
		return typeid;
	}
	/**
	 * 设置：标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：标题
	 */
	public String getTitle() {
		return title;
	}

	public String getTitleimg() {
		return titleimg;
	}

	public void setTitleimg(String titleimg) {
		this.titleimg = titleimg;
	}

	/**
	 * 设置：描述
	 */
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	/**
	 * 获取：描述
	 */
	public String getDescribe() {
		return describe;
	}
	/**
	 * 设置：正文内容
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}
	/**
	 * 获取：正文内容
	 */
	public String getContents() {
		return contents;
	}
	/**
	 * 设置：阅读量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
	/**
	 * 获取：阅读量
	 */
	public Integer getNum() {
		return num;
	}
	/**
	 * 设置：发布人
	 */
	public void setOpuser(String opuser) {
		this.opuser = opuser;
	}
	/**
	 * 获取：发布人
	 */
	public String getOpuser() {
		return opuser;
	}
	/**
	 * 设置：发布时间
	 */
	public void setSendtime(Date sendtime) {
		this.sendtime = sendtime;
	}
	/**
	 * 获取：发布时间
	 */
	public Date getSendtime() {
		return sendtime;
	}
	/**
	 * 设置：是否显示(0:显示；1:不显示)
	 */
	public void setIsshow(Integer isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否显示(0:显示；1:不显示)
	 */
	public Integer getIsshow() {
		return isshow;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
}
