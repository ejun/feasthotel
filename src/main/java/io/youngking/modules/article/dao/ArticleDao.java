package io.youngking.modules.article.dao;

import io.youngking.modules.article.entity.ArticleEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 文章表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 14:06:17
 */
@Mapper
public interface ArticleDao extends BaseMapper<ArticleEntity> {
    /**
     * 查询文章列表
     */
    List<ArticleEntity> getlist(Map<String, Object> params);
}
