package io.youngking.modules.article.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.article.dao.ArticleDao;
import io.youngking.modules.article.entity.ArticleEntity;
import io.youngking.modules.article.service.ArticleService;


@Service("articleService")
public class ArticleServiceImpl extends ServiceImpl<ArticleDao, ArticleEntity> implements ArticleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
        Page<ArticleEntity> page = this.selectPage(
                new Query<ArticleEntity>(params).getPage(),
                new EntityWrapper<ArticleEntity>().like(StringUtils.isNotBlank(key),"title",key)
                    .orNew(StringUtils.isNotBlank(key),"`describe` like {0}","%"+key+"%")
                    .orderBy("sort desc,id desc")
        );

        return new PageUtils(page);
    }

    @Override
    public List<ArticleEntity> getlist(Map<String, Object> params){
         return baseMapper.getlist(params);
    }
}
