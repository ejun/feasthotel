package io.youngking.modules.article.controller;

import java.io.File;
import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import io.youngking.modules.sys.controller.AbstractController;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.article.entity.ArticleEntity;
import io.youngking.modules.article.service.ArticleService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 文章表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 14:06:17
 */
@RestController
@RequestMapping("article/article")
public class ArticleController extends AbstractController {
    @Autowired
    private ArticleService articleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("article:article:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = articleService.queryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        page.setList(articleService.getlist(params1));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("article:article:info")
    public R info(@PathVariable("id") Integer id){
			ArticleEntity article = articleService.selectById(id);
        String img = article.getTitleimg();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/article/" + imglist[i];
                newlist += "{name:'"+imglist[i]+"',"+"url:'" + fileSavePath + "'}" + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = "[" + newlist.substring(0, newlist.length() - 1) + "]";
        }
        article.setTitleimg(newlist);
        return R.ok().put("article", article);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/article/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/article/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("article:article:save")
    public R save(@RequestBody ArticleEntity article){
        article.setOpuser(getUser().getUsername());
        article.setSendtime(new Date());
        articleService.insert(article);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("article:article:update")
    public R update(@RequestBody ArticleEntity article){
        article.setOpuser(getUser().getUsername());
        article.setSendtime(new Date());
        articleService.updateById(article);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("article:article:delete")
    public R delete(@RequestBody Integer[] ids){
			articleService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
