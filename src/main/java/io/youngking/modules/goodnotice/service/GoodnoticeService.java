package io.youngking.modules.goodnotice.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.goodnotice.entity.GoodnoticeEntity;

import java.util.List;
import java.util.Map;

/**
 * 订餐通知表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:57:13
 */
public interface GoodnoticeService extends IService<GoodnoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils Pagelist(Map<String, Object> params);

    List<Map<String,Object>> getlist(Map<String, Object> params);

    Map<String,Object> getinfo (Integer id);
}

