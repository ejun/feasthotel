package io.youngking.modules.goodnotice.dao;

import io.youngking.modules.goodnotice.entity.GoodnoticeEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 订餐通知表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:57:13
 */
@Mapper
public interface GoodnoticeDao extends BaseMapper<GoodnoticeEntity> {
    List<Map<String,Object>> getlist(Map<String, Object> params);

    Map<String,Object> getinfo (Integer id);
}
