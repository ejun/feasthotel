package io.youngking.modules.goodnotice.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 订餐通知表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:57:13
 */
@TableName("tb_goodnotice")
public class GoodnoticeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */
	private Integer customerid;
	/**
	 * 订单id
	 */
	private Integer receiptid;
	/**
	 * 发送时间
	 */
	private Date sendtime;
	/**
	 * 是否回复
	 */
	private Integer isreply;
	/**
	 * 回复内容
	 */
	private String recontent;
	/**
	 * 回复时间
	 */
	private Date retime;
	/**
	 * 回复人
	 */
	private String reuser;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：订单id
	 */
	public void setReceiptid(Integer receiptid) {
		this.receiptid = receiptid;
	}
	/**
	 * 获取：订单id
	 */
	public Integer getReceiptid() {
		return receiptid;
	}
	/**
	 * 设置：发送时间
	 */
	public void setSendtime(Date sendtime) {
		this.sendtime = sendtime;
	}
	/**
	 * 获取：发送时间
	 */
	public Date getSendtime() {
		return sendtime;
	}
	/**
	 * 设置：是否回复
	 */
	public void setIsreply(Integer isreply) {
		this.isreply = isreply;
	}
	/**
	 * 获取：是否回复
	 */
	public Integer getIsreply() {
		return isreply;
	}
	/**
	 * 设置：回复内容
	 */
	public void setRecontent(String recontent) {
		this.recontent = recontent;
	}
	/**
	 * 获取：回复内容
	 */
	public String getRecontent() {
		return recontent;
	}
	/**
	 * 设置：回复时间
	 */
	public void setRetime(Date retime) {
		this.retime = retime;
	}
	/**
	 * 获取：回复时间
	 */
	public Date getRetime() {
		return retime;
	}
	/**
	 * 设置：回复人
	 */
	public void setReuser(String reuser) {
		this.reuser = reuser;
	}
	/**
	 * 获取：回复人
	 */
	public String getReuser() {
		return reuser;
	}
}
