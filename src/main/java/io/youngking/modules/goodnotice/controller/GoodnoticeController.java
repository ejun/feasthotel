package io.youngking.modules.goodnotice.controller;

import java.text.SimpleDateFormat;
import java.util.*;

import com.baomidou.mybatisplus.toolkit.StringUtils;
import io.youngking.modules.goodreceipt.entity.GoodreceiptEntity;
import io.youngking.modules.goodreceipt.service.GoodreceiptService;
import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;
import io.youngking.modules.wxuserinfo.service.WxuserinfoService;
import io.youngking.weixin4j.Weixin;
import io.youngking.weixin4j.WeixinException;
import io.youngking.weixin4j.model.message.template.TemplateData;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.goodnotice.entity.GoodnoticeEntity;
import io.youngking.modules.goodnotice.service.GoodnoticeService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 订餐通知表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:57:13
 */
@RestController
@RequestMapping("goodnotice/goodnotice")
public class GoodnoticeController {
    @Autowired
    private GoodnoticeService goodnoticeService;
    @Autowired
    private WxuserinfoService wxuserinfoService;
    @Autowired
    private GoodreceiptService goodreceiptService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("goodnotice:goodnotice:list")
    public R list(@RequestParam Map<String, Object> params){
        int currPage = 0;
        int limit = 0;

        Map<String, Object> map = new HashMap<>();
        //分页参数
        if (params.get("page") != null) {
            currPage = Integer.parseInt((String) params.get("page"));
        }
        if (params.get("limit") != null) {
            limit = Integer.parseInt((String) params.get("limit"));
        }
        map.put("key", params.get("key"));
        map.put("offset", (currPage - 1) * limit);
        map.put("limit", limit);

        //获取数据
        List<Map<String, Object>> list = goodnoticeService.getlist(map);
        Map<String, Object> params1 = new HashMap<>();
        params1.put("list", list);
        params1.put("CurrPage", currPage);
        params1.put("PageSize", limit);
        params1.put("TotalCount", list.size());
        params1.put("TotalPage", (int) Math.ceil((double) list.size() / limit));

        PageUtils page = goodnoticeService.Pagelist(params1);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("goodnotice:goodnotice:info")
    public R info(@PathVariable("id") Integer id){
			Map<String,Object> goodnotice = goodnoticeService.getinfo(id);

        return R.ok().put("goodnotice", goodnotice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("goodnotice:goodnotice:save")
    public R save(@RequestBody GoodnoticeEntity goodnotice){
			goodnoticeService.insert(goodnotice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("goodnotice:goodnotice:update")
    public R update(@RequestBody GoodnoticeEntity goodnotice) throws WeixinException {
        goodnotice.setIsreply(1);
        goodnotice.setRetime(new Date());
			goodnoticeService.updateById(goodnotice);
        GoodreceiptEntity goodreceiptEntity = goodreceiptService.selectById(goodnotice.getReceiptid());
        //微信发送给用户
        WxuserinfoEntity wxuserinfoEntity = wxuserinfoService.selectById(goodnotice.getCustomerid());
        Weixin weixin = new Weixin();
        if (StringUtils.isNotEmpty(wxuserinfoEntity.getOpenid())) {
            List<TemplateData> templateDataList = new ArrayList<>();
            TemplateData templateData = new TemplateData();
            templateData.setColor("red");
            templateData.setValue("商家回复：" + goodnotice.getRecontent());
            templateData.setKey("first");
            templateDataList.add(templateData);
            TemplateData templateData1 = new TemplateData();
            templateData1.setColor("");
            templateData1.setValue(goodreceiptEntity.getCustomername());
            templateData1.setKey("keyword1");
            templateDataList.add(templateData1);
            TemplateData templateData2 = new TemplateData();
            templateData2.setColor("");
            templateData2.setValue(goodreceiptEntity.getReceiptcode());
            templateData2.setKey("keyword2");
            templateDataList.add(templateData2);
            TemplateData templateData3 = new TemplateData();
            templateData3.setColor("");
            templateData3.setValue(goodreceiptEntity.getSendtime());
            templateData3.setKey("keyword3");
            templateDataList.add(templateData3);
            TemplateData templateData4 = new TemplateData();
            templateData4.setColor("");
            templateData4.setValue("点击查看订单详情");
            templateData4.setKey("remark");
            templateDataList.add(templateData4);
            weixin.message().sendTemplateMessage(wxuserinfoEntity.getOpenid(), "iXv3njyTfm4muwL2WeM1wgTOB47g-E9xzMGa1gj0-9o", templateDataList, "http://www.yanzuohotel.com/fillfeedback?id=" + goodnotice.getReceiptid() + "&wxuserinfoid=" + wxuserinfoEntity.getId());
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("goodnotice:goodnotice:delete")
    public R delete(@RequestBody Integer[] ids){
			goodnoticeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
