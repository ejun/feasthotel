package io.youngking.modules.breakfast.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.breakfast.entity.BreakfastEntity;
import io.youngking.modules.breakfast.service.BreakfastService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 早餐表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-20 14:47:22
 */
@RestController
@RequestMapping("breakfast/breakfast")
public class BreakfastController {
    @Autowired
    private BreakfastService breakfastService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("breakfast:breakfast:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = breakfastService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("breakfast:breakfast:info")
    public R info(@PathVariable("id") Integer id){
			BreakfastEntity breakfast = breakfastService.selectById(id);
        String img = breakfast.getImgurl();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/breakfast/" + imglist[i];
                newlist += "{name:'"+imglist[i]+"',"+"url:'" + fileSavePath + "'}" + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = "[" + newlist.substring(0, newlist.length() - 1) + "]";
        }
        breakfast.setImgurl(newlist);
        return R.ok().put("breakfast", breakfast);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/breakfast/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/breakfast/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("breakfast:breakfast:save")
    public R save(@RequestBody BreakfastEntity breakfast){
			breakfastService.insert(breakfast);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("breakfast:breakfast:update")
    public R update(@RequestBody BreakfastEntity breakfast){
			breakfastService.updateById(breakfast);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("breakfast:breakfast:delete")
    public R delete(@RequestBody Integer[] ids){
			breakfastService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
