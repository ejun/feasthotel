package io.youngking.modules.breakfast.dao;

import io.youngking.modules.breakfast.entity.BreakfastEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 早餐表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-20 14:47:22
 */
@Mapper
public interface BreakfastDao extends BaseMapper<BreakfastEntity> {
	
}
