package io.youngking.modules.breakfast.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.breakfast.entity.BreakfastEntity;

import java.util.Map;

/**
 * 早餐表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-20 14:47:22
 */
public interface BreakfastService extends IService<BreakfastEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

