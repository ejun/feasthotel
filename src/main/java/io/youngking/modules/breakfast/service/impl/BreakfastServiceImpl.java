package io.youngking.modules.breakfast.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.breakfast.dao.BreakfastDao;
import io.youngking.modules.breakfast.entity.BreakfastEntity;
import io.youngking.modules.breakfast.service.BreakfastService;


@Service("breakfastService")
public class BreakfastServiceImpl extends ServiceImpl<BreakfastDao, BreakfastEntity> implements BreakfastService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        Page<BreakfastEntity> page = this.selectPage(
                new Query<BreakfastEntity>(params).getPage(),
                new EntityWrapper<BreakfastEntity>().like(StringUtils.isNotBlank(key), "title", key)
                        .orNew("description like {0}","%"+key+"%")
                        .orderBy("sort desc,id desc")
        );

        return new PageUtils(page);
    }

}
