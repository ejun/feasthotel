package io.youngking.modules.weixin.service;

import com.baomidou.mybatisplus.mapper.SqlRunner;
import io.youngking.weixin4j.model.user.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class WinxinMessageService {
    public List<Map<String, Object>> getAllTables() {
        String sql = "select TABLE_NAME as tableName,TABLE_COMMENT as tableComment from information_schema.`TABLES` where TABLE_SCHEMA = 'guns'";
        return SqlRunner.db().selectList(sql);
    }

    //查询微信用户
    public Object getwxuser(String openid) {
        String sql = "SELECT * from tb_wxuserinfo where openid ='" + openid + "'";
        Map<String, Object> map = SqlRunner.db().selectOne(sql);
        return map;
    }

    //增加用户
    public Object addwxuser(Map<String, Object> map) throws ParseException {
        String openid = map.get("openid").toString();
        String subscribe = map.get("subscribe").toString();
        String nickname = map.get("nickname").toString();
        String sex = map.get("sex").toString();
        String language = map.get("language").toString();
        String city = map.get("city").toString();
        String province = map.get("province").toString();
        String country = map.get("country").toString();
        String headimgurl = map.get("headimgurl").toString();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String subscribeTime = df.format(new Date());
        String groupid = map.get("groupid").toString();

        String sql = "insert tb_wxuserinfo (openid,subscribe,nickname,sex,language,city,province," +
                "country,headimgurl,subscribe_time,groupid) values (" + "'" + openid + "'" + "," + subscribe
                + "," + "'" + nickname + "'" + "," + sex + "," + "'" + language + "'" + "," + "'" + city + "'" + "," + "'" + province + "'" + "," + "'" + country + "'" + ","
                + "'" + headimgurl + "'" + "," + "'" + subscribeTime + "'" + "," + groupid + ")";
        SqlRunner.db().insert(sql);
        return "SUCCESS";
    }

    //再次关注
    public Object editwxuser(String openid) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String subscribeTime = df.format(new Date());
        String sql = "update tb_wxuserinfo set subscribe = 1,subscribe_time= "+"'"+subscribeTime+"'"+ " where openid = '" + openid + "'";
        SqlRunner.db().update(sql);
        return "SUCCESS";
    }

    //取消关注
    public Object deletewxuser(String openid) {
        String sql = "update tb_wxuserinfo set subscribe = 0 where openid = '" + openid + "'";
        SqlRunner.db().update(sql);
        return "SUCCESS";
    }
}
