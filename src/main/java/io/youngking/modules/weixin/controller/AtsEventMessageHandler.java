package io.youngking.modules.weixin.controller;

import io.youngking.modules.weixin.service.WinxinMessageService;
import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;
import io.youngking.weixin4j.Weixin;
import io.youngking.weixin4j.WeixinException;
import io.youngking.weixin4j.model.message.Articles;
import io.youngking.weixin4j.model.user.User;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import io.youngking.weixin4j.model.message.OutputMessage;
import io.youngking.weixin4j.model.message.event.*;
import io.youngking.weixin4j.model.message.output.TextOutputMessage;
import io.youngking.weixin4j.spi.IEventMessageHandler;

import javax.annotation.Resource;
import java.awt.*;
import java.text.ParseException;
import java.util.*;
import java.util.List;

/**
 * 自定义事件消息处理器
 *
 * @author yangqisheng
 */
@Controller
public class AtsEventMessageHandler implements IEventMessageHandler {
    private WinxinMessageService winxinMessageService = new WinxinMessageService();

    //关注
    @Override
    public OutputMessage subscribe(SubscribeEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        Weixin weixin = new Weixin();
        String openid = msg.getFromUserName();
        try {
            User user = weixin.user().info(openid);
            HashMap<String, Object> paramMap = new HashMap();
            paramMap.put("openid", user.getOpenid());
            paramMap.put("subscribe", Integer.parseInt(user.getSubscribe()));
            paramMap.put("nickname", user.getNickname());
            paramMap.put("sex", user.getSex());
            paramMap.put("language", user.getLanguage());
            paramMap.put("city", user.getCity());
            paramMap.put("province", user.getProvince());
            paramMap.put("country", user.getCountry());
            paramMap.put("headimgurl", user.getHeadimgurl());
            paramMap.put("subscribeTime", new Date());
            paramMap.put("groupid", user.getGroupid());

            if (winxinMessageService.getwxuser(openid) != null) {
                winxinMessageService.editwxuser(openid);
            } else {
                winxinMessageService.addwxuser(paramMap);
            }

            //发送图文消息
            List<Articles> articlesList = new ArrayList<>();
            Articles articles = new Articles();
            articles.setTitle("丽江泸沽湖晏坐湖景清奢民宿");
            articles.setDescription("丽江泸沽湖晏坐湖景清奢民宿");
            articles.setPicUrl("https://mmbiz.qpic.cn/mmbiz_jpg/3ib78Bl1TWibWeiaBe2uLWal1X2XqUP3sYvT3yCLQ0YEKztvbg3psgPaLyZLdFiaBYfkTphq6H2icT4yGwV3uyIMLyA/0?wx_fmt=jpeg");
            articles.setPicurl("https://mmbiz.qpic.cn/mmbiz_jpg/3ib78Bl1TWibWeiaBe2uLWal1X2XqUP3sYvT3yCLQ0YEKztvbg3psgPaLyZLdFiaBYfkTphq6H2icT4yGwV3uyIMLyA/0?wx_fmt=jpeg");
            articles.setUrl("https://mp.weixin.qq.com/s/JrwrarDqpdY2_lAD_-LZSg");
            articlesList.add(articles);
            weixin.message().customSendNews(openid, articlesList);
        } catch (WeixinException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        out.setContent("");
        return out;
    }

    //取消关注
    @Override
    public OutputMessage unSubscribe(UnSubscribeEventMessage msg) {
        winxinMessageService.deletewxuser(msg.getFromUserName());
        return null;
    }

    @Override
    public OutputMessage qrsceneSubscribe(QrsceneSubscribeEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("感谢您的关注！，来源：" + msg.getEventKey());
        return out;
    }

    @Override
    public OutputMessage qrsceneScan(QrsceneScanEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("你的消息已经收到！");
        return out;
    }

    @Override
    public OutputMessage location(LocationEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("你的消息已经收到！");
        return out;
    }

    @Override
    public OutputMessage click(ClickEventMessage msg) {
        return null;
    }

    @Override
    public OutputMessage view(ViewEventMessage msg) {
        return null;
    }

    @Override
    public OutputMessage scanCodePush(ScanCodePushEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("扫码！");
        return out;
    }

    @Override
    public OutputMessage scanCodeWaitMsg(ScanCodeWaitMsgEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("扫码等待中！");
        return out;
    }

    @Override
    public OutputMessage picSysPhoto(PicSysPhotoEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("发起拍照！");
        return out;
    }

    @Override
    public OutputMessage picPhotoOrAlbum(PicPhotoOrAlbumEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("选择相册！");
        return out;
    }

    @Override
    public OutputMessage picWeixin(PicWeixinEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("上次图片！");
        return out;
    }

    @Override
    public OutputMessage locationSelect(LocationSelectEventMessage msg) {
        TextOutputMessage out = new TextOutputMessage();
        out.setContent("选择地理位置！");
        return out;
    }

}
