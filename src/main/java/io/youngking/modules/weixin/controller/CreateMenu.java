package io.youngking.modules.weixin.controller;

import io.youngking.weixin4j.Weixin;
import io.youngking.weixin4j.WeixinException;
import io.youngking.weixin4j.component.MenuComponent;
import io.youngking.weixin4j.model.menu.Menu;
import io.youngking.weixin4j.model.menu.MiniprogramButton;
import io.youngking.weixin4j.model.menu.SingleButton;
import io.youngking.weixin4j.model.menu.ViewButton;

import java.util.ArrayList;
import java.util.List;


/**
 * 自定义菜单的创建
 */
    public class CreateMenu {

    public static void main(String[] args) {
        //1.初始化weixin对象
        Weixin weixin = new Weixin();
        //2.获取菜单组件
        MenuComponent menuComponet = weixin.menu();
        //3.创建一个Menu对象
        Menu menu = new Menu();
        //初始化菜单列表
        List<SingleButton> buttons = new ArrayList<SingleButton>();
        //添加一级菜单
/*        ViewButton button1 = new ViewButton("使用说明", "http://www.baidu.com/");
        buttons.add(button1);

        ViewButton button2 = new ViewButton("定制下单", "http://www.baidu.com/");
        buttons.add(button2);*/

        //创建菜单，包含二级子菜单
        SingleButton button1 = new SingleButton("订房点餐");
/*        ViewButton button1_1 = new ViewButton("订房", "http://www.baidu.com");
        button1.getSubButton().add(button1_1);*/
        ViewButton button1_2 = new ViewButton("预定点餐", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b5991de0fccf98e&redirect_uri=http%3A%2F%2Fwww.yanzuohotel.com%2Fshenglian-fast%2FcallBackController%2FcallBack&response_type=code&scope=snsapi_base&state=index#wechat_redirect");
        button1.getSubButton().add(button1_2);
        ViewButton button1_3 = new ViewButton("早餐", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b5991de0fccf98e&redirect_uri=http%3A%2F%2Fwww.yanzuohotel.com%2Fshenglian-fast%2FcallBackController%2FcallBack&response_type=code&scope=snsapi_base&state=Show#wechat_redirect");
        button1.getSubButton().add(button1_3);
        ViewButton button1_4 = new ViewButton("我的订单", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b5991de0fccf98e&redirect_uri=http%3A%2F%2Fwww.yanzuohotel.com%2Fshenglian-fast%2FcallBackController%2FcallBack&response_type=code&scope=snsapi_base&state=list#wechat_redirect");
        button1.getSubButton().add(button1_4);
        MiniprogramButton button1_5 = new MiniprogramButton("订房小程序","wxdad5dd30868f7eba","pages/welcome/welcome","https://mp.weixin.qq.com/s/JrwrarDqpdY2_lAD_-LZSg");
        button1.getSubButton().add(button1_5);
        buttons.add(button1);

/*        SingleButton button2 = new SingleButton("攻略");
        ViewButton button2_1= new ViewButton("二级菜单", "http://www.baidu.com/");
        button1.getSubButton().add(button2_1);*/

        ViewButton button2 = new ViewButton("攻略", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b5991de0fccf98e&redirect_uri=http%3A%2F%2Fwww.yanzuohotel.com%2Fshenglian-fast%2FcallBackController%2FcallBack&response_type=code&scope=snsapi_base&state=Raiders#wechat_redirect");
        buttons.add(button2);

        SingleButton button3 = new SingleButton("服务");
        ViewButton button3_1 = new ViewButton("账号关联", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b5991de0fccf98e&redirect_uri=http%3A%2F%2Fwww.yanzuohotel.com%2Fshenglian-fast%2FcallBackController%2FcallBack&response_type=code&scope=snsapi_base&state=Binding#wechat_redirect");
        button3.getSubButton().add(button3_1);
        ViewButton button3_2 = new ViewButton("投诉建议", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b5991de0fccf98e&redirect_uri=http%3A%2F%2Fwww.yanzuohotel.com%2Fshenglian-fast%2FcallBackController%2FcallBack&response_type=code&scope=snsapi_base&state=Complaint#wechat_redirect");
        button3.getSubButton().add(button3_2);
/*         ViewButton button3_3 = new ViewButton("酒店导航", "http://www.baidu.com");
        button3.getSubButton().add(button3_3);
        ViewButton button3_4 = new ViewButton("客服", "http://www.baidu.com");
        button3.getSubButton().add(button3_4);
        ViewButton button3_5 = new ViewButton("WiFi链接", "http://www.baidu.com");
        button3.getSubButton().add(button3_5);*/
        buttons.add(button3);

        //设置自定义菜单
        menu.setButton(buttons);

        try {
            //调用微信自定义菜单组件，创建自定义菜单
            menuComponet.create(menu);
        } catch (WeixinException ex) {
            //打印创建异常日志
            ex.printStackTrace();
        }
    }
}
