package io.youngking.modules.wxappletsuserinfo.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.wxappletsuserinfo.entity.WxappletsuserinfoEntity;

import java.util.Map;

/**
 * 小程序用户表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-12 16:19:44
 */
public interface WxappletsuserinfoService extends IService<WxappletsuserinfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

