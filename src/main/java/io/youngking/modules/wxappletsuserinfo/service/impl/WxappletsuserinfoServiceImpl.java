package io.youngking.modules.wxappletsuserinfo.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.wxappletsuserinfo.dao.WxappletsuserinfoDao;
import io.youngking.modules.wxappletsuserinfo.entity.WxappletsuserinfoEntity;
import io.youngking.modules.wxappletsuserinfo.service.WxappletsuserinfoService;


@Service("wxappletsuserinfoService")
public class WxappletsuserinfoServiceImpl extends ServiceImpl<WxappletsuserinfoDao, WxappletsuserinfoEntity> implements WxappletsuserinfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<WxappletsuserinfoEntity> page = this.selectPage(
                new Query<WxappletsuserinfoEntity>(params).getPage(),
                new EntityWrapper<WxappletsuserinfoEntity>().like(StringUtils.isNotBlank(key),"nickname", key)
                        .orNew(StringUtils.isNotBlank(key),"remark like {0}", "%" + key + "%")
                        .orderBy("id desc")
        );

        return new PageUtils(page);
    }

}
