package io.youngking.modules.wxappletsuserinfo.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.wxappletsuserinfo.entity.WxappletsuserinfoEntity;
import io.youngking.modules.wxappletsuserinfo.service.WxappletsuserinfoService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 小程序用户表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-12 16:19:44
 */
@RestController
@RequestMapping("wxappletsuserinfo/wxappletsuserinfo")
public class WxappletsuserinfoController {
    @Autowired
    private WxappletsuserinfoService wxappletsuserinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wxappletsuserinfo:wxappletsuserinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wxappletsuserinfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wxappletsuserinfo:wxappletsuserinfo:info")
    public R info(@PathVariable("id") Integer id){
			WxappletsuserinfoEntity wxappletsuserinfo = wxappletsuserinfoService.selectById(id);

        return R.ok().put("wxappletsuserinfo", wxappletsuserinfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wxappletsuserinfo:wxappletsuserinfo:save")
    public R save(@RequestBody WxappletsuserinfoEntity wxappletsuserinfo){
			wxappletsuserinfoService.insert(wxappletsuserinfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wxappletsuserinfo:wxappletsuserinfo:update")
    public R update(@RequestBody WxappletsuserinfoEntity wxappletsuserinfo){
			wxappletsuserinfoService.updateById(wxappletsuserinfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wxappletsuserinfo:wxappletsuserinfo:delete")
    public R delete(@RequestBody Integer[] ids){
			wxappletsuserinfoService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
