package io.youngking.modules.wxappletsuserinfo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 小程序用户表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-12 16:19:44
 */
@TableName("tb_wxappletsuserinfo")
public class WxappletsuserinfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户标识
	 */
	private String openid;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 头像
	 */
	private String avatarurl;
	/**
	 * 性别(0:未知；1:男；2:女)
	 */
	private Integer sex;
	/**
	 * 所在城市
	 */
	private String city;
	/**
	 * 所在省份
	 */
	private String province;
	/**
	 * 所在国家
	 */
	private String country;
	/**
	 * 语言
	 */
	private String language;
	/**
	 * 授权时间
	 */
	private Date authtime;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户标识
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：用户标识
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：昵称
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * 获取：昵称
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * 设置：头像
	 */
	public void setAvatarurl(String avatarurl) {
		this.avatarurl = avatarurl;
	}
	/**
	 * 获取：头像
	 */
	public String getAvatarurl() {
		return avatarurl;
	}
	/**
	 * 设置：性别(0:未知；1:男；2:女)
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别(0:未知；1:男；2:女)
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * 设置：所在城市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：所在城市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：所在省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：所在省份
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：所在国家
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * 获取：所在国家
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * 设置：语言
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * 获取：语言
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * 设置：授权时间
	 */
	public void setAuthtime(Date authtime) {
		this.authtime = authtime;
	}
	/**
	 * 获取：授权时间
	 */
	public Date getAuthtime() {
		return authtime;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
