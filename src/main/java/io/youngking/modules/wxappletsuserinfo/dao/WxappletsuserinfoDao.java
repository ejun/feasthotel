package io.youngking.modules.wxappletsuserinfo.dao;

import io.youngking.modules.wxappletsuserinfo.entity.WxappletsuserinfoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 小程序用户表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-12 16:19:44
 */
@Mapper
public interface WxappletsuserinfoDao extends BaseMapper<WxappletsuserinfoEntity> {
	
}
