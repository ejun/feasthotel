package io.youngking.modules.surrounding.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.surrounding.entity.SurroundingEntity;
import io.youngking.modules.surrounding.service.SurroundingService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 周边信息
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-02 15:38:38
 */
@RestController
@RequestMapping("surrounding/surrounding")
public class SurroundingController {
    @Autowired
    private SurroundingService surroundingService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("surrounding:surrounding:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = surroundingService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("surrounding:surrounding:info")
    public R info(@PathVariable("id") Integer id){
			SurroundingEntity surrounding = surroundingService.selectById(id);

        return R.ok().put("surrounding", surrounding);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("surrounding:surrounding:save")
    public R save(@RequestBody SurroundingEntity surrounding){
			surroundingService.insert(surrounding);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("surrounding:surrounding:update")
    public R update(@RequestBody SurroundingEntity surrounding){
			surroundingService.updateById(surrounding);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("surrounding:surrounding:delete")
    public R delete(@RequestBody Integer[] ids){
			surroundingService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
