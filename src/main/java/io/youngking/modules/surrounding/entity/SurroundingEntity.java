package io.youngking.modules.surrounding.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 周边信息
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-02 15:38:38
 */
@TableName("tb_surrounding")
public class SurroundingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 类型（1：景区，2：交通；3：其他）
	 */
	private Integer itype;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 内容
	 */
	private String contents;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 是否显示（0：显示；1：不显示）
	 */
	private Integer isshow;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类型（1：景区，2：交通；3：其他）
	 */
	public void setItype(Integer itype) {
		this.itype = itype;
	}
	/**
	 * 获取：类型（1：景区，2：交通；3：其他）
	 */
	public Integer getItype() {
		return itype;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：内容
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}
	/**
	 * 获取：内容
	 */
	public String getContents() {
		return contents;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：是否显示（0：显示；1：不显示）
	 */
	public void setIsshow(Integer isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否显示（0：显示；1：不显示）
	 */
	public Integer getIsshow() {
		return isshow;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
