package io.youngking.modules.surrounding.dao;

import io.youngking.modules.surrounding.entity.SurroundingEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 周边信息
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-02 15:38:38
 */
@Mapper
public interface SurroundingDao extends BaseMapper<SurroundingEntity> {
	
}
