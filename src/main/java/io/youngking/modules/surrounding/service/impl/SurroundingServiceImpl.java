package io.youngking.modules.surrounding.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.surrounding.dao.SurroundingDao;
import io.youngking.modules.surrounding.entity.SurroundingEntity;
import io.youngking.modules.surrounding.service.SurroundingService;


@Service("surroundingService")
public class SurroundingServiceImpl extends ServiceImpl<SurroundingDao, SurroundingEntity> implements SurroundingService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<SurroundingEntity> page = this.selectPage(
                new Query<SurroundingEntity>(params).getPage(),
                new EntityWrapper<SurroundingEntity>().like("name",key)
                        .orNew("contents like {0}","%"+key+"%")
                        .orderBy("sort desc,id desc")
        );
        return new PageUtils(page);
    }

}
