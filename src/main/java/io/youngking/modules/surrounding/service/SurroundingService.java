package io.youngking.modules.surrounding.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.surrounding.entity.SurroundingEntity;

import java.util.Map;

/**
 * 周边信息
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-08-02 15:38:38
 */
public interface SurroundingService extends IService<SurroundingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

