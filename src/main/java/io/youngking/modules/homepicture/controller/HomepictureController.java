package io.youngking.modules.homepicture.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import io.youngking.modules.hotelinfo.entity.HotelinfoEntity;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.homepicture.entity.HomepictureEntity;
import io.youngking.modules.homepicture.service.HomepictureService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 首页图片
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-31 15:52:39
 */
@RestController
@RequestMapping("homepicture/homepicture")
public class HomepictureController {
    @Autowired
    private HomepictureService homepictureService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("homepicture:homepicture:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = homepictureService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("homepicture:homepicture:info")
    public R info(@PathVariable("id") Integer id) {
        HomepictureEntity homepicture = homepictureService.selectById(id);
        String img = homepicture.getUrl();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/homepic/" + imglist[i];
                newlist += "{name:'" + imglist[i] + "'," + "url:'" + fileSavePath + "'}" + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = "[" + newlist.substring(0, newlist.length() - 1) + "]";
        }
        homepicture.setUrl(newlist);
        return R.ok().put("homepicture", homepicture);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/homepic/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }

        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/homepic/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("homepicture:homepicture:save")
    public R save(@RequestBody HomepictureEntity homepicture) {
        homepictureService.insert(homepicture);
        if (homepicture.getIsshow() == 0) {
            HomepictureEntity homepictureEntity = new HomepictureEntity();
            homepictureEntity.setIsshow(1);
            homepictureService.update(homepictureEntity, new EntityWrapper<HomepictureEntity>().where("id<>{0}", homepicture.getId()));
        }
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("homepicture:homepicture:update")
    public R update(@RequestBody HomepictureEntity homepicture) {
        homepictureService.updateById(homepicture);
        if (homepicture.getIsshow() == 0) {
            HomepictureEntity homepictureEntity = new HomepictureEntity();
            homepictureEntity.setIsshow(1);
            homepictureService.update(homepictureEntity, new EntityWrapper<HomepictureEntity>().where("id<>{0}", homepicture.getId()));
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("homepicture:homepicture:delete")
    public R delete(@RequestBody Integer[] ids) {
        homepictureService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
