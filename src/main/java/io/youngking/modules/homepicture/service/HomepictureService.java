package io.youngking.modules.homepicture.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.homepicture.entity.HomepictureEntity;

import java.util.Map;

/**
 * 首页图片
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-31 15:52:39
 */
public interface HomepictureService extends IService<HomepictureEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

