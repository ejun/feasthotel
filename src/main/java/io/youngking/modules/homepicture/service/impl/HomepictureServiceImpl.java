package io.youngking.modules.homepicture.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.homepicture.dao.HomepictureDao;
import io.youngking.modules.homepicture.entity.HomepictureEntity;
import io.youngking.modules.homepicture.service.HomepictureService;


@Service("homepictureService")
public class HomepictureServiceImpl extends ServiceImpl<HomepictureDao, HomepictureEntity> implements HomepictureService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<HomepictureEntity> page = this.selectPage(
                new Query<HomepictureEntity>(params).getPage(),
                new EntityWrapper<HomepictureEntity>().like("description",key).orNew("remark like {0}","%"+key+"%")
        );

        return new PageUtils(page);
    }

}
