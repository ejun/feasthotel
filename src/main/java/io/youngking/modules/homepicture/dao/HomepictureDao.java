package io.youngking.modules.homepicture.dao;

import io.youngking.modules.homepicture.entity.HomepictureEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页图片
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-31 15:52:39
 */
@Mapper
public interface HomepictureDao extends BaseMapper<HomepictureEntity> {
	
}
