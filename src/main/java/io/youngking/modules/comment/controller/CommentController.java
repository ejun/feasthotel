package io.youngking.modules.comment.controller;

import java.io.File;
import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.comment.entity.CommentEntity;
import io.youngking.modules.comment.service.CommentService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 房间评价表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-30 16:39:41
 */
@RestController
@RequestMapping("comment/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("comment:comment:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = commentService.queryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        page.setList(commentService.getlist(params1));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("comment:comment:info")
    public R info(@PathVariable("id") Integer id){
			CommentEntity comment = commentService.selectById(id);

        String img = comment.getImgurl();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/comment/" + imglist[i];
                newlist += fileSavePath + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = newlist.substring(0, newlist.length() - 1);
        }
        comment.setImgurl(newlist);
        return R.ok().put("comment", comment);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/comment/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("comment:comment:save")
    public R save(@RequestBody CommentEntity comment){
        comment.setCommenttime(new Date());
			commentService.insert(comment);

        return R.ok();
    }
    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("comment:comment:update")
    public R update(@RequestBody CommentEntity comment){
        comment.setReplytime(new Date());
			commentService.updateById(comment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("comment:comment:delete")
    public R delete(@RequestBody Integer[] ids){
			commentService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/comment/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }
}
