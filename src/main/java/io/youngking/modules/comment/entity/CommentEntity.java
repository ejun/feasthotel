package io.youngking.modules.comment.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Date;

/**
 * 房间评价表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-30 16:39:41
 */
@TableName("tb_comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */

	private Integer customerid;
	/**
	 * 房间类型id
	 */
	private Integer roomtypeid;
	/**
	 * 用户昵称
	 */
	private String nickname;
	/**
	 * 房型名称
	 */
	private String roomtypename;
	/**
	 * 订单id
	 */
	private Integer receiptid;
	/**
	 * 评价内容
	 */
	private String contents;
	/**
	 * 图片
	 */
	private String imgurl;
	/**
	 * 回复内容
	 */
	private String replyinfo;
	/**
	 * 回复时间
	 */
	private Date replytime;
	/**
	 * 评价时间
	 */
	private Date commenttime;
	/**
	 *图片数组
	 */
	@TableField(exist = false)
	private String[] img;

	public String[] getImg() {
		return img;
	}

	public void setImg(String[] img) {
		this.img = img;
	}

	public Integer getRoomtypeid() {
		return roomtypeid;
	}

	public void setRoomtypeid(Integer roomtypeid) {
		this.roomtypeid = roomtypeid;
	}

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：用户昵称
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * 获取：用户昵称
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * 设置：房型名称
	 */
	public void setRoomtypename(String roomtypename) {
		this.roomtypename = roomtypename;
	}
	/**
	 * 获取：房型名称
	 */
	public String getRoomtypename() {
		return roomtypename;
	}
	/**
	 * 设置：订单id
	 */
	public void setReceiptid(Integer receiptid) {
		this.receiptid = receiptid;
	}
	/**
	 * 获取：订单id
	 */
	public Integer getReceiptid() {
		return receiptid;
	}
	/**
	 * 设置：评价内容
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}
	/**
	 * 获取：评价内容
	 */
	public String getContents() {
		return contents;
	}
	/**
	 * 设置：图片
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 获取：图片
	 */
	public String getImgurl() {
		return imgurl;
	}
	/**
	 * 设置：回复内容
	 */
	public void setReplyinfo(String replyinfo) {
		this.replyinfo = replyinfo;
	}
	/**
	 * 获取：回复内容
	 */
	public String getReplyinfo() {
		return replyinfo;
	}
	/**
	 * 设置：回复时间
	 */
	public void setReplytime(Date replytime) {
		this.replytime = replytime;
	}
	/**
	 * 获取：回复时间
	 */
	public Date getReplytime() {
		return replytime;
	}
	/**
	 * 设置：评价时间
	 */
	public void setCommenttime(Date commenttime) {
		this.commenttime = commenttime;
	}
	/**
	 * 获取：评价时间
	 */
	public Date getCommenttime() {
		return commenttime;
	}
}
