package io.youngking.modules.comment.dao;

import io.youngking.modules.comment.entity.CommentEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 房间评价表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-30 16:39:41
 */
@Mapper
public interface CommentDao extends BaseMapper<CommentEntity> {

	List<Map<String,Object>> getlist(Map<String, Object> params);
}
