package io.youngking.modules.comment.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.comment.entity.CommentEntity;

import java.util.List;
import java.util.Map;

/**
 * 房间评价表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-30 16:39:41
 */
public interface CommentService extends IService<CommentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<Map<String,Object>> getlist(Map<String, Object> params);
}

