package io.youngking.modules.comment.service.impl;


import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.comment.dao.CommentDao;
import io.youngking.modules.comment.entity.CommentEntity;
import io.youngking.modules.comment.service.CommentService;


@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<CommentDao, CommentEntity> implements CommentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key=(String)params.get("key");
        Page<CommentEntity> page = this.selectPage(
                new Query<CommentEntity>(params).getPage(),
                new EntityWrapper<CommentEntity>().like(StringUtils.isNotBlank(key),"nickname",key)

        );

        return new PageUtils(page);
    }

    @Override
    public List<Map<String,Object>> getlist(Map<String, Object> params) {
        return baseMapper.getlist(params);
    }

}
