package io.youngking.modules.roomnotice.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.roomnotice.entity.RoomnoticeEntity;

import java.util.List;
import java.util.Map;

/**
 * 订房通知表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:53:12
 */
public interface RoomnoticeService extends IService<RoomnoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils Pagelist(Map<String, Object> params);

    List<Map<String,Object>> getlist(Map<String, Object> params);
}

