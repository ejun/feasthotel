package io.youngking.modules.roomnotice.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.roomnotice.entity.RoomnoticeEntity;
import io.youngking.modules.roomnotice.service.RoomnoticeService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 订房通知表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:53:12
 */
@RestController
@RequestMapping("roomnotice/roomnotice")
public class RoomnoticeController {
    @Autowired
    private RoomnoticeService roomnoticeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("roomnotice:roomnotice:list")
    public R list(@RequestParam Map<String, Object> params){
        int currPage = 0;
        int limit = 0;

        Map<String, Object> map = new HashMap<>();
        //分页参数
        if (params.get("page") != null) {
            currPage = Integer.parseInt((String) params.get("page"));
        }
        if (params.get("limit") != null) {
            limit = Integer.parseInt((String) params.get("limit"));
        }
        map.put("key", params.get("key"));
        map.put("offset", (currPage - 1) * limit);
        map.put("limit", limit);

        //获取数据
        List<Map<String, Object>> list = roomnoticeService.getlist(map);
        Map<String, Object> params1 = new HashMap<>();
        params1.put("list", list);
        params1.put("CurrPage", currPage);
        params1.put("PageSize", limit);
        params1.put("TotalCount", list.size());
        params1.put("TotalPage", (int) Math.ceil((double) list.size() / limit));

        PageUtils page = roomnoticeService.Pagelist(params1);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("roomnotice:roomnotice:info")
    public R info(@PathVariable("id") Integer id){
			RoomnoticeEntity roomnotice = roomnoticeService.selectById(id);

        return R.ok().put("roomnotice", roomnotice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("roomnotice:roomnotice:save")
    public R save(@RequestBody RoomnoticeEntity roomnotice){
			roomnoticeService.insert(roomnotice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("roomnotice:roomnotice:update")
    public R update(@RequestBody RoomnoticeEntity roomnotice){
			roomnoticeService.updateById(roomnotice);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("roomnotice:roomnotice:delete")
    public R delete(@RequestBody Integer[] ids){
			roomnoticeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
