package io.youngking.modules.roomnotice.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 订房通知表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:53:12
 */
@TableName("tb_roomnotice")
public class RoomnoticeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */
	private Integer customerid;
	/**
	 * 订单id
	 */
	private Integer receiptid;
	/**
	 * 发送时间
	 */
	private Date sendtime;
	/**
	 * 发送人
	 */
	private String senduser;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：订单id
	 */
	public void setReceiptid(Integer receiptid) {
		this.receiptid = receiptid;
	}
	/**
	 * 获取：订单id
	 */
	public Integer getReceiptid() {
		return receiptid;
	}
	/**
	 * 设置：发送时间
	 */
	public void setSendtime(Date sendtime) {
		this.sendtime = sendtime;
	}
	/**
	 * 获取：发送时间
	 */
	public Date getSendtime() {
		return sendtime;
	}
	/**
	 * 设置：发送人
	 */
	public void setSenduser(String senduser) {
		this.senduser = senduser;
	}
	/**
	 * 获取：发送人
	 */
	public String getSenduser() {
		return senduser;
	}
}
