package io.youngking.modules.roomnotice.dao;

import io.youngking.modules.roomnotice.entity.RoomnoticeEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 订房通知表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-21 11:53:12
 */
@Mapper
public interface RoomnoticeDao extends BaseMapper<RoomnoticeEntity> {
    List<Map<String,Object>> getlist(Map<String, Object> params);
}
