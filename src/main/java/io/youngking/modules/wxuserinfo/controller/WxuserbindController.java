package io.youngking.modules.wxuserinfo.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;
import io.youngking.modules.wxuserinfo.service.WxuserinfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 微信用户信息表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 09:39:51
 */
@RestController
@RequestMapping("wxuserinfo/wxuserbind")
public class WxuserbindController {
    @Autowired
    private WxuserinfoService wxuserinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wxuserinfo:wxuserbind:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wxuserinfoService.bindqueryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 订房绑定
     */
    @RequestMapping("/DFBD")
    @RequiresPermissions("wxuserinfo:wxuserbind:DFBD")
    public R DFBD(@RequestBody Integer[] ids){
        WxuserinfoEntity wxuserinfo = new WxuserinfoEntity();
        wxuserinfo.setIsdfnotice(1);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new WxuserinfoEntity());
        ew.in("id",ids);
        wxuserinfoService.update(wxuserinfo,ew);
        return R.ok();
    }

    /**
     * 订房解绑
     */
    @RequestMapping("/DFJB")
    @RequiresPermissions("wxuserinfo:wxuserbind:DFJB")
    public R DFJB(@RequestBody Integer[] ids){
        WxuserinfoEntity wxuserinfo = new WxuserinfoEntity();
        wxuserinfo.setIsdfnotice(0);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new WxuserinfoEntity());
        ew.in("id",ids);
        wxuserinfoService.update(wxuserinfo,ew);
        return R.ok();
    }

    /**
     * 订餐绑定
     */
    @RequestMapping("/DCBD")
    @RequiresPermissions("wxuserinfo:wxuserbind:DCBD")
    public R DCBD(@RequestBody Integer[] ids){
        WxuserinfoEntity wxuserinfo = new WxuserinfoEntity();
        wxuserinfo.setIsdcnotice(1);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new WxuserinfoEntity());
        ew.in("id",ids);
        wxuserinfoService.update(wxuserinfo,ew);

        return R.ok();
    }

    /**
     * 订餐解绑
     */
    @RequestMapping("/DCJB")
    @RequiresPermissions("wxuserinfo:wxuserbind:DCJB")
    public R DCJB(@RequestBody Integer[] ids){
        WxuserinfoEntity wxuserinfo = new WxuserinfoEntity();
        wxuserinfo.setIsdcnotice(0);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new WxuserinfoEntity());
        ew.in("id",ids);
        wxuserinfoService.update(wxuserinfo,ew);

        return R.ok();
    }

    /**
     * 点评绑定
     */
    @RequestMapping("/DPBD")
    @RequiresPermissions("wxuserinfo:wxuserbind:DPBD")
    public R DPBD(@RequestBody Integer[] ids){
        WxuserinfoEntity wxuserinfo = new WxuserinfoEntity();
        wxuserinfo.setIsdpnotice(1);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new WxuserinfoEntity());
        ew.in("id",ids);
        wxuserinfoService.update(wxuserinfo,ew);

        return R.ok();
    }

    /**
     * 点评解绑
     */
    @RequestMapping("/DPJB")
    @RequiresPermissions("wxuserinfo:wxuserbind:DPJB")
    public R DPJB(@RequestBody Integer[] ids){
        WxuserinfoEntity wxuserinfo = new WxuserinfoEntity();
        wxuserinfo.setIsdpnotice(0);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new WxuserinfoEntity());
        ew.in("id",ids);
        wxuserinfoService.update(wxuserinfo,ew);

        return R.ok();
    }
}
