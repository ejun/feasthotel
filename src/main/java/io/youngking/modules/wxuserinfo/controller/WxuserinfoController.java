package io.youngking.modules.wxuserinfo.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;
import io.youngking.modules.wxuserinfo.service.WxuserinfoService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 微信用户信息表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 09:39:51
 */
@RestController
@RequestMapping("wxuserinfo/wxuserinfo")
public class WxuserinfoController {
    @Autowired
    private WxuserinfoService wxuserinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wxuserinfo:wxuserinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wxuserinfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wxuserinfo:wxuserinfo:info")
    public R info(@PathVariable("id") Integer id){
			WxuserinfoEntity wxuserinfo = wxuserinfoService.selectById(id);

        return R.ok().put("wxuserinfo", wxuserinfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wxuserinfo:wxuserinfo:save")
    public R save(@RequestBody WxuserinfoEntity wxuserinfo){
			wxuserinfoService.insert(wxuserinfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wxuserinfo:wxuserinfo:update")
    public R update(@RequestBody WxuserinfoEntity wxuserinfo){
        wxuserinfoService.updateById(wxuserinfo);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wxuserinfo:wxuserinfo:delete")
    public R delete(@RequestBody Integer[] ids){
			wxuserinfoService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
