package io.youngking.modules.wxuserinfo.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;

import java.util.Map;

/**
 * 微信用户信息表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 09:39:51
 */
public interface WxuserinfoService extends IService<WxuserinfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    PageUtils bindqueryPage(Map<String, Object> params);
}

