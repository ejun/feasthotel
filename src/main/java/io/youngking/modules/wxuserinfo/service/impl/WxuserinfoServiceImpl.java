package io.youngking.modules.wxuserinfo.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.wxuserinfo.dao.WxuserinfoDao;
import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;
import io.youngking.modules.wxuserinfo.service.WxuserinfoService;


@Service("wxuserinfoService")
public class WxuserinfoServiceImpl extends ServiceImpl<WxuserinfoDao, WxuserinfoEntity> implements WxuserinfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<WxuserinfoEntity> page = this.selectPage(
                new Query<WxuserinfoEntity>(params).getPage(),
                new EntityWrapper<WxuserinfoEntity>().like(StringUtils.isNotBlank(key),"nickname", key)
                        .orNew(StringUtils.isNotBlank(key),"remark like {0}", "%" + key + "%")
                        .orderBy("id desc")
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils bindqueryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<WxuserinfoEntity> page = this.selectPage(
                new Query<WxuserinfoEntity>(params).getPage(),
                new EntityWrapper<WxuserinfoEntity>().where("isstaff=1")
                        .like(StringUtils.isNotBlank(key),"nickname", key)
                        .orNew(StringUtils.isNotBlank(key),"remark like {0}", "%" + key + "%")
                        .orderBy("id desc")
        );

        return new PageUtils(page);
    }

}
