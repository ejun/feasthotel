package io.youngking.modules.wxuserinfo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信用户信息表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 09:39:51
 */
@TableName("tb_wxuserinfo")
public class WxuserinfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 普通用户的标识
	 */
	private String openid;
	/**
	 * 是否订阅
	 */
	private Integer subscribe;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 性别(1:男；2:女)
	 */
	private Integer sex;
	/**
	 * 所用语言
	 */
	private String language;
	/**
	 * 所在城市
	 */
	private String city;
	/**
	 * 所在省份
	 */
	private String province;
	/**
	 * 所在国家
	 */
	private String country;
	/**
	 * 头像地址
	 */
	private String headimgurl;
	/**
	 * 最后一次关注时间
	 */
	private Date subscribeTime;
	/**
	 * 公众号绑定id
	 */
	private String unionid;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 分组id
	 */
	private Integer groupid;
	/**
	 * 个人二维码
	 */
	private String qrurl;
	/**
	 * 条形码路径
	 */
	private String imagepath;
	/**
	 * 刷新时间(每个用户每天刷新一次)
	 */
	private Date refreshtime;
	/**
	 * 是否公司员工(0：否；1：是)
	 */
	private Integer isstaff;
	/**
	 * 订房通知(0：否；1：是)
	 */
	private Integer isdfnotice;
	/**
	 * 订餐通知(0：否；1：是)
	 */
	private Integer isdcnotice;
	/**
	 * 点评通知(0：否；1：是)
	 */
	private Integer isdpnotice;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：普通用户的标识
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：普通用户的标识
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：是否订阅
	 */
	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}
	/**
	 * 获取：是否订阅
	 */
	public Integer getSubscribe() {
		return subscribe;
	}
	/**
	 * 设置：昵称
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * 获取：昵称
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * 设置：性别(1:男；2:女)
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别(1:男；2:女)
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * 设置：所用语言
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * 获取：所用语言
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * 设置：所在城市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：所在城市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：所在省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：所在省份
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：所在国家
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * 获取：所在国家
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * 设置：头像地址
	 */
	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}
	/**
	 * 获取：头像地址
	 */
	public String getHeadimgurl() {
		return headimgurl;
	}
	/**
	 * 设置：最后一次关注时间
	 */
	public void setSubscribeTime(Date subscribeTime) {
		this.subscribeTime = subscribeTime;
	}
	/**
	 * 获取：最后一次关注时间
	 */
	public Date getSubscribeTime() {
		return subscribeTime;
	}
	/**
	 * 设置：公众号绑定id
	 */
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	/**
	 * 获取：公众号绑定id
	 */
	public String getUnionid() {
		return unionid;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：分组id
	 */
	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}
	/**
	 * 获取：分组id
	 */
	public Integer getGroupid() {
		return groupid;
	}
	/**
	 * 设置：个人二维码
	 */
	public void setQrurl(String qrurl) {
		this.qrurl = qrurl;
	}
	/**
	 * 获取：个人二维码
	 */
	public String getQrurl() {
		return qrurl;
	}
	/**
	 * 设置：条形码路径
	 */
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	/**
	 * 获取：条形码路径
	 */
	public String getImagepath() {
		return imagepath;
	}
	/**
	 * 设置：刷新时间(每个用户每天刷新一次)
	 */
	public void setRefreshtime(Date refreshtime) {
		this.refreshtime = refreshtime;
	}
	/**
	 * 获取：刷新时间(每个用户每天刷新一次)
	 */
	public Date getRefreshtime() {
		return refreshtime;
	}

	public Integer getIsstaff() {
		return isstaff;
	}

	public void setIsstaff(Integer isstaff) {
		this.isstaff = isstaff;
	}

	public Integer getIsdfnotice() {
		return isdfnotice;
	}

	public void setIsdfnotice(Integer isdfnotice) {
		this.isdfnotice = isdfnotice;
	}

	public Integer getIsdcnotice() {
		return isdcnotice;
	}

	public void setIsdcnotice(Integer isdcnotice) {
		this.isdcnotice = isdcnotice;
	}

	public Integer getIsdpnotice() {
		return isdpnotice;
	}

	public void setIsdpnotice(Integer isdpnotice) {
		this.isdpnotice = isdpnotice;
	}
}
