package io.youngking.modules.wxuserinfo.dao;

import io.youngking.modules.wxuserinfo.entity.WxuserinfoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 微信用户信息表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 09:39:51
 */
@Mapper
public interface WxuserinfoDao extends BaseMapper<WxuserinfoEntity> {
	
}
