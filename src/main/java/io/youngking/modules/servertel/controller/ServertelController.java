package io.youngking.modules.servertel.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.servertel.entity.ServertelEntity;
import io.youngking.modules.servertel.service.ServertelService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;


/**
 * 客服电话表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:18:59
 */
@RestController
@RequestMapping("servertel/servertel")
public class ServertelController {
    @Autowired
    private ServertelService servertelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("servertel:servertel:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = servertelService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("servertel:servertel:info")
    public R info(@PathVariable("id") Integer id) {
        ServertelEntity servertel = servertelService.selectById(id);

        return R.ok().put("servertel", servertel);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("servertel:servertel:save")
    public R save(@RequestBody ServertelEntity servertel) {
        servertelService.insert(servertel);
        if (servertel.getIsdefault() == 0) {
            ServertelEntity tel = new ServertelEntity();
            tel.setIsdefault(1);
            EntityWrapper ew = new EntityWrapper();
            ew.setEntity(new ServertelEntity());
            ew.where("id <> {0}", servertel.getId());
            servertelService.update(tel, ew);
        }
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("servertel:servertel:update")
    public R update(@RequestBody ServertelEntity servertel) {
        servertelService.updateById(servertel);
        if (servertel.getIsdefault() == 0) {
            ServertelEntity tel = new ServertelEntity();
            tel.setIsdefault(1);
            EntityWrapper ew = new EntityWrapper();
            ew.setEntity(new ServertelEntity());
            ew.where("id <> {0}", servertel.getId());
            servertelService.update(tel, ew);
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("servertel:servertel:delete")
    public R delete(@RequestBody Integer[] ids) {
        servertelService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
