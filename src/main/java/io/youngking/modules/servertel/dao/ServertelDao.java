package io.youngking.modules.servertel.dao;

import io.youngking.modules.servertel.entity.ServertelEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客服电话表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:18:59
 */
@Mapper
public interface ServertelDao extends BaseMapper<ServertelEntity> {
	
}
