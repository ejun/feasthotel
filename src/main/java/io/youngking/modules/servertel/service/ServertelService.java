package io.youngking.modules.servertel.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.servertel.entity.ServertelEntity;

import java.util.Map;

/**
 * 客服电话表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:18:59
 */
public interface ServertelService extends IService<ServertelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

