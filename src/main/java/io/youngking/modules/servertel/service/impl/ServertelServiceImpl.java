package io.youngking.modules.servertel.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.servertel.dao.ServertelDao;
import io.youngking.modules.servertel.entity.ServertelEntity;
import io.youngking.modules.servertel.service.ServertelService;


@Service("servertelService")
public class ServertelServiceImpl extends ServiceImpl<ServertelDao, ServertelEntity> implements ServertelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<ServertelEntity> page = this.selectPage(
                new Query<ServertelEntity>(params).getPage(),
                new EntityWrapper<ServertelEntity>().like(StringUtils.isNotBlank(key),"staffname",key)
                        .orNew(StringUtils.isNotBlank(key),"`tel` like {0}","%"+key+"%").orderBy("id desc")
        );

        return new PageUtils(page);
    }

}
