package io.youngking.modules.servertel.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 客服电话表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:18:59
 */
@TableName("tb_servertel")
public class ServertelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 客服名称
	 */
	private String staffname;
	/**
	 * 电话号码
	 */
	private String tel;
	/**
	 * 当前客服(0:是；1:否)
	 */
	private Integer isdefault;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：客服名称
	 */
	public void setStaffname(String staffname) {
		this.staffname = staffname;
	}
	/**
	 * 获取：客服名称
	 */
	public String getStaffname() {
		return staffname;
	}
	/**
	 * 设置：电话号码
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	/**
	 * 获取：电话号码
	 */
	public String getTel() {
		return tel;
	}
	/**
	 * 设置：当前客服(0:是；1:否)
	 */
	public void setIsdefault(Integer isdefault) {
		this.isdefault = isdefault;
	}
	/**
	 * 获取：当前客服(0:是；1:否)
	 */
	public Integer getIsdefault() {
		return isdefault;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
