package io.youngking.modules.goodrecepititem.dao;

import io.youngking.modules.goodrecepititem.entity.GoodrecepititemEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 商品分单表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-08-01 16:31:21
 */
@Mapper
public interface GoodrecepititemDao extends BaseMapper<GoodrecepititemEntity> {

    List<GoodrecepititemEntity> getlist(Map<String,Object> params);
}
