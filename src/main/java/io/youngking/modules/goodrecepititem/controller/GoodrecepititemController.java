package io.youngking.modules.goodrecepititem.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.goodrecepititem.entity.GoodrecepititemEntity;
import io.youngking.modules.goodrecepititem.service.GoodrecepititemService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 商品分单表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-08-01 16:31:21
 */
@RestController
@RequestMapping("goodrecepititem/goodrecepititem")
public class GoodrecepititemController {
    @Autowired
    private GoodrecepititemService goodrecepititemService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("goodrecepititem:goodrecepititem:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = goodrecepititemService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("goodrecepititem:goodrecepititem:info")
    public R info(@PathVariable("id") Integer id){
			GoodrecepititemEntity goodrecepititem = goodrecepititemService.selectById(id);

        return R.ok().put("goodrecepititem", goodrecepititem);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("goodrecepititem:goodrecepititem:save")
    public R save(@RequestBody GoodrecepititemEntity goodrecepititem){
			goodrecepititemService.insert(goodrecepititem);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("goodrecepititem:goodrecepititem:update")
    public R update(@RequestBody GoodrecepititemEntity goodrecepititem){
			goodrecepititemService.updateById(goodrecepititem);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("goodrecepititem:goodrecepititem:delete")
    public R delete(@RequestBody Integer[] ids){
			goodrecepititemService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
