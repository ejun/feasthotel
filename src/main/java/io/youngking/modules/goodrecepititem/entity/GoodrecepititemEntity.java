package io.youngking.modules.goodrecepititem.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import io.youngking.modules.good.entity.GoodEntity;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 商品分单表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-08-01 16:31:21
 */
@TableName("tb_goodrecepititem")
public class GoodrecepititemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 订单id
	 */
	private Integer receiptid;
	/**
	 * 食品id
	 */
	private Integer goodid;
	/**
	 * 食品名称
	 */
	private String goodname;
	/**
	 * 单价
	 */
	private BigDecimal price;
	/**
	 * 数量
	 */
	private Integer num;
	/**
	 * 合计金额
	 */
	private BigDecimal totalmoney;
	/**
	 * 商品信息
	 */
	@TableField(exist = false)
	private GoodEntity goodEntity;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：订单id
	 */
	public void setReceiptid(Integer receiptid) {
		this.receiptid = receiptid;
	}
	/**
	 * 获取：订单id
	 */
	public Integer getReceiptid() {
		return receiptid;
	}
	/**
	 * 设置：食品id
	 */
	public void setGoodid(Integer goodid) {
		this.goodid = goodid;
	}
	/**
	 * 获取：食品id
	 */
	public Integer getGoodid() {
		return goodid;
	}
	/**
	 * 设置：食品名称
	 */
	public void setGoodname(String goodname) {
		this.goodname = goodname;
	}
	/**
	 * 获取：食品名称
	 */
	public String getGoodname() {
		return goodname;
	}
	/**
	 * 设置：单价
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * 获取：单价
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * 设置：数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
	/**
	 * 获取：数量
	 */
	public Integer getNum() {
		return num;
	}
	/**
	 * 设置：合计金额
	 */
	public void setTotalmoney(BigDecimal totalmoney) {
		this.totalmoney = totalmoney;
	}
	/**
	 * 获取：合计金额
	 */
	public BigDecimal getTotalmoney() {
		return totalmoney;
	}

	public GoodEntity getGoodEntity() {
		return goodEntity;
	}

	public void setGoodEntity(GoodEntity goodEntity) {
		this.goodEntity = goodEntity;
	}
}
