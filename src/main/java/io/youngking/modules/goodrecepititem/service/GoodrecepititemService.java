package io.youngking.modules.goodrecepititem.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.goodrecepititem.entity.GoodrecepititemEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品分单表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-08-01 16:31:21
 */
public interface GoodrecepititemService extends IService<GoodrecepititemEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<GoodrecepititemEntity> getlist(Map<String,Object> params);
}

