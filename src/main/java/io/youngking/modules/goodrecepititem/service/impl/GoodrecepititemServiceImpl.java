package io.youngking.modules.goodrecepititem.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.goodrecepititem.dao.GoodrecepititemDao;
import io.youngking.modules.goodrecepititem.entity.GoodrecepititemEntity;
import io.youngking.modules.goodrecepititem.service.GoodrecepititemService;


@Service("goodrecepititemService")
public class GoodrecepititemServiceImpl extends ServiceImpl<GoodrecepititemDao, GoodrecepititemEntity> implements GoodrecepititemService {

    //根据订单id获取详细
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key=(String)params.get("key");
        String receiptid = (String) params.get("receiptid");
        Page<GoodrecepititemEntity> page = this.selectPage(
                new Query<GoodrecepititemEntity>(params).getPage(),
                new EntityWrapper<GoodrecepititemEntity>().eq(StringUtils.isNotEmpty(receiptid),"receiptid",receiptid)
                        .like("goodname",key).orderBy("id desc")
        );

        return new PageUtils(page);
    }

    @Override
    public List<GoodrecepititemEntity> getlist(Map<String, Object> params) {
        return baseMapper.getlist(params);
    }

}
