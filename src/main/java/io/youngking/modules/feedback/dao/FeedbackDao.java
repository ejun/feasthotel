package io.youngking.modules.feedback.dao;

import io.youngking.modules.feedback.entity.FeedbackEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 用户建议表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 10:53:59
 */
@Mapper
public interface FeedbackDao extends BaseMapper<FeedbackEntity> {

    List<Map<String,Object>> getlist(Map<String, Object> params);

    Map<String,Object> getinfo (Integer id);
}
