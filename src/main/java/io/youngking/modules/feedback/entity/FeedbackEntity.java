package io.youngking.modules.feedback.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户建议表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 10:53:59
 */
@TableName("tb_feedback")
public class FeedbackEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 通知类型(1:订房；2:点餐；3:事中点评)
	 */
	private Integer itype;
	/**
	 * 用户id
	 */
	private Integer customerid;
	/**
	 * 通知内容
	 */
	private String contents;
	/**
	 * 发送时间
	 */
	private Date sendtime;
	/**
	 * 回复内容
	 */
	private String replycontent;
	/**
	 * 回复时间
	 */
	private Date replytime;
	/**
	 * 回复人
	 */
	private String replystaff;
	/**
	 * 好评度
	 */
	private String praise;
	/**
	 * 图片
	 */
	private String imgurl;
	/**
	 * 回复状态(0:未回复，1:已回复)
	 */
	private Integer status;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：通知类型(1:订房；2:点餐；3:事中点评)
	 */
	public void setItype(Integer itype) {
		this.itype = itype;
	}
	/**
	 * 获取：通知类型(1:订房；2:点餐；3:事中点评)
	 */
	public Integer getItype() {
		return itype;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：通知内容
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}
	/**
	 * 获取：通知内容
	 */
	public String getContents() {
		return contents;
	}
	/**
	 * 设置：发送时间
	 */
	public void setSendtime(Date sendtime) {
		this.sendtime = sendtime;
	}
	/**
	 * 获取：发送时间
	 */
	public Date getSendtime() {
		return sendtime;
	}
	/**
	 * 设置：回复内容
	 */
	public void setReplycontent(String replycontent) {
		this.replycontent = replycontent;
	}
	/**
	 * 获取：回复内容
	 */
	public String getReplycontent() {
		return replycontent;
	}
	/**
	 * 设置：回复时间
	 */
	public void setReplytime(Date replytime) {
		this.replytime = replytime;
	}
	/**
	 * 获取：回复时间
	 */
	public Date getReplytime() {
		return replytime;
	}
	/**
	 * 设置：回复人
	 */
	public void setReplystaff(String replystaff) {
		this.replystaff = replystaff;
	}
	/**
	 * 获取：回复人
	 */
	public String getReplystaff() {
		return replystaff;
	}

	public String getPraise() {
		return praise;
	}

	public void setPraise(String praise) {
		this.praise = praise;
	}

	public String getImgurl() {
		return imgurl;
	}

	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
