package io.youngking.modules.feedback.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.feedback.dao.FeedbackDao;
import io.youngking.modules.feedback.entity.FeedbackEntity;
import io.youngking.modules.feedback.service.FeedbackService;


@Service("feedbackService")
public class FeedbackServiceImpl extends ServiceImpl<FeedbackDao, FeedbackEntity> implements FeedbackService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key=(String)params.get("key");
        Page<FeedbackEntity> page = this.selectPage(
                new Query<FeedbackEntity>(params).getPage(),
                new EntityWrapper<FeedbackEntity>().like(StringUtils.isNotBlank(key),"contents",key)
        );

        return new PageUtils(page);
    }

    /**
     * 分页数据
     */
    @Override
    public PageUtils Pagelist(Map<String, Object> params) {
        PageUtils page = new PageUtils();
        page.setList((List<?>) params.get("list"));
        page.setCurrPage(Integer.parseInt(params.get("CurrPage").toString())); //单前页
        page.setPageSize(Integer.parseInt(params.get("PageSize").toString())); //每页多少条
        page.setTotalCount(Integer.parseInt(params.get("TotalCount").toString())); //总条数
        page.setTotalPage(Integer.parseInt(params.get("TotalPage").toString())); //总页数
        return  page ;
    }

    /**
     * 列表数据
     */
    @Override
    public List<Map<String,Object>> getlist(Map<String, Object> params){
        return baseMapper.getlist(params);
    }

    /**
     * 获取详细
     */
    @Override
    public Map<String,Object> getinfo (Integer id){
        return baseMapper.getinfo(id);
    }
}
