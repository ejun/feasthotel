package io.youngking.modules.feedback.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.feedback.entity.FeedbackEntity;

import java.util.List;
import java.util.Map;

/**
 * 用户建议表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 10:53:59
 */
public interface FeedbackService extends IService<FeedbackEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils Pagelist(Map<String, Object> params);

    List<Map<String,Object>> getlist(Map<String, Object> params);

    Map<String,Object> getinfo (Integer id);

}

