package io.youngking.modules.refundreceipt.dao;

import io.youngking.modules.refundreceipt.entity.RefundreceiptEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 退款信息
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-19 14:49:40
 */
@Mapper
public interface RefundreceiptDao extends BaseMapper<RefundreceiptEntity> {
    List<Map<String,Object>> getlist(Map<String, Object> params);

    Map<String,Object> getinfo (Integer id);
}
