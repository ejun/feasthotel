package io.youngking.modules.refundreceipt.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.refundreceipt.dao.RefundreceiptDao;
import io.youngking.modules.refundreceipt.entity.RefundreceiptEntity;
import io.youngking.modules.refundreceipt.service.RefundreceiptService;


@Service("refundreceiptService")
public class RefundreceiptServiceImpl extends ServiceImpl<RefundreceiptDao, RefundreceiptEntity> implements RefundreceiptService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RefundreceiptEntity> page = this.selectPage(
                new Query<RefundreceiptEntity>(params).getPage(),
                new EntityWrapper<RefundreceiptEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 分页数据
     */
    @Override
    public PageUtils Pagelist(Map<String, Object> params) {
        PageUtils page = new PageUtils();
        page.setList((List<?>) params.get("list"));
        page.setCurrPage(Integer.parseInt(params.get("CurrPage").toString())); //单前页
        page.setPageSize(Integer.parseInt(params.get("PageSize").toString())); //每页多少条
        page.setTotalCount(Integer.parseInt(params.get("TotalCount").toString())); //总条数
        page.setTotalPage(Integer.parseInt(params.get("TotalPage").toString())); //总页数
        return  page ;
    }

    /**
     * 列表数据
     */
    @Override
    public List<Map<String,Object>> getlist(Map<String, Object> params){
        return baseMapper.getlist(params);
    }

    /**
     * 获取详细
     */
    @Override
    public Map<String,Object> getinfo (Integer id){
        return baseMapper.getinfo(id);
    }
}
