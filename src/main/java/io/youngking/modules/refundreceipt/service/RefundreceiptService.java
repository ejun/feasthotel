package io.youngking.modules.refundreceipt.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.refundreceipt.entity.RefundreceiptEntity;

import java.util.List;
import java.util.Map;

/**
 * 退款信息
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-19 14:49:40
 */
public interface RefundreceiptService extends IService<RefundreceiptEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils Pagelist(Map<String, Object> params);

    List<Map<String,Object>> getlist(Map<String, Object> params);

    Map<String,Object> getinfo (Integer id);
}

