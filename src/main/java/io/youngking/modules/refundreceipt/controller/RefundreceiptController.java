package io.youngking.modules.refundreceipt.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.toolkit.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.refundreceipt.entity.RefundreceiptEntity;
import io.youngking.modules.refundreceipt.service.RefundreceiptService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 退款信息
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-19 14:49:40
 */
@RestController
@RequestMapping("refundreceipt/refundreceipt")
public class RefundreceiptController {
    @Autowired
    private RefundreceiptService refundreceiptService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("refundreceipt:refundreceipt:list")
    public R list(@RequestParam Map<String, Object> params){
        int currPage = 0;
        int limit = 0;

        Map<String, Object> map = new HashMap<>();
        //分页参数
        if (params.get("page") != null) {
            currPage = Integer.parseInt((String) params.get("page"));
        }
        if (params.get("limit") != null) {
            limit = Integer.parseInt((String) params.get("limit"));
        }
        map.put("key", params.get("key"));
        map.put("offset", (currPage - 1) * limit);
        map.put("limit", limit);

        //获取数据
        List<Map<String, Object>> list = refundreceiptService.getlist(map);
        Map<String, Object> params1 = new HashMap<>();
        params1.put("list", list);
        params1.put("CurrPage", currPage);
        params1.put("PageSize", limit);
        params1.put("TotalCount", list.size());
        params1.put("TotalPage", (int) Math.ceil((double) list.size() / limit));

        PageUtils page = refundreceiptService.Pagelist(params1);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("refundreceipt:refundreceipt:info")
    public R info(@PathVariable("id") Integer id){
        Map<String, Object> map = refundreceiptService.getinfo(id);
        String img = map.get("imgurl").toString();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int j = 0; j < imglist.length; j++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/refund/" + imglist[j];
                newlist += fileSavePath + ",";
            }
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(newlist)) {
            newlist = newlist.substring(0, newlist.length() - 1);
        }
        map.put("imgurl", newlist);
        return R.ok().put("refundreceipt", map);

    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("refundreceipt:refundreceipt:save")
    public R save(@RequestBody RefundreceiptEntity refundreceipt){
			refundreceiptService.insert(refundreceipt);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("refundreceipt:refundreceipt:update")
    public R update(@RequestBody RefundreceiptEntity refundreceipt){
			refundreceiptService.updateById(refundreceipt);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("refundreceipt:refundreceipt:delete")
    public R delete(@RequestBody Integer[] ids){
			refundreceiptService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
