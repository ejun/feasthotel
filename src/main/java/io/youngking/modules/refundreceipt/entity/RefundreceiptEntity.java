package io.youngking.modules.refundreceipt.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 退款信息
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-09-19 14:49:40
 */
@TableName("tb_refundreceipt")
public class RefundreceiptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 退款单编号
	 */
	private String refundcode;
	/**
	 * 退款时间
	 */
	private Date refundtime;
	/**
	 * 退款理由
	 */
	private String refundreason;
	/**
	 * 审核状态(0:待退款；1:退款成功)
	 */
	private Integer status;
	/**
	 * 源订单id
	 */
	private Integer receiptid;
	/**
	 * 图片
	 */
	private String imgurl;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：退款单编号
	 */
	public void setRefundcode(String refundcode) {
		this.refundcode = refundcode;
	}
	/**
	 * 获取：退款单编号
	 */
	public String getRefundcode() {
		return refundcode;
	}
	/**
	 * 设置：退款时间
	 */
	public void setRefundtime(Date refundtime) {
		this.refundtime = refundtime;
	}
	/**
	 * 获取：退款时间
	 */
	public Date getRefundtime() {
		return refundtime;
	}
	/**
	 * 设置：退款理由
	 */
	public void setRefundreason(String refundreason) {
		this.refundreason = refundreason;
	}
	/**
	 * 获取：退款理由
	 */
	public String getRefundreason() {
		return refundreason;
	}
	/**
	 * 设置：审核状态(0:待退款；1:退款成功)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：审核状态(0:待退款；1:退款成功)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：源订单id
	 */
	public void setReceiptid(Integer receiptid) {
		this.receiptid = receiptid;
	}
	/**
	 * 获取：源订单id
	 */
	public Integer getReceiptid() {
		return receiptid;
	}
	/**
	 * 设置：图片
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 获取：图片
	 */
	public String getImgurl() {
		return imgurl;
	}
}
