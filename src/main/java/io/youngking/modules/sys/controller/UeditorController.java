package io.youngking.modules.sys.controller;

import io.youngking.common.xss.XssHttpServletRequestWrapper;
import io.youngking.ueditor.ActionEnter;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

/**
 * 用于处理关于ueditor插件相关的请求
 * @author Guoqing
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/sys/ueditor")
public class UeditorController {

    @RequestMapping(value = "/exec")
    @ResponseBody
    public String exec(HttpServletRequest request) throws UnsupportedEncodingException {
        HttpServletRequest orgRequest =  XssHttpServletRequestWrapper.getOrgRequest(request);
        orgRequest.setCharacterEncoding("utf-8");
        String rootPath = orgRequest.getRealPath("/");
        return new ActionEnter( orgRequest, rootPath).exec();
    }
}