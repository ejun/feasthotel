package io.youngking.modules.articlearea.dao;

import io.youngking.modules.articlearea.entity.ArticleareaEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文章区域表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 10:52:11
 */
@Mapper
public interface ArticleareaDao extends BaseMapper<ArticleareaEntity> {
	
}
