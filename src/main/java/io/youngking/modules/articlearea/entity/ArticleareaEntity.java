package io.youngking.modules.articlearea.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章区域表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 10:52:11
 */
@TableName("tb_articlearea")
public class ArticleareaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 区域编号
	 */
	private String areacode;
	/**
	 * 区域名称
	 */
	private String areaname;
	/**
	 * 是否显示(0:是；1:否)
	 */
	private Integer isshow;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 排序
	 */
	private String remark;
	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：区域编号
	 */
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	/**
	 * 获取：区域编号
	 */
	public String getAreacode() {
		return areacode;
	}
	/**
	 * 设置：区域名称
	 */
	public void setAreaname(String areaname) {
		this.areaname = areaname;
	}
	/**
	 * 获取：区域名称
	 */
	public String getAreaname() {
		return areaname;
	}
	/**
	 * 设置：是否显示(0:是；1:否)
	 */
	public void setIsshow(Integer isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否显示(0:是；1:否)
	 */
	public Integer getIsshow() {
		return isshow;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
