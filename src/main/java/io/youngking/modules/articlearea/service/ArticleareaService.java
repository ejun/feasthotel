package io.youngking.modules.articlearea.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.articlearea.entity.ArticleareaEntity;

import java.util.Map;

/**
 * 文章区域表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 10:52:11
 */
public interface ArticleareaService extends IService<ArticleareaEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

