package io.youngking.modules.articlearea.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.articlearea.dao.ArticleareaDao;
import io.youngking.modules.articlearea.entity.ArticleareaEntity;
import io.youngking.modules.articlearea.service.ArticleareaService;


@Service("articleareaService")
public class ArticleareaServiceImpl extends ServiceImpl<ArticleareaDao, ArticleareaEntity> implements ArticleareaService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        Page<ArticleareaEntity> page = this.selectPage(
                new Query<ArticleareaEntity>(params).getPage(),
                new EntityWrapper<ArticleareaEntity>().like(StringUtils.isNotBlank(key), "areacode", key)
                        .orNew(StringUtils.isNotBlank(key), "areaname like {0}", "%" + key + "%")
                        .orderBy("sort desc,id desc")
        );

        return new PageUtils(page);
    }

}
