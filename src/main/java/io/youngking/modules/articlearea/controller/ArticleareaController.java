package io.youngking.modules.articlearea.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.articlearea.entity.ArticleareaEntity;
import io.youngking.modules.articlearea.service.ArticleareaService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 文章区域表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 10:52:11
 */
@RestController
@RequestMapping("articlearea/articlearea")
public class ArticleareaController {
    @Autowired
    private ArticleareaService articleareaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("articlearea:articlearea:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = articleareaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("articlearea:articlearea:info")
    public R info(@PathVariable("id") Integer id){
			ArticleareaEntity articlearea = articleareaService.selectById(id);

        return R.ok().put("articlearea", articlearea);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("articlearea:articlearea:save")
    public R save(@RequestBody ArticleareaEntity articlearea){
			articleareaService.insert(articlearea);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("articlearea:articlearea:update")
    public R update(@RequestBody ArticleareaEntity articlearea){
			articleareaService.updateById(articlearea);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("articlearea:articlearea:delete")
    public R delete(@RequestBody Integer[] ids){
			articleareaService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 文章区域列表数据(添加、修改文章)
     */
    @GetMapping("/select")
    @RequiresPermissions("articlearea:articlearea:select")
    public R select(){
        //查询列表数据
        List<ArticleareaEntity> areaList = articleareaService.selectList(null);
        return R.ok().put("areaList", areaList);
    }
}
