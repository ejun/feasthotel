package io.youngking.modules.hotelinfo.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.hotelinfo.dao.HotelinfoDao;
import io.youngking.modules.hotelinfo.entity.HotelinfoEntity;
import io.youngking.modules.hotelinfo.service.HotelinfoService;


@Service("hotelinfoService")
public class HotelinfoServiceImpl extends ServiceImpl<HotelinfoDao, HotelinfoEntity> implements HotelinfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<HotelinfoEntity> page = this.selectPage(
            new Query<HotelinfoEntity>(params).getPage(),
            new EntityWrapper<HotelinfoEntity>().like(StringUtils.isNotBlank(key),"name",key)
            .orNew(StringUtils.isNotBlank(key),"`describe` like {0}","%"+key+"%").orderBy("id desc")
        );

        return new PageUtils(page);
    }

}
