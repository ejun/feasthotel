package io.youngking.modules.hotelinfo.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.hotelinfo.entity.HotelinfoEntity;

import java.util.Map;

/**
 * 酒店信息表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 15:25:52
 */
public interface HotelinfoService extends IService<HotelinfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

