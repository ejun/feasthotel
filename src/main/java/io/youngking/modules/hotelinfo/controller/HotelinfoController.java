package io.youngking.modules.hotelinfo.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.hotelinfo.entity.HotelinfoEntity;
import io.youngking.modules.hotelinfo.service.HotelinfoService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 酒店信息表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 15:25:52
 */
@RestController
@RequestMapping("hotelinfo/hotelinfo")
public class HotelinfoController {
    @Autowired
    private HotelinfoService hotelinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("hotelinfo:hotelinfo:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = hotelinfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("hotelinfo:hotelinfo:info")
    public R info(@PathVariable("id") Integer id) {
        HotelinfoEntity hotelinfo = hotelinfoService.selectById(id);
        String img = hotelinfo.getImgurl();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/hotelinfo/" + imglist[i];
                newlist += "{name:'"+imglist[i]+"',"+"url:'" + fileSavePath + "'}" + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = "[" + newlist.substring(0, newlist.length() - 1) + "]";
        }
        hotelinfo.setImgurl(newlist);
        return R.ok().put("hotelinfo", hotelinfo);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/hotelinfo/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/hotelinfo/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("hotelinfo:hotelinfo:save")
    public R save(@RequestBody HotelinfoEntity hotelinfo) {
        hotelinfoService.insert(hotelinfo);
        if (hotelinfo.getIsdefault() == 0) {
            HotelinfoEntity hotel = new HotelinfoEntity();
            hotel.setIsdefault(1);
            EntityWrapper ew = new EntityWrapper();
            ew.setEntity(new HotelinfoEntity());
            ew.where("id <> {0}", hotelinfo.getId());
            hotelinfoService.update(hotel, ew);
        }
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("hotelinfo:hotelinfo:update")
    public R update(@RequestBody HotelinfoEntity hotelinfo) {
        hotelinfoService.updateById(hotelinfo);
        if (hotelinfo.getIsdefault() == 0) {
            HotelinfoEntity hotel = new HotelinfoEntity();
            hotel.setIsdefault(1);
            EntityWrapper ew = new EntityWrapper();
            ew.setEntity(new HotelinfoEntity());
            ew.where("id <> {0}", hotelinfo.getId());
            hotelinfoService.update(hotel, ew);
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("hotelinfo:hotelinfo:delete")
    public R delete(@RequestBody Integer[] ids) {
        hotelinfoService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
