package io.youngking.modules.hotelinfo.dao;

import io.youngking.modules.hotelinfo.entity.HotelinfoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 酒店信息表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 15:25:52
 */
@Mapper
public interface HotelinfoDao extends BaseMapper<HotelinfoEntity> {
	
}
