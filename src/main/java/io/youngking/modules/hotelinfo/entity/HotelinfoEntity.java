package io.youngking.modules.hotelinfo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 酒店信息表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 15:25:52
 */
@TableName("tb_hotelinfo")
public class HotelinfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 酒店名称
	 */
	private String name;
	/**
	 * 描述
	 */
	private String describe;
	/**
	 * 图片
	 */
	private String imgurl;
	/**
	 * 入住时间
	 */
	private String settletime;
	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 显示状态(0:否；1:是)
	 */
	private Integer isdefault;
	/**
	 * 酒店设施
	 */
	private String installation;
	/**
	 * 酒店简介
	 */
	private String introduction;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：酒店名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：酒店名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：描述
	 */
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	/**
	 * 获取：描述
	 */
	public String getDescribe() {
		return describe;
	}
	/**
	 * 设置：图片
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 获取：图片
	 */
	public String getImgurl() {
		return imgurl;
	}
	/**
	 * 设置：入住时间
	 */
	public void setSettletime(String settletime) {
		this.settletime = settletime;
	}
	/**
	 * 获取：入住时间
	 */
	public String getSettletime() {
		return settletime;
	}
	/**
	 * 设置：电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：显示状态(0:否；1:是)
	 */
	public void setIsdefault(Integer isdefault) {
		this.isdefault = isdefault;
	}
	/**
	 * 获取：显示状态(0:否；1:是)
	 */
	public Integer getIsdefault() {
		return isdefault;
	}

	public String getInstallation() {
		return installation;
	}

	public void setInstallation(String installation) {
		this.installation = installation;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
}
