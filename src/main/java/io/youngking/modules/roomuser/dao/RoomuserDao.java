package io.youngking.modules.roomuser.dao;

import io.youngking.modules.roomuser.entity.RoomuserEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 房间用户表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 14:56:04
 */
@Mapper
public interface RoomuserDao extends BaseMapper<RoomuserEntity> {

    List<RoomuserEntity> getlist(Map<String, Object> params);
}
