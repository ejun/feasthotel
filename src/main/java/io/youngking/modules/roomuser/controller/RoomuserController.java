package io.youngking.modules.roomuser.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.roomuser.entity.RoomuserEntity;
import io.youngking.modules.roomuser.service.RoomuserService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 房间用户表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 14:56:04
 */
@RestController
@RequestMapping("roomuser/roomuser")
public class RoomuserController {
    @Autowired
    private RoomuserService roomuserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("roomuser:roomuser:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = roomuserService.queryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        page.setList(roomuserService.getlist(params1));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("roomuser:roomuser:info")
    public R info(@PathVariable("id") Integer id){
			RoomuserEntity roomuser = roomuserService.selectById(id);

        return R.ok().put("roomuser", roomuser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("roomuser:roomuser:save")
    public R save(@RequestBody RoomuserEntity roomuser){
        roomuser.setCreatetime(new Date());
			roomuserService.insert(roomuser);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("roomuser:roomuser:update")
    public R update(@RequestBody RoomuserEntity roomuser){
			roomuserService.updateById(roomuser);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("roomuser:roomuser:delete")
    public R delete(@RequestBody Integer[] ids){
			roomuserService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
