package io.youngking.modules.roomuser.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 房间用户表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 14:56:04
 */
@TableName("tb_roomuser")
public class RoomuserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 房间id
	 */
	private Integer roomid;
	/**
	 * 用户id
	 */
	private Integer userid;
	/**
	 * 绑定时间
	 */
	private Date createtime;
	/**
	 * 入住状态(0:未入住；1:已入住)
	 */
	private Integer status;
	/**
	 * 入住次数
	 */
	private Integer settlenum;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：房间id
	 */
	public void setRoomid(Integer roomid) {
		this.roomid = roomid;
	}
	/**
	 * 获取：房间id
	 */
	public Integer getRoomid() {
		return roomid;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：绑定时间
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：绑定时间
	 */
	public Date getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：入住状态(0:未入住；1:已入住)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：入住状态(0:未入住；1:已入住)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：入住次数
	 */
	public void setSettlenum(Integer settlenum) {
		this.settlenum = settlenum;
	}
	/**
	 * 获取：入住次数
	 */
	public Integer getSettlenum() {
		return settlenum;
	}
}
