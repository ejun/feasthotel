package io.youngking.modules.roomuser.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.roomuser.dao.RoomuserDao;
import io.youngking.modules.roomuser.entity.RoomuserEntity;
import io.youngking.modules.roomuser.service.RoomuserService;


@Service("roomuserService")
public class RoomuserServiceImpl extends ServiceImpl<RoomuserDao, RoomuserEntity> implements RoomuserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("customername");
        Page<RoomuserEntity> page = this.selectPage(
                new Query<RoomuserEntity>(params).getPage(),
                new EntityWrapper<RoomuserEntity>().like(StringUtils.isNotBlank(key),"username",key)
        );

        return new PageUtils(page);
    }

    @Override
    public List<RoomuserEntity> getlist(Map<String, Object> params) {
        return baseMapper.getlist(params);
    }

}
