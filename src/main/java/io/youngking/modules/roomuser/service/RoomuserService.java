package io.youngking.modules.roomuser.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.roomuser.entity.RoomuserEntity;

import java.util.List;
import java.util.Map;

/**
 * 房间用户表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 14:56:04
 */
public interface RoomuserService extends IService<RoomuserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<RoomuserEntity> getlist(Map<String, Object> params);
}

