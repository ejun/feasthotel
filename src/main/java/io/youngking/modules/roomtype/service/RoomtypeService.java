package io.youngking.modules.roomtype.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.roomtype.entity.RoomtypeEntity;

import java.util.Map;

/**
 * 房间类型表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 11:56:05
 */
public interface RoomtypeService extends IService<RoomtypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

