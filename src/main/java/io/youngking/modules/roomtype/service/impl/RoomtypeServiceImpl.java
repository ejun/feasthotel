package io.youngking.modules.roomtype.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;
import io.youngking.modules.roomtype.dao.RoomtypeDao;
import io.youngking.modules.roomtype.entity.RoomtypeEntity;
import io.youngking.modules.roomtype.service.RoomtypeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("roomtypeService")
public class RoomtypeServiceImpl extends ServiceImpl<RoomtypeDao, RoomtypeEntity> implements RoomtypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key=(String)params.get("key");
        Page<RoomtypeEntity> page = this.selectPage(
                new Query<RoomtypeEntity>(params).getPage(),
                new EntityWrapper<RoomtypeEntity>().like(StringUtils.isNotBlank(key),"typename",key)

        );

        return new PageUtils(page);
    }

}
