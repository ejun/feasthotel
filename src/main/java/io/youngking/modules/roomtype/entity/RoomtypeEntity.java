package io.youngking.modules.roomtype.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 房间类型表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 11:56:05
 */
@TableName("tb_roomtype")
public class RoomtypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 类型名称
	 */
	private String typename;
	/**
	 * 库存数量
	 */
	private Integer stock;
	/**
	 * 价格
	 */
	private BigDecimal price;
	/**
	 * 所在楼层
	 */
	private String floor;
	/**
	 * 规格
	 */
	private String format;
	/**
	 * 描述
	 */
	private String describe;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 图片
	 */
	private String imgurl;
	/**
	 * 标签
	 */
	private String tag;
	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类型名称
	 */
	public void setTypename(String typename) {
		this.typename = typename;
	}
	/**
	 * 获取：类型名称
	 */
	public String getTypename() {
		return typename;
	}
	/**
	 * 设置：库存数量
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	/**
	 * 获取：库存数量
	 */
	public Integer getStock() {
		return stock;
	}
	/**
	 * 设置：价格
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * 获取：价格
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * 设置：所在楼层
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}
	/**
	 * 获取：所在楼层
	 */
	public String getFloor() {
		return floor;
	}
	/**
	 * 设置：规格
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	/**
	 * 获取：规格
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * 设置：描述
	 */
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	/**
	 * 获取：描述
	 */
	public String getDescribe() {
		return describe;
	}
	/**
	 * 设置：状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：图片
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 获取：图片
	 */
	public String getImgurl() {
		return imgurl;
	}
	/**
	 * 设置：标签
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * 获取：标签
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
}
