package io.youngking.modules.roomtype.dao;

import io.youngking.modules.roomtype.entity.RoomtypeEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 房间类型表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 11:56:05
 */
@Mapper
public interface RoomtypeDao extends BaseMapper<RoomtypeEntity> {

}
