package io.youngking.modules.roomtype.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.youngking.RenrenApplication;
import io.youngking.common.exception.RRException;
import io.youngking.common.utils.ImageHelper;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;
import io.youngking.modules.goodtype.entity.GoodtypeEntity;
import io.youngking.modules.roomtype.entity.RoomtypeEntity;
import io.youngking.modules.roomtype.service.RoomtypeService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * 房间类型表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 11:56:05
 */
@RestController
@RequestMapping("roomtype/roomtype")
public class RoomtypeController {
    @Autowired
    private RoomtypeService roomtypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("roomtype:roomtype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = roomtypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("roomtype:roomtype:info")
    public R info(@PathVariable("id") Integer id){
			RoomtypeEntity roomtype = roomtypeService.selectById(id);
        String img = roomtype.getImgurl();
        String newlist = "";
        if (StringUtils.isNotEmpty(img)) {
            String[] imglist = img.split(",");
            for (int i = 0; i < imglist.length; i++) {
                String fileSavePath = "http://www.yanzuohotel.com/shenglian-fast/static/roomtype/" + imglist[i];
                newlist += "{name:'"+imglist[i]+"',"+"url:'" + fileSavePath + "'}" + ",";
            }
        }
        if (StringUtils.isNotEmpty(newlist)) {
            newlist = "[" + newlist.substring(0, newlist.length() - 1) + "]";
        }
        roomtype.setImgurl(newlist);
        return R.ok().put("roomtype", roomtype);
    }

    /**
     * 保存图片
     */
    @RequestMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        String pictureName = UUID.randomUUID().toString() + ".jpg";
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/roomtype/";
        File f = new File(fileSavePath);
        // 判断路径是否存在，不存在则创建
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageHelper.compress(file.getInputStream(), new File(fileSavePath + pictureName), 670);
        return R.ok().put("name", pictureName);
    }

    /**
     * 删除图片
     */
    @PostMapping("/picdelete")
    private R picdelete(@RequestBody String simpleObject) {
        JSONObject mapTypes = JSON.parseObject(simpleObject);
        String name = mapTypes.getString("name");
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String fileSavePath = path + "/static/roomtype/" + name;
        //删除文件夹中的图片文件
        File file = new File(fileSavePath);//文件路径
        file.delete();
        return R.ok();
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("roomtype:roomtype:save")
    public R save(@RequestBody RoomtypeEntity roomtype){
			roomtypeService.insert(roomtype);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("roomtype:roomtype:update")
    public R update(@RequestBody RoomtypeEntity roomtype){
			roomtypeService.updateById(roomtype);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("roomtype:roomtype:delete")
    public R delete(@RequestBody Integer[] ids){
			roomtypeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

    @RequestMapping("/selectlist")
    public R selectlist(){
        List<RoomtypeEntity> typeList=roomtypeService.selectList(null);
        return R.ok().put("typeList",typeList);
    }
}
