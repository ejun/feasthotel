package io.youngking.modules.goodreceipt.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 商品订单表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 11:54:05
 */
@TableName("tb_goodreceipt")
public class GoodreceiptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 订单编号
	 */
	private String receiptcode;
	/**
	 * 用户id
	 */
	private Integer customerid;
	/**
	 * 送餐地址
	 */
	private String address;
	/**
	 * 送餐时间
	 */
	private String sendtime;
	/**
	 * 合计金额
	 */
	private BigDecimal totalmoney;
	/**
	 * 订单状态(0:未付款；1:待配送；2:已完成)
	 */
	private Integer status;
	/**
	 * 用户姓名
	 */
	private String customername;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 下单时间
	 */
	private Date chargetime;
	/**
	 * 用餐人数
	 */
	private String peoplenum;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：订单编号
	 */
	public void setReceiptcode(String receiptcode) {
		this.receiptcode = receiptcode;
	}
	/**
	 * 获取：订单编号
	 */
	public String getReceiptcode() {
		return receiptcode;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：送餐地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：送餐地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：送餐时间
	 */
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}
	/**
	 * 获取：送餐时间
	 */
	public String getSendtime() {
		return sendtime;
	}
	/**
	 * 设置：合计金额
	 */
	public void setTotalmoney(BigDecimal totalmoney) {
		this.totalmoney = totalmoney;
	}
	/**
	 * 获取：合计金额
	 */
	public BigDecimal getTotalmoney() {
		return totalmoney;
	}
	/**
	 * 设置：订单状态(0:未付款；1:待配送；2:已完成)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：订单状态(0:未付款；1:待配送；2:已完成)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：用户姓名
	 */
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	/**
	 * 获取：用户姓名
	 */
	public String getCustomername() {
		return customername;
	}
	/**
	 * 设置：联系电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：联系电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：下单时间
	 */
	public void setChargetime(Date chargetime) {
		this.chargetime = chargetime;
	}
	/**
	 * 获取：下单时间
	 */
	public Date getChargetime() {
		return chargetime;
	}

	public String getPeoplenum() {
		return peoplenum;
	}

	public void setPeoplenum(String peoplenum) {
		this.peoplenum = peoplenum;
	}
}
