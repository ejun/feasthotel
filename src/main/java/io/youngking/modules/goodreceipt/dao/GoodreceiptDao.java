package io.youngking.modules.goodreceipt.dao;

import io.youngking.modules.goodreceipt.entity.GoodreceiptEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品订单表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 11:54:05
 */
@Mapper
public interface GoodreceiptDao extends BaseMapper<GoodreceiptEntity> {
	
}
