package io.youngking.modules.goodreceipt.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.youngking.modules.goodrecepititem.entity.GoodrecepititemEntity;
import io.youngking.modules.goodrecepititem.service.GoodrecepititemService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.goodreceipt.entity.GoodreceiptEntity;
import io.youngking.modules.goodreceipt.service.GoodreceiptService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 商品订单表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 11:54:05
 */
@RestController
@RequestMapping("goodreceipt/goodreceipt")
public class GoodreceiptController {
    @Autowired
    private GoodreceiptService goodreceiptService;
    @Autowired
    private GoodrecepititemService goodrecepititemService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("goodreceipt:goodreceipt:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = goodreceiptService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 用户订单
     */
    @RequestMapping("/orderlist")
    public R orderlist(@RequestParam Map<String, Object> params){
        PageUtils page = goodreceiptService.requeryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("goodreceipt:goodreceipt:info")
    public R info(@PathVariable("id") Integer id){
			GoodreceiptEntity goodreceipt = goodreceiptService.selectById(id);

        return R.ok().put("goodreceipt", goodreceipt);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("goodreceipt:goodreceipt:save")
    public R save(@RequestBody GoodreceiptEntity goodreceipt){
			goodreceiptService.insert(goodreceipt);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("goodreceipt:goodreceipt:update")
    public R update(@RequestBody GoodreceiptEntity goodreceipt){
			goodreceiptService.updateById(goodreceipt);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("goodreceipt:goodreceipt:delete")
    public R delete(@RequestBody Integer[] ids){
        for (int i = 0; i <ids.length ; i++) {
            goodrecepititemService.delete(new EntityWrapper<GoodrecepititemEntity>().eq("receiptid",ids[i]));
            goodreceiptService.deleteById(ids[i]);
        }
        return R.ok();
    }

}
