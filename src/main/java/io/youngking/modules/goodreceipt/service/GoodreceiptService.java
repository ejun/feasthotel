package io.youngking.modules.goodreceipt.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.goodreceipt.entity.GoodreceiptEntity;

import java.util.Map;

/**
 * 商品订单表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-27 11:54:05
 */
public interface GoodreceiptService extends IService<GoodreceiptEntity> {

    PageUtils queryPage(Map<String, Object> params);
    /**
     *     根据用户查询订单
     */
    PageUtils requeryPage(Map<String, Object> params);
}

