package io.youngking.modules.goodreceipt.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.goodreceipt.dao.GoodreceiptDao;
import io.youngking.modules.goodreceipt.entity.GoodreceiptEntity;
import io.youngking.modules.goodreceipt.service.GoodreceiptService;


@Service("goodreceiptService")
public class GoodreceiptServiceImpl extends ServiceImpl<GoodreceiptDao, GoodreceiptEntity> implements GoodreceiptService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        Page<GoodreceiptEntity> page = this.selectPage(
                new Query<GoodreceiptEntity>(params).getPage(),
                new EntityWrapper<GoodreceiptEntity>().like(StringUtils.isNotBlank(key), "receiptcode", key)
                        .orNew(StringUtils.isNotBlank(key), "`customername` like {0}", "%" + key + "%")
                        .orderBy("id desc")
        );

        return new PageUtils(page);
    }
    /**
     * 根据用户查询订单
     * @param params
     * @return
     */
    @Override
    public PageUtils requeryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        String customerid = (String) params.get("customerid");
        Page<GoodreceiptEntity> page = this.selectPage(
                new Query<GoodreceiptEntity>(params).getPage(),
                new EntityWrapper<GoodreceiptEntity>().andNew(StringUtils.isNotBlank(customerid), "customerid = {0}", customerid).like(StringUtils.isNotBlank(key), "receiptcode", key)
                        .orNew(StringUtils.isNotBlank(key), "`customername` like {0}", "%" + key + "%")
                        .orderBy("id desc")
        );

        return new PageUtils(page);
    }
}
