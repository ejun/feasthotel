package io.youngking.modules.roominfo.dao;

import io.youngking.modules.roominfo.entity.RoominfoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 房间信息表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 12:52:25
 */
@Mapper
public interface RoominfoDao extends BaseMapper<RoominfoEntity> {
    List<RoominfoEntity> getlist(Map<String,Object> params);
}
