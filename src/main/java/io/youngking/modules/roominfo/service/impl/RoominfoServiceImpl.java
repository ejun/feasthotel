package io.youngking.modules.roominfo.service.impl;

import com.baomidou.mybatisplus.enums.SqlLike;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.roominfo.dao.RoominfoDao;
import io.youngking.modules.roominfo.entity.RoominfoEntity;
import io.youngking.modules.roominfo.service.RoominfoService;


@Service("roominfoService")
public class RoominfoServiceImpl extends ServiceImpl<RoominfoDao, RoominfoEntity> implements RoominfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key=(String)params.get("key");
        Page<RoominfoEntity> page = this.selectPage(
                new Query<RoominfoEntity>(params).getPage(),
                new EntityWrapper<RoominfoEntity>().like(StringUtils.isNotBlank(key),"roomcode",key).orNew(StringUtils.isNotBlank(key),"'roomname' like {0}","%"+key+"%" )
        );
        return new PageUtils(page);
    }

    @Override
    public List<RoominfoEntity> getlist(Map<String, Object> params) {
        return baseMapper.getlist(params);
    }

}
