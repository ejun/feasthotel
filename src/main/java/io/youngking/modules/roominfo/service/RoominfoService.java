package io.youngking.modules.roominfo.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.roominfo.entity.RoominfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 房间信息表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 12:52:25
 */
public interface RoominfoService extends IService<RoominfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<RoominfoEntity> getlist(Map<String, Object> params);
}

