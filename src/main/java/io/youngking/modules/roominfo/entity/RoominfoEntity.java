package io.youngking.modules.roominfo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 房间信息表
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 12:52:25
 */
@TableName("tb_roominfo")
public class RoominfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 房间类型
	 */
	private Integer typeid;
	/**
	 * 房间编号
	 */
	private String roomcode;
	/**
	 * 房间名称
	 */
	private String roomname;
	/**
	 * 房间秘钥
	 */
	private String secretkey;
	/**
	 * 楼层
	 */
	private Integer floor;
	/**
	 * 是否有窗户(0:是；1:否)
	 */
	private Integer iswindow;
	/**
	 * 朝向
	 */
	private String towards;
	/**
	 * 面积
	 */
	private Float area;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 状态(0:空房；1:预定；2:入住)
	 */
	private Integer status;

	public Integer getTypeid() {
		return typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：房间编号
	 */
	public void setRoomcode(String roomcode) {
		this.roomcode = roomcode;
	}
	/**
	 * 获取：房间编号
	 */
	public String getRoomcode() {
		return roomcode;
	}
	/**
	 * 设置：房间名称
	 */
	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}
	/**
	 * 获取：房间名称
	 */
	public String getRoomname() {
		return roomname;
	}
	/**
	 * 设置：房间秘钥
	 */
	public void setSecretkey(String secretkey) {
		this.secretkey = secretkey;
	}
	/**
	 * 获取：房间秘钥
	 */
	public String getSecretkey() {
		return secretkey;
	}
	/**
	 * 设置：楼层
	 */
	public void setFloor(Integer floor) {
		this.floor = floor;
	}
	/**
	 * 获取：楼层
	 */
	public Integer getFloor() {
		return floor;
	}
	/**
	 * 设置：是否有窗户(0:是；1:否)
	 */
	public void setIswindow(Integer iswindow) {
		this.iswindow = iswindow;
	}
	/**
	 * 获取：是否有窗户(0:是；1:否)
	 */
	public Integer getIswindow() {
		return iswindow;
	}
	/**
	 * 设置：朝向
	 */
	public void setTowards(String towards) {
		this.towards = towards;
	}
	/**
	 * 获取：朝向
	 */
	public String getTowards() {
		return towards;
	}
	/**
	 * 设置：面积
	 */
	public void setArea(Float area) {
		this.area = area;
	}
	/**
	 * 获取：面积
	 */
	public Float getArea() {
		return area;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：状态(0:空房；1:预定；2:入住)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态(0:空房；1:预定；2:入住)
	 */
	public Integer getStatus() {
		return status;
	}
}
