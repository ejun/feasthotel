package io.youngking.modules.roominfo.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.youngking.modules.goodtype.entity.GoodtypeEntity;
import io.youngking.modules.roomtype.entity.RoomtypeEntity;
import io.youngking.modules.roomtype.service.RoomtypeService;
import io.youngking.modules.roomuser.entity.RoomuserEntity;
import io.youngking.modules.roomuser.service.RoomuserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.youngking.modules.roominfo.entity.RoominfoEntity;
import io.youngking.modules.roominfo.service.RoominfoService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;

import static io.youngking.common.utils.Common.getRandom;


/**
 * 房间信息表
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-24 12:52:25
 */
@RestController
@RequestMapping("roominfo/roominfo")
public class RoominfoController {
    @Autowired
    private RoominfoService roominfoService;
    @Autowired
    private RoomtypeService roomtypeService;
    @Autowired
    private RoomuserService roomuserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("roominfo:roominfo:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = roominfoService.queryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        page.setList(roominfoService.getlist(params1));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("roominfo:roominfo:info")
    public R info(@PathVariable("id") Integer id) {
        RoominfoEntity roominfo = roominfoService.selectById(id);

        return R.ok().put("roominfo", roominfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("roominfo:roominfo:save")
    public R save(@RequestBody RoominfoEntity roominfo) {
        roominfoService.insert(roominfo);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new RoomtypeEntity());
        ew.where("id = {0}", roominfo.getTypeid());
        RoomtypeEntity roomtypeEntity = roomtypeService.selectOne(ew);
        roomtypeEntity.setStock(roomtypeEntity.getStock() + 1);
        roomtypeService.updateById(roomtypeEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("roominfo:roominfo:update")
    public R update(@RequestBody RoominfoEntity roominfo) {
        roominfoService.updateById(roominfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("roominfo:roominfo:delete")
    public R delete(@RequestBody Integer[] ids) {
        for (int i = 0; i < ids.length; i++) {
            RoominfoEntity roominfoEntity = roominfoService.selectById(Integer.parseInt(ids[i].toString()));
            RoomtypeEntity roomtypeEntity = roomtypeService.selectById(roominfoEntity.getTypeid());
            roomtypeEntity.setStock(roomtypeEntity.getStock() - 1);
            roomtypeService.updateById(roomtypeEntity);
            roominfoService.deleteById(roominfoEntity);
        }
        return R.ok();
    }

    /**
     * 开房
     */
    @RequestMapping("/open")
    @RequiresPermissions("roominfo:roominfo:open")
    public R open(@RequestBody Integer id) {
        //房间状态改为入住
        RoominfoEntity roominfoEntity = roominfoService.selectById(id);
        roominfoEntity.setSecretkey(getRandom(5));
        roominfoEntity.setStatus(2);
        roominfoService.updateById(roominfoEntity);
        //房型库存减1
/*        RoomtypeEntity roomtypeEntity = roomtypeService.selectById(roominfoEntity.getTypeid());
        roomtypeEntity.setStock(roomtypeEntity.getStock() - 1);
        roomtypeService.updateById(roomtypeEntity);*/
        return R.ok();
    }

    /**
     * 退房
     */
    @RequestMapping("/reset")
    @RequiresPermissions("roominfo:roominfo:reset")
    public R reset(@RequestBody Integer id) {
        //房间状态改为空房
        RoominfoEntity roominfoEntity = roominfoService.selectById(id);
        roominfoEntity.setSecretkey("");
        roominfoEntity.setStatus(0);
        roominfoService.updateById(roominfoEntity);
        //房型库存加1
        RoomtypeEntity roomtypeEntity = roomtypeService.selectById(roominfoEntity.getTypeid());
        roomtypeEntity.setStock(roomtypeEntity.getStock() + 1);
        roomtypeService.updateById(roomtypeEntity);
        //删除用户房间绑定状态
        RoomuserEntity roomuserEntity = new RoomuserEntity();
        roomuserEntity.setStatus(0);
        EntityWrapper ew = new EntityWrapper();
        ew.setEntity(new RoomuserEntity());
        ew.eq("roomid",roominfoEntity.getId());
        roomuserService.update(roomuserEntity,ew);
        return R.ok();
    }


}
