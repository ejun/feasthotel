package io.youngking.modules.cart.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.cart.entity.CartEntity;

import java.util.List;
import java.util.Map;

/**
 * 购物车
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:49:27
 */
public interface CartService extends IService<CartEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CartEntity> getlist(Map<String,Object> params);
}

