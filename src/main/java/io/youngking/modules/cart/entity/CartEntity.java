package io.youngking.modules.cart.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 购物车
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:49:27
 */
@TableName("tb_cart")
public class CartEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */
	private Integer customerid;
	/**
	 * 食品id
	 */
	private Integer foodid;
	/**
	 * 单价
	 */
	private BigDecimal price;
	/**
	 * 数量
	 */
	private Integer num;
	/**
	 * 是否结算(0:未结算；1:已结算)
	 */
	private Integer ischarge;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：食品id
	 */
	public void setFoodid(Integer foodid) {
		this.foodid = foodid;
	}
	/**
	 * 获取：食品id
	 */
	public Integer getFoodid() {
		return foodid;
	}
	/**
	 * 设置：单价
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * 获取：单价
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * 设置：数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
	/**
	 * 获取：数量
	 */
	public Integer getNum() {
		return num;
	}
	/**
	 * 设置：是否结算(0:未结算；1:已结算)
	 */
	public void setIscharge(Integer ischarge) {
		this.ischarge = ischarge;
	}
	/**
	 * 获取：是否结算(0:未结算；1:已结算)
	 */
	public Integer getIscharge() {
		return ischarge;
	}
}
