package io.youngking.modules.cart.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.cart.entity.CartEntity;
import io.youngking.modules.cart.service.CartService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;



/**
 * 购物车
 *
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:49:27
 */
@RestController
@RequestMapping("cart/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cart:cart:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = cartService.queryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        page.setList(cartService.getlist(params1));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cart:cart:info")
    public R info(@PathVariable("id") Integer id){
			CartEntity cart = cartService.selectById(id);

        return R.ok().put("cart", cart);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cart:cart:save")
    public R save(@RequestBody CartEntity cart){
			cartService.insert(cart);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cart:cart:update")
    public R update(@RequestBody CartEntity cart){
			cartService.updateById(cart);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cart:cart:delete")
    public R delete(@RequestBody Integer[] ids){
			cartService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
