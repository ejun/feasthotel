package io.youngking.modules.cart.dao;

import io.youngking.modules.cart.entity.CartEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 购物车
 * 
 * @author limouren
 * @email sunlightcs@gmail.com
 * @date 2018-07-25 16:49:27
 */
@Mapper
public interface CartDao extends BaseMapper<CartEntity> {

   List<CartEntity> getlist(Map<String,Object> params);


}
