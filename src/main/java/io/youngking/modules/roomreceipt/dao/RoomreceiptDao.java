package io.youngking.modules.roomreceipt.dao;

import io.youngking.modules.roomreceipt.entity.RoomreceiptEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 房间订单表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 09:58:20
 */
@Mapper
public interface RoomreceiptDao extends BaseMapper<RoomreceiptEntity> {
    /**
     * 查询订单列表
     */
    List<RoomreceiptEntity> getlist(Map<String, Object> params);
    /**
     * 根据用户查询订单
     */
    List<RoomreceiptEntity> regetlist(Map<String, Object> params);

    /**
     * 订单详细(房间订单管理)
     */
    Map<String,Object> getinfo(Integer id);

}
