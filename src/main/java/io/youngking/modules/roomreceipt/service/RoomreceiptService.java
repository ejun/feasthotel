package io.youngking.modules.roomreceipt.service;

import com.baomidou.mybatisplus.service.IService;
import io.youngking.common.utils.PageUtils;
import io.youngking.modules.roomreceipt.entity.RoomreceiptEntity;

import java.util.List;
import java.util.Map;

/**
 * 房间订单表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 09:58:20
 */
public interface RoomreceiptService extends IService<RoomreceiptEntity> {
    /**
     * 查询订单列表（房间订单管理--分页）
     */
    PageUtils queryPage(Map<String, Object> params);
    /**
     * 查询订单列表（房间订单管理--数据）
     */
    List<RoomreceiptEntity> getlist(Map<String, Object> params);

    /**
     * 根据用户查询订单(后台用户管理--分页)
     */
    PageUtils requeryPage(Map<String, Object> params);
    /**
     * 根据用户查询订单(后台用户管理--数据)
     */
    List<RoomreceiptEntity> regetlist(Map<String, Object> params);

    /**
     * 订单详细(房间订单管理)
     */
    Map<String,Object> getinfo(Integer id);
}

