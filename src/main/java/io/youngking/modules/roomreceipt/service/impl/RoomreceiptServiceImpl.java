package io.youngking.modules.roomreceipt.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.Query;

import io.youngking.modules.roomreceipt.dao.RoomreceiptDao;
import io.youngking.modules.roomreceipt.entity.RoomreceiptEntity;
import io.youngking.modules.roomreceipt.service.RoomreceiptService;


@Service("roomreceiptService")
public class RoomreceiptServiceImpl extends ServiceImpl<RoomreceiptDao, RoomreceiptEntity> implements RoomreceiptService {

    /**
     * 查询订单列表（订单管理）
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        Page<RoomreceiptEntity> page = this.selectPage(
                new Query<RoomreceiptEntity>(params).getPage(),
                new EntityWrapper<RoomreceiptEntity>().like(StringUtils.isNotBlank(key),"receiptcode",key)
                    .orNew(StringUtils.isNotBlank(key),"username like {0}","%"+key+"%")
                    .orNew(StringUtils.isNotBlank(key),"phone like {0}","%"+key+"%")
                    .orderBy("id desc")
        );

        return new PageUtils(page);
    }

    @Override
    public List<RoomreceiptEntity> getlist(Map<String, Object> params){
        return baseMapper.getlist(params);
    }

    /**
     * 根据用户查询订单（用户管理）
     * @param params
     * @return
     */
    @Override
    public PageUtils requeryPage(Map<String, Object> params) {
        String key = params.get("key").toString();
        String customerid = params.get("customerid").toString();
        Page<RoomreceiptEntity> page = this.selectPage(
                new Query<RoomreceiptEntity>(params).getPage(),
                new EntityWrapper<RoomreceiptEntity>().where("customerid={0}",customerid).like(StringUtils.isNotBlank(key),"receiptcode",key)
                    .orNew(StringUtils.isNotBlank(key),"username like {0}","%"+key+"%")
                    .orNew(StringUtils.isNotBlank(key),"phone like {0}","%"+key+"%")
                    .orderBy("id desc")
        );

        return new PageUtils(page);
    }

    @Override
    public List<RoomreceiptEntity> regetlist(Map<String, Object> params){
        return baseMapper.regetlist(params);
    }

    /**
     * 订单详细信息
     */
    @Override
    public Map<String,Object> getinfo(Integer id){return baseMapper.getinfo(id);}

}
