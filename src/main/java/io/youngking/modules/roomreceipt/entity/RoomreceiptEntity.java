package io.youngking.modules.roomreceipt.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 房间订单表
 * 
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 09:58:20
 */
@TableName("tb_roomreceipt")
public class RoomreceiptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 单据编号
	 */
	private String receiptcode;
	/**
	 * 用户id
	 */
	private Integer customerid;
	/**
	 * 合计应收金额
	 */
	private BigDecimal totalshouldmoney;
	/**
	 * 合计实收金额
	 */
	private BigDecimal totalmoney;
	/**
	 * 下单时间
	 */
	private Date chargetime;
	/**
	 * 订单状态(0=待付款,2=待入住,3=已入住,4=已完成,5=已评价)
	 */
	private Integer status;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 房间数量
	 */
	private Integer roomnum;
	/**
	 * 入住人姓名
	 */
	private String username;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 入住时间
	 */
	private Date starttime;
	/**
	 * 离开时间
	 */
	private Date endtime;
	/**
	 * 房间类型
	 */
	private Integer roomtype;

	private String holdtime;

	@TableField(exist = false)
	private String typename;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：单据编号
	 */
	public void setReceiptcode(String receiptcode) {
		this.receiptcode = receiptcode;
	}
	/**
	 * 获取：单据编号
	 */
	public String getReceiptcode() {
		return receiptcode;
	}
	/**
	 * 设置：用户id
	 */
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getCustomerid() {
		return customerid;
	}
	/**
	 * 设置：合计应收金额
	 */
	public void setTotalshouldmoney(BigDecimal totalshouldmoney) {
		this.totalshouldmoney = totalshouldmoney;
	}
	/**
	 * 获取：合计应收金额
	 */
	public BigDecimal getTotalshouldmoney() {
		return totalshouldmoney;
	}
	/**
	 * 设置：合计实收金额
	 */
	public void setTotalmoney(BigDecimal totalmoney) {
		this.totalmoney = totalmoney;
	}
	/**
	 * 获取：合计实收金额
	 */
	public BigDecimal getTotalmoney() {
		return totalmoney;
	}
	/**
	 * 设置：下单时间
	 */
	public void setChargetime(Date chargetime) {
		this.chargetime = chargetime;
	}
	/**
	 * 获取：下单时间
	 */
	public Date getChargetime() {
		return chargetime;
	}
	/**
	 * 设置：订单状态(0=待付款,2=待入住,3=已入住,4=已完成,5=已评价)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：订单状态(0=待付款,2=待入住,3=已入住,4=已完成,5=已评价)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：房间数量
	 */
	public void setRoomnum(Integer roomnum) {
		this.roomnum = roomnum;
	}
	/**
	 * 获取：房间数量
	 */
	public Integer getRoomnum() {
		return roomnum;
	}
	/**
	 * 设置：入住人姓名
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 获取：入住人姓名
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * 设置：联系电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：联系电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：入住时间
	 */
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	/**
	 * 获取：入住时间
	 */
	public Date getStarttime() {
		return starttime;
	}
	/**
	 * 设置：离开时间
	 */
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	/**
	 * 获取：离开时间
	 */
	public Date getEndtime() {
		return endtime;
	}
	/**
	 * 设置：房间类型
	 */
	public void setRoomtype(Integer roomtype) {
		this.roomtype = roomtype;
	}
	/**
	 * 获取：房间类型
	 */
	public Integer getRoomtype() {
		return roomtype;
	}

	public String getHoldtime() {
		return holdtime;
	}

	public void setHoldtime(String holdtime) {
		this.holdtime = holdtime;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}
}
