package io.youngking.modules.roomreceipt.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.youngking.modules.roomreceipt.entity.RoomreceiptEntity;
import io.youngking.modules.roomreceipt.service.RoomreceiptService;
import io.youngking.common.utils.PageUtils;
import io.youngking.common.utils.R;


/**
 * 房间订单表
 *
 * @author lifei
 * @email sunlightcs@gmail.com
 * @date 2018-07-26 09:58:20
 */
@RestController
@RequestMapping("roomreceipt/roomreceipt")
public class RoomreceiptController {
    @Autowired
    private RoomreceiptService roomreceiptService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("roomreceipt:roomreceipt:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = roomreceiptService.queryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        page.setList(roomreceiptService.getlist(params1));
        return R.ok().put("page", page);
    }

    /**
     * 获取订单记录（根据用户）
     */
    @RequestMapping("/orderlist")
    @RequiresPermissions("roomreceipt:roomreceipt:orderlist")
    public R orderlist(@RequestParam Map<String, Object> params) {
        PageUtils page = roomreceiptService.requeryPage(params);
        Map<String, Object> params1 = new HashMap<>();
        int currPage = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        params1.put("offset", (currPage - 1) * limit);
        params1.put("limit", limit);
        params1.put("key", params.get("key"));
        params1.put("customerid", params.get("customerid"));
        page.setList(roomreceiptService.regetlist(params1));
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("roomreceipt:roomreceipt:info")
    public R info(@PathVariable("id") Integer id) {
        Map<String,Object> roomreceipt = roomreceiptService.getinfo(id);
        return R.ok().put("roomreceipt", roomreceipt);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("roomreceipt:roomreceipt:save")
    public R save(@RequestBody RoomreceiptEntity roomreceipt) {
        roomreceiptService.insert(roomreceipt);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("roomreceipt:roomreceipt:update")
    public R update(@RequestBody RoomreceiptEntity roomreceipt) {
        roomreceiptService.updateById(roomreceipt);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("roomreceipt:roomreceipt:delete")
    public R delete(@RequestBody Integer[] ids) {
        roomreceiptService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }
}
